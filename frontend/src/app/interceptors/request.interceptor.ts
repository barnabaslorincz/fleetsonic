import {NgxSpinnerService} from 'ngx-spinner';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {finalize, tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  activeRequestCount = 0;

  constructor(private spinnerService: NgxSpinnerService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.spinnerService.show();
    this.activeRequestCount++;
    return next.handle(req)
      .pipe ( tap (
        ), finalize(() => {
          this.activeRequestCount--;
          if (this.activeRequestCount === 0) {
            this.spinnerService.hide();
          }
        })
      );
  }
}
