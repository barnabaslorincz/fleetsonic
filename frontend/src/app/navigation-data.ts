import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'DASHBOARD',
    url: '/dashboard',
    icon: 'icon-speedometer',
  },
  {
    name: 'PROFILE',
    url: '/users',
    icon: 'icon-user',
  },
  {
    name: 'ORGANISATION',
    url: '/organisation',
    icon: 'cil-building',
  },
  {
    name: 'VEHICLES',
    url: '/vehicles',
    icon: 'cil-car-alt',
  },
  {
    name: 'EVENTS',
    url: '/events',
    icon: 'cil-calendar',
  },
  {
    name: 'JOURNEYS',
    url: '/journeys',
    icon: 'cil-swap-horizontal',
  }
];
