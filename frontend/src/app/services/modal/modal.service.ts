import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ModalData} from '../../models/modal/modal-data';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private modalSubject: Subject<ModalData> = new Subject<ModalData>();
  private resultSubject: Subject<boolean> = new Subject<boolean>();

  constructor() { }

  launchModal(data: ModalData): Subject<boolean> {
    this.modalSubject.next(data);
    return this.resultSubject;
  }

  onModalClosed(value: boolean) {
    this.resultSubject.next(value);
  }

  getModalSubject(): Subject<ModalData> {
    return this.modalSubject;
  }
}
