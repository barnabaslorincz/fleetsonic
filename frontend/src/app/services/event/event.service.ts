import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import {IdentityModel} from '../../models/shared/identity-model';
import {UserUpdateRequest} from '../../models/user/user-update-request';
import {ResourceList} from '../../models/shared/resource-list';
import {VehicleModel} from '../../models/vehicle/vehicle.model';
import {VehicleCreateRequest} from '../../models/vehicle/vehicle-create-request';
import {VehicleUpdateRequest} from '../../models/vehicle/vehicle-update-request';
import {EventModel} from '../../models/event/event.model';
import {EventCreateRequest} from '../../models/event/event-create-request';
import {EventUpdateRequest} from '../../models/event/event-update-request';

const API_URL = 'http://localhost:8080';
const VEHICLE_URL = '/vehicles/';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  constructor(private http: HttpClient) { }

  getForVehicle(vehicleId: number, params: HttpParams): Observable<ResourceList<EventModel>> {
    return this.http.get<ResourceList<EventModel>>(API_URL + VEHICLE_URL + vehicleId + '/events/', { params : params });
  }

  getAll(params: HttpParams): Observable<ResourceList<EventModel>> {
    return this.http.get<ResourceList<EventModel>>(API_URL + '/events/', { params : params });
  }

  get(vehicleId: number, request: IdentityModel): Observable<EventModel> {
    return this.http.get<EventModel>(API_URL + VEHICLE_URL + vehicleId + '/events/' + request.id);
  }

  create(vehicleId: number, request: EventCreateRequest): Observable<IdentityModel> {
    return this.http.post<IdentityModel>(API_URL + VEHICLE_URL + vehicleId + '/events/', request);
  }

  update(vehicleId: number, eventId: number, request: EventUpdateRequest): Observable<any> {
    return this.http.put(API_URL + VEHICLE_URL + vehicleId + '/events/' + eventId, request);
  }

  statCount(): Observable<number> {
    return this.http.get<number>(API_URL + '/events/statCount', );
  }

  statAmountCount(): Observable<number> {
    return this.http.get<number>(API_URL + '/events/statAmountCount', );
  }

}
