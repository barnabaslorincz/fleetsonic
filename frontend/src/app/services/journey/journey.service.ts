import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import {IdentityModel} from '../../models/shared/identity-model';
import {ResourceList} from '../../models/shared/resource-list';
import {JourneyModel} from '../../models/journey/journey.model';
import {JourneyCreateUpdateRequest} from '../../models/journey/journey-create-update-request';

const API_URL = 'http://localhost:8080';
const JOURNEY_URL = '/vehicles/';

@Injectable({
  providedIn: 'root'
})
export class JourneyService {
  constructor(private http: HttpClient) { }

  getForVehicle(vehicleId: number, params: HttpParams): Observable<ResourceList<JourneyModel>> {
    return this.http.get<ResourceList<JourneyModel>>(API_URL + JOURNEY_URL + vehicleId + '/journeys/', { params : params });
  }

  getAll(params: HttpParams): Observable<ResourceList<JourneyModel>> {
    return this.http.get<ResourceList<JourneyModel>>(API_URL + '/journeys/', { params : params });
  }

  get(vehicleId: number, request: IdentityModel): Observable<JourneyModel> {
    return this.http.get<JourneyModel>(API_URL + JOURNEY_URL + vehicleId + '/journeys/' + request.id);
  }

  create(vehicleId: number, request: JourneyCreateUpdateRequest): Observable<IdentityModel> {
    return this.http.post<IdentityModel>(API_URL + JOURNEY_URL + vehicleId + '/journeys/', request);
  }

  update(vehicleId: number, journeyId: number, request: JourneyCreateUpdateRequest): Observable<any> {
    return this.http.put(API_URL + JOURNEY_URL + vehicleId + '/journeys/' + journeyId, request);
  }

  statCount(): Observable<number> {
    return this.http.get<number>(API_URL + '/journeys/statCount');
  }

  statKmCount(): Observable<number> {
    return this.http.get<number>(API_URL + '/journeys/statKmCount');
  }

}
