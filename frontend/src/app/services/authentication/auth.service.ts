import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {RegisterRequest} from '../../models/register/register-request';
import {LoginRequest} from '../../models/login/login-request';

const AUTH_API = 'http://localhost:8080/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(request: LoginRequest): Observable<any> {
    return this.http.post(AUTH_API + 'login', request, httpOptions);
  }

  register(request: RegisterRequest): Observable<any> {
    return this.http.post(AUTH_API + 'register', request, httpOptions);
  }
}
