import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import {IdentityModel} from '../../models/shared/identity-model';
import {UserModel} from '../../models/user/user.model';
import {UserUpdateRequest} from '../../models/user/user-update-request';
import {UserSearchModel} from '../../models/user/user-search-model';
import {ResourceList} from '../../models/shared/resource-list';

const API_URL = 'http://localhost:8080/users/';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  getAll(params: HttpParams): Observable<ResourceList<UserModel>> {
    return this.http.get<ResourceList<UserModel>>(API_URL, { params : params });
  }

  get(request: IdentityModel): Observable<UserModel> {
    return this.http.get<UserModel>(API_URL + request.id);
  }

  update(request: UserUpdateRequest): Observable<any> {
    return this.http.put(API_URL + request.id, request);
  }

  updateImage(userId: number, request?: IdentityModel): Observable<any> {
    return this.http.put(API_URL + userId + '/image', request || {});
  }

}
