import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IdentityModel} from '../../models/shared/identity-model';

const API_URL = 'http://localhost:8080/image/';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  constructor(private http: HttpClient) { }

  get(request: IdentityModel): Observable<any> {
    return this.http.get(API_URL + request.id, {responseType: 'blob'});
  }

  uploadImage(image: File): Observable<number> {
    const formData: FormData = new FormData();
    formData.append('image', image);
    return this.http.post<number>(API_URL, formData);
  }

}
