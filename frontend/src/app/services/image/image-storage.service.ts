import {Injectable, SecurityContext} from '@angular/core';
import {ImageService} from './image.service';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ImageStorageService {

  images: ImageStorageModel[] = [];

  constructor(private imageService: ImageService,
              private sanitizer: DomSanitizer) { }

  public getImage(imageId: number) {
    const image = this.images.find(img => img.id === imageId);
    if (image) {
      return image;
    } else {
      const newImage = {
        id: imageId,
        image: undefined
      };
      this.imageService.get({ id: newImage.id}).subscribe((blob: any) => {
        const objectURL = URL.createObjectURL(blob);
        newImage.image = this.sanitizer.sanitize(SecurityContext.HTML, this.sanitizer.bypassSecurityTrustHtml(objectURL));
      });
      this.images.push(newImage);
      return newImage;
    }
  }
}

class ImageStorageModel {
  public id: number;
  public image?: SafeStyle;
}
