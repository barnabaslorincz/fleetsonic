import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import {IdentityModel} from '../../models/shared/identity-model';
import {UserUpdateRequest} from '../../models/user/user-update-request';
import {ResourceList} from '../../models/shared/resource-list';
import {VehicleModel} from '../../models/vehicle/vehicle.model';
import {VehicleCreateRequest} from '../../models/vehicle/vehicle-create-request';
import {VehicleUpdateRequest} from '../../models/vehicle/vehicle-update-request';

const API_URL = 'http://localhost:8080/vehicles/';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  constructor(private http: HttpClient) { }

  getAll(params: HttpParams): Observable<ResourceList<VehicleModel>> {
    return this.http.get<ResourceList<VehicleModel>>(API_URL, { params : params });
  }

  get(request: IdentityModel): Observable<VehicleModel> {
    return this.http.get<VehicleModel>(API_URL + request.id);
  }

  create(request: VehicleCreateRequest): Observable<IdentityModel> {
    return this.http.post<IdentityModel>(API_URL, request);
  }

  update(request: VehicleUpdateRequest): Observable<any> {
    return this.http.put(API_URL + request.id, request);
  }

  updateImage(vehicleId: number, request?: IdentityModel): Observable<any> {
    return this.http.put(API_URL + vehicleId + '/image', request || {});
  }

}
