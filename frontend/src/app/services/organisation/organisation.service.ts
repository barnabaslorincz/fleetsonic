import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IdentityModel} from '../../models/shared/identity-model';
import {UserModel} from '../../models/user/user.model';
import {UserUpdateRequest} from '../../models/user/user-update-request';
import {OrganisationCreateUpdateRequest} from '../../models/organisation/organisation-create-update-request';
import {OrganisationJoinRequest} from '../../models/organisation/organisation-join-request';
import {OrganisationModel} from '../../models/organisation/organisation.model';

const API_URL = 'http://localhost:8080/organisations/';

@Injectable({
  providedIn: 'root'
})
export class OrganisationService {
  constructor(private http: HttpClient) { }

  get(request: IdentityModel): Observable<OrganisationModel> {
    return this.http.get<OrganisationModel>(API_URL + request.id);
  }

  create(request: OrganisationCreateUpdateRequest): Observable<any> {
    return this.http.post(API_URL, request);
  }

  update(request: OrganisationCreateUpdateRequest): Observable<any> {
    return this.http.put(API_URL + request.id, request);
  }

  join(request: OrganisationJoinRequest): Observable<any> {
    return this.http.patch(API_URL + 'join', request);
  }

  refreshInviteCode(request: IdentityModel): Observable<any> {
    return this.http.patch(API_URL + request.id + '/refresh-code', request);
  }

  removeMember(organisationId: number, request: IdentityModel): Observable<any> {
    return this.http.patch(API_URL  + organisationId + '/remove-member', request);
  }

  removeAdmin(organisationId: number, request: IdentityModel): Observable<any> {
    return this.http.patch(API_URL  + organisationId + '/remove-admin', request);
  }

  addAdmin(organisationId: number, request: IdentityModel): Observable<any> {
    return this.http.patch(API_URL  + organisationId + '/add-admin', request);
  }

}
