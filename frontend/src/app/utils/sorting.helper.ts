export class SortingHelper {
  public orderField: string = 'id';
  public orderDirection: string = 'DESC';

  reset() {
    this.orderField = 'id';
    this.orderDirection = 'ASC';
  }
}
