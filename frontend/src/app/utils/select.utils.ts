export class SelectUtils {

  static compareByValue(optionOne, optionTwo): boolean {
    return optionOne === optionTwo;
  }

}
