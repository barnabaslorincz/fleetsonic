export class PagingHelper {
  public totalItems: number = 0;
  public currentPage: number = 1;
  public maxSize: number = 10;

  reset() {
    this.currentPage = 1;
  }
}
