import {BaseModel} from '../shared/base.model';

export class OrganisationModel extends BaseModel {
  name: string;
  invite_code: string;
}
