import {IdentityModel} from '../shared/identity-model';

export interface OrganisationCreateUpdateRequest {
  id?: number;
  name: string;
}
