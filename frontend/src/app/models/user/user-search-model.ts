import {Role} from '../role/role.model';
import {PagingModel} from '../shared/paging.model';

export interface UserSearchModel extends PagingModel {
  user_name?: string;
  first_name?: string;
  last_name?: string;
  email?: string;
  role?: Role;
  driving_license_expiry_from?: Date;
  driving_license_expiry_to?: Date;
}
