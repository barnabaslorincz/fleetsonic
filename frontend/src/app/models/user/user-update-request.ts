import {IdentityModel} from '../shared/identity-model';

export interface UserUpdateRequest extends IdentityModel {
  first_name: string;
  last_name: string;
  driving_license_expiry?: Date;
}
