import {BaseModel} from '../shared/base.model';
import {OrganisationModel} from '../organisation/organisation.model';
import {Role} from '../role/role.model';

export class UserModel extends BaseModel {
  first_name: string;
  last_name: string;
  user_name: string;
  email: string;
  password: string;
  role: string;
  image_id?: number;
  driving_license_expiry?: Date;
  organisation?: OrganisationModel;
}
