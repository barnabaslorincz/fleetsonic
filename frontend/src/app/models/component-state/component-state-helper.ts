import {ComponentState} from './component-state';

export class ComponentStateHelper {
  get state(): ComponentState {
    return this._state;
  }

  set state(value: ComponentState) {
    this._state = value;
  }
  private _state: ComponentState;

  constructor(state: string) {
    switch (state) {
      case 'create':
        this._state = ComponentState.CREATE;
        break;
      case 'edit':
        this._state = ComponentState.UPDATE;
        break;
      case 'detail':
        this._state = ComponentState.DETAIL;
        break;
      default:
        this._state = ComponentState.DETAIL;
        break;
    }
  }

  isCreateView(): boolean {
    return this.state === ComponentState.CREATE;
  }

  isUpdateView(): boolean {
    return this.state === ComponentState.UPDATE;
  }

  isReadonlyView(): boolean {
    return this.state === ComponentState.DETAIL;
  }
}
