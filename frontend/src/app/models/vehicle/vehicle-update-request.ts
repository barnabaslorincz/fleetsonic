import {IdentityModel} from '../shared/identity-model';

export interface VehicleUpdateRequest extends IdentityModel {
  license_plate?: string;
  fuel_capacity?: number;
  inspection_validity?: Date;
}
