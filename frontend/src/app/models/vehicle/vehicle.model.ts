import {BaseModel} from '../shared/base.model';
import {OrganisationModel} from '../organisation/organisation.model';
import {UserModel} from '../user/user.model';

export class VehicleModel extends BaseModel {
  make: string;
  model: string;
  fuel_type: string;
  license_plate: string;
  fuel_capacity: number;
  km_counter?: number;
  inspection_validity?: Date;
  owner?: UserModel;
  organisation?: OrganisationModel;
  image_id?: number;
  image?: any;
}
