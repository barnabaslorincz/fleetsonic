export interface VehicleCreateRequest {
  make: string;
  model: string;
  fuel_type: string;
  license_plate: string;
  fuel_capacity?: number;
  km_counter: number;
  inspection_validity?: Date;
  owner_id?: number;
  organisation_id?: number;
}
