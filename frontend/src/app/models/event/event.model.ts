import {BaseModel} from '../shared/base.model';
import {VehicleModel} from '../vehicle/vehicle.model';

export class EventModel extends BaseModel {
  event_type: string;
  time: string;
  km_counter: number;
  latitude?: number;
  longitude?: number;
  amount?: number;
  price?: number;
  description?: number;
  vehicle?: VehicleModel;
}
