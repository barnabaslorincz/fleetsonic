export interface EventCreateRequest {
  event_type: string;
  time: string;
  km_counter: number;
  latitude?: number;
  longitude?: number;
  amount?: number;
  price?: number;
  description?: number;
}
