export class ServerErrorModel {
  fieldErrors: any = {};
  globalError?: string = undefined;

  clear(): void {
    this.fieldErrors = {};
    this.globalError = undefined;
  }

  parse(error: any): void {
    if (error) {
      if (error.field_errors && error.field_errors.length > 0) {
        error.field_errors.forEach(fieldError => {
          this.fieldErrors[fieldError.field] = fieldError.error;
        });
      }
      if (error.global_errors && error.global_errors.length > 0) {
        this.globalError = error.global_errors.join('\n');
      }
    }
  }
}
