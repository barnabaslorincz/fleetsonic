export class BaseModel {
  id: number;
  creation_time: Date;
  update_time: Date;
  creator_user?: string;
  creator_user_id?: number;
}
