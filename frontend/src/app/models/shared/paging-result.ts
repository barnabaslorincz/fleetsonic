export class PagingResult {
  current_number_of_items: number;
  total_number_of_items: number;
}
