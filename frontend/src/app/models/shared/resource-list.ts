import {PagingResult} from './paging-result';

export class ResourceList<T> {
  items: T[];
  paging_result: PagingResult;
}
