import {OrderDirection} from './order-direction';

export interface PagingModel {
  page_number?: number;
  number_of_items?: number;
  order_direction?: OrderDirection;
  order_field?: string;
}
