export class ModalData {
  titleStringKey: string;
  bodyStringKey: string;
  type: string;
  positiveButtonText: string;
  negativeButtonText: string;
}
