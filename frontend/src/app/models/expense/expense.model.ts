import {BaseModel} from '../shared/base.model';

export class ExpenseModel extends BaseModel {
  name: string;
  amount: number;
}
