export interface RegisterRequest {
  first_name: string;
  last_name: string;
  user_name: string;
  email: string;
  password: string;
  driving_license_expiry?: Date;
}
