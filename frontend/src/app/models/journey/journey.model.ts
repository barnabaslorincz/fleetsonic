import {BaseModel} from '../shared/base.model';
import {VehicleModel} from '../vehicle/vehicle.model';

export class JourneyModel extends BaseModel {
  start_time: string;
  finish_time?: string;
  start_km: number;
  finish_km?: number;
  start_latitude?: number;
  start_longitude?: number;
  finish_latitude?: number;
  finish_longitude?: number;
  description?: number;
  vehicle?: VehicleModel;
}
