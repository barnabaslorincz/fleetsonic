import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SortingHelper} from '../../utils/sorting.helper';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[tableSorter]',
  templateUrl: './table-sorter.component.html',
  styleUrls: ['./table-sorter.component.scss']
})
export class TableSorterComponent implements OnInit {

  @Input()
  stringKey: string;

  @Input()
  sorterField: string;

  @Input()
  sortingHelper: SortingHelper;

  @Output()
  sortingEvent: EventEmitter<any> = new EventEmitter<any>();

  get iconClass() {
    return this.sorterField === this.sortingHelper.orderField ?
      this.sortingHelper.orderDirection === 'ASC' ? 'cil-sort-ascending' : 'cil-sort-descending'
      : 'cil-swap-vertical';
  }

  constructor() { }

  ngOnInit(): void {
  }

  onSorterClicked() {
    this.sortingHelper.orderField = this.sorterField;
    this.sortingHelper.orderDirection =
      this.sortingHelper.orderDirection === 'ASC' ? 'DESC' : 'ASC';
    this.sortingEvent.emit();
  }

}
