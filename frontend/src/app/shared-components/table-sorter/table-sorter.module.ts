import { NgModule } from '@angular/core';
import {TableSorterComponent} from './table-sorter.component';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    TableSorterComponent,
  ],
  imports: [
    TranslateModule,
    CommonModule
  ],
  exports: [
    TableSorterComponent
  ],
  providers: [ ],
})

export class TableSorterModule { }
