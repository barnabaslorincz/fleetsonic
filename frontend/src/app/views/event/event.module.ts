import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AlertModule} from 'ngx-bootstrap/alert';
import {ModalModule} from 'ngx-bootstrap/modal';
import {EventRoutingModule} from './event-routing.module';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {TableSorterModule} from '../../shared-components/table-sorter/table-sorter.module';
import {EventComponent} from './event/event.component';
import {EventListComponent} from './event-list/event-list.component';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {VehicleModule} from '../vehicle/vehicle.module';

@NgModule({
  imports: [
    FormsModule,
    EventRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    TranslateModule,
    ReactiveFormsModule,
    CommonModule,
    NgxSpinnerModule,
    AlertModule,
    ModalModule,
    PaginationModule,
    TableSorterModule,
    LeafletModule,
    VehicleModule
  ],
  declarations: [
    EventComponent,
    EventListComponent
  ]
})
export class EventModule { }
