import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {EventListComponent} from './event-list/event-list.component';
import {EventComponent} from './event/event.component';

const routes: Routes = [
  {
    path: '',
    component: EventListComponent,
    data: {
      title: 'EVENT_LIST'
    }
  },
  {
    path: 'create',
    component: EventComponent,
    data: {
      title: 'EVENT_CREATE'
    }
  },
  {
    path: ':id/edit',
    component: EventComponent,
    data: {
      title: 'EVENT_EDIT'
    }
  },
  {
    path: ':id/detail',
    component: EventComponent,
    data: {
      title: 'EVENT_DETAIL'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventRoutingModule {}
