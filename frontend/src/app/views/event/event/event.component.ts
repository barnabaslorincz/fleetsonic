import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {VehicleModel} from '../../../models/vehicle/vehicle.model';
import {ComponentStateHelper} from '../../../models/component-state/component-state-helper';
import {FormBuilder, Validators} from '@angular/forms';
import {ServerErrorModel} from '../../../models/shared/server-error.model';
import {UserService} from '../../../services/user/user.service';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {Role} from '../../../models/role/role.model';
import { SelectUtils } from '../../../utils/select.utils';
import {EventModel} from '../../../models/event/event.model';
import {EventService} from '../../../services/event/event.service';
import {EventCreateRequest} from '../../../models/event/event-create-request';
import {EventUpdateRequest} from '../../../models/event/event-update-request';
import {icon, latLng, Layer, marker, tileLayer} from 'leaflet';
import {LeafletDirective} from '@asymmetrik/ngx-leaflet';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit, OnDestroy, AfterViewInit {
  SelectUtils = SelectUtils;
  alerts: any = [];
  date: Date = new Date();

  @ViewChild(LeafletDirective) leaflet;

  eventTypes = [
    'REFUELING_OR_CHARGING',
    'CLEANING',
    'MAINTENANCE',
    'INCIDENT'
  ];

  leafletOptions = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
    ],
    center: latLng(47.4979, 19.0402),
    zoom: 13
  };
  leafletCenter = latLng({ lat: 47.4979, lng: 19.0402 });
  leafletMarkers: Layer[] = [];


  id: number;
  vehicleId?: number;
  private routeSubscription: Subscription;
  eventModel: EventModel = new EventModel();
  vehicleModel: VehicleModel = new VehicleModel();
  componentStateHelper: ComponentStateHelper;

  eventForm = this.fb.group({
    eventType: ['', Validators.required],
    time: ['', Validators.required],
    kmCounter: ['', Validators.required],
    latitude: [''],
    longitude: [''],
    amount: [''],
    price: [''],
    description: [''],
  });

  get eventType() { return this.eventForm.get('eventType'); }
  get time() { return this.eventForm.get('time'); }
  get kmCounter() { return this.eventForm.get('kmCounter'); }
  get latitude() { return this.eventForm.get('latitude'); }
  get longitude() { return this.eventForm.get('longitude'); }
  get amount() { return this.eventForm.get('amount'); }
  get price() { return this.eventForm.get('price'); }
  get description() { return this.eventForm.get('description'); }

  serverErrors: ServerErrorModel = new ServerErrorModel();

  constructor(private userService: UserService,
              private vehicleService: VehicleService,
              private eventService: EventService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private tokenStorageService: TokenStorageService) { }

  ngOnInit() {
    this.routeSubscription = this.route.url.subscribe(urlSegments => {
      this.vehicleId = +this.route.snapshot.paramMap.get('vehicleId');
      if (this.vehicleId) {
        this.loadVehicle();
      }
      this.componentStateHelper = new ComponentStateHelper(urlSegments[urlSegments.length - 1].path);
      if (!this.componentStateHelper.isCreateView()) {
        this.id = +urlSegments[0].path;
        this.loadEvent();
      } else {
        this.setUpMap();
      }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  loadVehicle(): void {
    this.vehicleService.get({ id: this.vehicleId }).subscribe((vehicleModel: VehicleModel) => {
      this.vehicleModel = vehicleModel;
    });
  }

  loadEvent(): void {
    this.eventService.get(this.vehicleId, { id: this.id }).subscribe((eventModel: EventModel) => {
      this.eventModel = eventModel;
      this.eventType.setValue(eventModel.event_type);
      this.time.setValue(eventModel.time);
      this.kmCounter.setValue(eventModel.km_counter);
      this.latitude.setValue(eventModel.latitude);
      this.longitude.setValue(eventModel.longitude);
      this.amount.setValue(eventModel.amount);
      this.price.setValue(eventModel.price);
      this.description.setValue(eventModel.description);
      this.setUpMap();
    });
  }

  onSubmit(): void {
    this.eventForm.markAllAsTouched();
    if (this.eventForm.invalid) {
      return;
    }
    this.serverErrors.clear();
    if (this.componentStateHelper.isCreateView()) {
      this.createEvent();
    } else {
      this.updateEvent();
    }
  }

  createEvent() {
    const request: EventCreateRequest = {
      event_type: this.eventType.value,
      time: this.time.value,
      km_counter: this.kmCounter.value,
      latitude: this.latitude.value,
      longitude: this.longitude.value,
      amount: this.amount.value,
      price: this.price.value,
      description: this.description.value,
    };
    this.eventService.create(this.vehicleId, request).subscribe(
      response => {
        this.router.navigate(['vehicles/' + this.vehicleId + '/events/' + response.id + '/detail']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  updateEvent() {
    const request: EventUpdateRequest = {
      time: this.time.value,
      km_counter: this.kmCounter.value,
      latitude: this.latitude.value,
      longitude: this.longitude.value,
      amount: this.amount.value,
      price: this.price.value,
      description: this.description.value,
    };
    this.eventService.update(this.vehicleId, this.id, request).subscribe(
      response => {
        this.router.navigate(['vehicles/' + this.vehicleId + '/events/' + this.id + '/detail']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  hasLocalValidationError(fieldName: string): boolean {
    const control = this.eventForm.get(fieldName);
    return control.invalid && this.isTouched(fieldName);
  }

  hasRemoteValidationError(fieldName: string): boolean {
    return !!this.serverErrors.fieldErrors[fieldName];
  }

  hasValidationError(localName, remoteName): boolean {
    return this.hasLocalValidationError(localName) || this.hasRemoteValidationError(remoteName);
  }

  removeRemoteValidationError(remoteName): void {
    this.serverErrors.fieldErrors[remoteName] = undefined;
  }

  isTouched(fieldName: string): boolean {
    const control = this.eventForm.get(fieldName);
    return control.dirty || control.touched;
  }

  navigateBack(): void {
    const uri = this.id ? 'vehicles/' + this.vehicleId + '/events/' + this.id + '/detail' : 'vehicles/' + this.vehicleId + '/events/';
    this.router.navigate([uri]);
  }

  edit() {
    this.router.navigate(['vehicles/' + this.vehicleId + '/events/' + this.id + '/edit']);
  }

  isMine(): boolean {
    return this.tokenStorageService.getUser().user_name === this.eventModel.creator_user;
  }

  isAdmin(): boolean {
    return Role[this.tokenStorageService.getUser().role as keyof typeof Role] === Role.ADMIN;
  }

  getFuelTypeMeasurement(): string {
    let fuelTypeMeasurement = '';
    if (this.vehicleModel.fuel_type) {
      switch (this.vehicleModel.fuel_type) {
        case 'PETROL':
        case 'DIESEL':
        case 'PLUGIN_HYBRID':
        case 'HYBRID':
        case 'LPG':
        case 'CNG':
          fuelTypeMeasurement = 'liter';
          break;
        case 'BATTERY_ELECTRIC':
          fuelTypeMeasurement = 'kWh';
          break;
      }
    }
    return fuelTypeMeasurement;
  }

  getEventTypeStringKey(eventType: string): string {
    let eventTypeStringKey = eventType;
    if (this.vehicleModel.fuel_type && eventType === 'REFUELING_OR_CHARGING') {
      switch (this.vehicleModel.fuel_type) {
        case 'PETROL':
        case 'DIESEL':
        case 'PLUGIN_HYBRID':
        case 'HYBRID':
        case 'LPG':
        case 'CNG':
          eventTypeStringKey = 'REFUELING';
          break;
        case 'BATTERY_ELECTRIC':
          eventTypeStringKey = 'CHARGING';
          break;
      }
    }
    return eventTypeStringKey;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.leaflet.getMap().invalidateSize();
    }, 250);
  }

  setUpMap() {
    if (!this.componentStateHelper.isCreateView() && this.eventModel.latitude && this.eventModel.latitude) {
      this.addMarker(this.eventModel.latitude, this.eventModel.longitude);
      this.leafletCenter = latLng(this.eventModel.latitude, this.eventModel.longitude);
    } else if (!this.componentStateHelper.isReadonlyView() && (!this.eventModel.latitude || !this.eventModel.longitude)) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
            this.leafletCenter = latLng(position.coords.latitude, position.coords.longitude);
        });
      } else {
        this.leafletCenter = latLng({ lat: 47.4979, lng: 19.0402 });
      }
    }
  }

  addMarker(lat: number, lon: number) {
    this.leafletMarkers.push(marker(
      [lat, lon],
      {
        icon: icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: 'assets/marker-icon.png',
          shadowUrl: 'assets/marker-shadow.png'
        })
      }
    ));
  }

  onMapClick($event) {
    if (!this.componentStateHelper.isReadonlyView()) {
      this.leafletMarkers.splice(0, this.leafletMarkers.length);
      this.addMarker($event.latlng.lat, $event.latlng.lng);
      this.latitude.setValue($event.latlng.lat);
      this.longitude.setValue($event.latlng.lng);
    }
  }

  resetMarkers() {
    this.leafletMarkers.splice(0, this.leafletMarkers.length);
    this.latitude.setValue('');
    this.longitude.setValue('');
  }

  navigateToUserDetail(userId: number) {
    this.router.navigate(['users/' + userId + '/detail']);
  }

}
