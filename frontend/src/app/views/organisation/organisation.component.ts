import {Component, OnDestroy, OnInit} from '@angular/core';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {OrganisationModel} from '../../models/organisation/organisation.model';
import {ComponentStateHelper} from '../../models/component-state/component-state-helper';
import {FormBuilder, Validators} from '@angular/forms';
import {ServerErrorModel} from '../../models/shared/server-error.model';
import {OrganisationService} from '../../services/organisation/organisation.service';
import {OrganisationCreateUpdateRequest} from '../../models/organisation/organisation-create-update-request';
import {Role} from '../../models/role/role.model';
import {Subscription} from 'rxjs';
import {UserService} from '../../services/user/user.service';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-organisation',
  templateUrl: './organisation.component.html',
  styleUrls: ['./organisation.component.scss']
})
export class OrganisationComponent implements OnInit, OnDestroy {
  private routeSubscription: Subscription;

  id?: number;
  organisationModel: OrganisationModel = new OrganisationModel();
  componentStateHelper: ComponentStateHelper;

  organisationForm = this.fb.group({
    name: ['', Validators.required]
  });

  get name() { return this.organisationForm.get('name'); }

  serverErrors: ServerErrorModel = new ServerErrorModel();

  constructor(private organisationService: OrganisationService,
              private userService: UserService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    if (!this.tokenStorageService.getUser().organisation) {
      this.router.navigate(['organisation/create-join']);
    }
    this.routeSubscription = this.route.url.subscribe(urlSegments => {
      this.componentStateHelper = new ComponentStateHelper(urlSegments[0].path);
      this.id = this.tokenStorageService.getUser().organisation ? this.tokenStorageService.getUser().organisation.id : undefined;
      if (this.id) {
        this.loadOrganisation();
      }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  loadOrganisation(): void {
    this.organisationService.get({ id: this.id }).subscribe((organisationModel: OrganisationModel) => {
      this.organisationModel = organisationModel;
      this.name.setValue(organisationModel.name);
    });
  }

  onSubmit(): void {
    this.organisationForm.markAllAsTouched();
    if (this.organisationForm.invalid) {
      return;
    }
    this.serverErrors.clear();
    const request: OrganisationCreateUpdateRequest = {
      id: this.id,
      name: this.name.value,
    };
    this.organisationService.update(request).subscribe(
      response => {
        this.router.navigate(['organisation/detail']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  hasLocalValidationError(fieldName: string): boolean {
    const control = this.organisationForm.get(fieldName);
    return control.invalid && this.isTouched(fieldName);
  }

  hasRemoteValidationError(fieldName: string): boolean {
    return !!this.serverErrors.fieldErrors[fieldName];
  }

  hasValidationError(localName, remoteName): boolean {
    return this.hasLocalValidationError(localName) || this.hasRemoteValidationError(remoteName);
  }

  removeRemoteValidationError(remoteName): void {
    this.serverErrors.fieldErrors[remoteName] = undefined;
  }

  isTouched(fieldName: string): boolean {
    const control = this.organisationForm.get(fieldName);
    return control.dirty || control.touched;
  }

  navigateBack(): void {
    this.router.navigate(['organisation/detail']);
  }

  edit() {
    this.router.navigate(['organisation/edit']);
  }

  isAdmin(): boolean {
    return Role[this.tokenStorageService.getUser().role as keyof typeof Role] === Role.ADMIN;
  }

  refreshInviteCode() {
    this.organisationService.refreshInviteCode({ id: this.id }).subscribe(() => {
      this.loadOrganisation();
    });
  }

}
