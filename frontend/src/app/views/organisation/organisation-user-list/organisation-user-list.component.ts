import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {HttpParams} from '@angular/common/http';
import {ResourceList} from '../../../models/shared/resource-list';
import {UserModel} from '../../../models/user/user.model';
import {Role} from '../../../models/role/role.model';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {FormBuilder} from '@angular/forms';
import {SelectUtils} from '../../../utils/select.utils';
import {ModalService} from '../../../services/modal/modal.service';
import {OrganisationService} from '../../../services/organisation/organisation.service';
import {ServerErrorModel} from '../../../models/shared/server-error.model';
import {PaginationInstance} from 'ngx-pagination';
import {PagingModel} from '../../../models/shared/paging.model';
import {PagingHelper} from '../../../utils/paging.helper';
import {SortingHelper} from '../../../utils/sorting.helper';

@Component({
  selector: 'app-organisation-user-list',
  templateUrl: './organisation-user-list.component.html',
  styleUrls: ['./organisation-user-list.component.scss']
})
export class OrganisationUserListComponent implements OnInit {
  SelectUtils = SelectUtils;
  searchParams: HttpParams = new HttpParams();
  pagingHelper: PagingHelper = new PagingHelper();
  sortingHelper: SortingHelper = new SortingHelper();
  alerts: any = [];

  @Input()
  organisationId: number;

  roles = [
    { key: 'NONE', value: null},
    { key: 'USER', value: Role.USER},
    { key: 'ADMIN', value: Role.ADMIN},
  ];

  searchForm = this.fb.group({
    firstName: [''],
    lastName: [''],
    userName: [''],
    drivingLicenseExpiryFrom: [''],
    drivingLicenseExpiryTo: [''],
    role: [this.roles[0].value],
  });

  get firstName() { return this.searchForm.get('firstName'); }
  get lastName() { return this.searchForm.get('lastName'); }
  get userName() { return this.searchForm.get('userName'); }
  get drivingLicenseExpiryFrom() { return this.searchForm.get('drivingLicenseExpiryFrom'); }
  get drivingLicenseExpiryTo() { return this.searchForm.get('drivingLicenseExpiryTo'); }
  get role() { return this.searchForm.get('role'); }

  get searchButtonStringKey () { return this.isSearchOpen ? 'CLOSE_SEARCH' : 'OPEN_SEARCH'; }

  users: UserModel[] = [];
  isSearchOpen: boolean = false;

  constructor(private userService: UserService,
              private organisationService: OrganisationService,
              private fb: FormBuilder,
              private tokenStorageService: TokenStorageService,
              private modalService: ModalService) {
  }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers(): void {
    this.searchParams = this.searchParams.set('page_number', this.pagingHelper.currentPage + '');
    this.searchParams = this.searchParams.set('order_field', this.sortingHelper.orderField);
    this.searchParams = this.searchParams.set('order_direction', this.sortingHelper.orderDirection);
    this.userService.getAll(this.searchParams).subscribe((result: ResourceList<UserModel>) => {
      this.users = result.items;
      this.pagingHelper.totalItems = result.paging_result.current_number_of_items;
    });
  }

  isAdmin(role: string): boolean {
    return Role[role as keyof typeof Role] === Role.ADMIN;
  }

  isCurrentUserAdmin(): boolean {
    return Role[this.tokenStorageService.getUser().role as keyof typeof Role] === Role.ADMIN;
  }

  toggleSearch() {
    this.isSearchOpen = !this.isSearchOpen;
  }

  search() {
    let params = new HttpParams();
    if (this.firstName.value) {
      params = params.append('first_name', this.firstName.value);
    }
    if (this.lastName.value) {
      params = params.append('last_name', this.lastName.value);
    }
    if (this.userName.value) {
      params = params.append('user_name', this.userName.value);
    }
    if (this.drivingLicenseExpiryFrom.value) {
      params = params.append('driving_license_expiry_from', this.drivingLicenseExpiryFrom.value);
    }
    if (this.drivingLicenseExpiryTo.value) {
      params = params.append('driving_license_expiry_to', this.drivingLicenseExpiryTo.value);
    }
    if (this.role.value) {
      params = params.append('role', Role[this.role.value as keyof typeof Role].toString());
    }
    this.searchParams = params;
    this.loadUsers();
  }

  resetSearch() {
    this.sortingHelper.reset();
    this.pagingHelper.reset();
    this.searchForm.reset();
    this.search();
  }

  removeUser(userId: number) {
    const subscription = this.modalService.launchModal({
      titleStringKey: 'DELETE_USER',
      bodyStringKey: 'DELETE_USER_CONFIRM',
      type: 'danger',
      positiveButtonText: 'REMOVE',
      negativeButtonText: 'CANCEL',
    }).subscribe((result) => {
      if (!!result) {
        this.organisationService.removeMember(this.organisationId, { id: userId }).subscribe(() => {
          this.loadUsers();
          subscription.unsubscribe();
        },
          error => {
            this.onRequestError(error);
            subscription.unsubscribe();
          });
      }
    });
  }

  addAdmin(userId: number) {
    const subscription = this.modalService.launchModal({
      titleStringKey: 'ADD_ADMIN',
      bodyStringKey: 'ADD_ADMIN_CONFIRM',
      type: 'primary',
      positiveButtonText: 'ADD',
      negativeButtonText: 'CANCEL',
    }).subscribe((result) => {
      if (!!result) {
          this.organisationService.addAdmin(this.organisationId, { id: userId }).subscribe(() => {
            this.loadUsers();
            subscription.unsubscribe();
          },
            error => {
              this.onRequestError(error);
              subscription.unsubscribe();
            });
      }
    });
  }

  removeAdmin(userId: number) {
    const subscription = this.modalService.launchModal({
      titleStringKey: 'REMOVE_ADMIN',
      bodyStringKey: 'REMOVE_ADMIN_CONFIRM',
      type: 'warning',
      positiveButtonText: 'REMOVE',
      negativeButtonText: 'CANCEL',
    }).subscribe((result) => {
      if (!!result) {
          this.organisationService.removeAdmin(this.organisationId, { id: userId }).subscribe(() => {
            this.loadUsers();
              subscription.unsubscribe();
          },
            error => {
              this.onRequestError(error);
              subscription.unsubscribe();
            });
      }
    });
  }

  onRequestError(error: any): void {
    const serverError = new ServerErrorModel();
    serverError.parse(error.error);
    if (serverError.globalError) {
      this.alerts.push({
        type: 'danger',
        msg: serverError.globalError,
        timeout: 5000
      });
    }
  }

  toggleAdmin($event, isAdmin: boolean, id: number) {
    $event.preventDefault();
    if (isAdmin) {
      this.removeAdmin(id);
    } else {
      this.addAdmin(id);
    }
  }

  pageChanged($event: any) {
    this.pagingHelper.currentPage = $event.page;
    this.loadUsers();
  }
}
