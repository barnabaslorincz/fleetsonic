import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {OrganisationComponent} from './organisation.component';
import {OrganisationCreateJoinComponent} from './organisation-create-join/organisation-create-join.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'detail',
  },
  {
    path: 'detail',
    component: OrganisationComponent,
    data: {
      title: 'ORGANISATION_DETAIL'
    }
  },
  {
    path: 'edit',
    component: OrganisationComponent,
    data: {
      title: 'ORGANISATION_EDIT'
    }
  },
  {
    path: 'create-join',
    component: OrganisationCreateJoinComponent,
    data: {
      title: 'ORGANISATION_CREATE_JOIN'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganisationRoutingModule {}
