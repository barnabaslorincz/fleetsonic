import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServerErrorModel} from '../../../models/shared/server-error.model';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {OrganisationService} from '../../../services/organisation/organisation.service';
import {OrganisationCreateUpdateRequest} from '../../../models/organisation/organisation-create-update-request';
import {OrganisationJoinRequest} from '../../../models/organisation/organisation-join-request';
import {UserService} from '../../../services/user/user.service';

@Component({
  selector: 'app-organisation-create-join',
  templateUrl: './organisation-create-join.component.html',
  styleUrls: ['./organisation-create-join.component.scss']
})
export class OrganisationCreateJoinComponent implements OnInit {

  createForm = this.fb.group({
    name: ['', Validators.required]
  });

  joinForm = this.fb.group({
    inviteCode: ['', Validators.required]
  });

  get name() { return this.createForm.get('name'); }
  get inviteCode() { return this.joinForm.get('inviteCode'); }

  createServerErrors: ServerErrorModel = new ServerErrorModel();
  joinServerErrors: ServerErrorModel = new ServerErrorModel();

  constructor(private fb: FormBuilder,
              private router: Router,
              private tokenStorageService: TokenStorageService,
              private organisationService: OrganisationService,
              private userService: UserService) { }

  ngOnInit(): void {
  }

  onCreateSubmit(): void {
    this.createForm.markAllAsTouched();
    if (this.createForm.invalid) {
      return;
    }
    this.createServerErrors.clear();
    const request: OrganisationCreateUpdateRequest = {
      name: this.name.value,
    };
    this.organisationService.create(request).subscribe(
      response => {
        this.userService.get({ id: this.tokenStorageService.getUser().id}).subscribe((user) => {
          this.tokenStorageService.saveUser(user);
          this.router.navigate(['organisation/detail']);
        });
      },
      error => {
        this.createServerErrors.parse(error.error);
      }
    );
  }

  onJoinSubmit(): void {
    this.joinForm.markAllAsTouched();
    if (this.joinForm.invalid) {
      return;
    }
    this.joinServerErrors.clear();
    const request: OrganisationJoinRequest = {
      invite_code: this.inviteCode.value,
    };
    this.organisationService.join(request).subscribe(
      response => {
        this.userService.get({ id: this.tokenStorageService.getUser().id}).subscribe((user) => {
          this.tokenStorageService.saveUser(user);
          this.router.navigate(['organisation/detail']);
        });
      },
      error => {
        this.joinServerErrors.parse(error.error);
      }
    );
  }

  hasLocalValidationError(fieldName: string, formGroup: FormGroup): boolean {
    const control = formGroup.get(fieldName);
    return control.invalid && this.isTouched(fieldName, formGroup);
  }

  hasRemoteValidationError(fieldName: string, serverErrors: ServerErrorModel): boolean {
    return !!serverErrors.fieldErrors[fieldName];
  }

  hasValidationError(localName: string, remoteName: string, formGroup: FormGroup, serverErrors: ServerErrorModel): boolean {
    return this.hasLocalValidationError(localName, formGroup) || this.hasRemoteValidationError(remoteName, serverErrors);
  }

  removeRemoteValidationError(remoteName: string, serverErrors: ServerErrorModel): void {
    serverErrors.fieldErrors[remoteName] = undefined;
  }

  isTouched(fieldName: string, formGroup: FormGroup): boolean {
    const control = formGroup.get(fieldName);
    return control.dirty || control.touched;
  }

}
