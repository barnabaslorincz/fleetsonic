import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {TranslateModule} from '@ngx-translate/core';
import {OrganisationRoutingModule} from './organisation-routing.module';
import {CommonModule} from '@angular/common';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AlertModule} from 'ngx-bootstrap/alert';
import {ModalModule} from 'ngx-bootstrap/modal';
import {OrganisationComponent} from './organisation.component';
import {OrganisationCreateJoinComponent} from './organisation-create-join/organisation-create-join.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { OrganisationUserListComponent } from './organisation-user-list/organisation-user-list.component';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {TableSorterModule} from '../../shared-components/table-sorter/table-sorter.module';

@NgModule({
  imports: [
    FormsModule,
    OrganisationRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    TranslateModule,
    ReactiveFormsModule,
    CommonModule,
    NgxSpinnerModule,
    AlertModule,
    ModalModule,
    NgxPaginationModule,
    PaginationModule,
    TableSorterModule
  ],
  declarations: [
    OrganisationComponent,
    OrganisationCreateJoinComponent,
    OrganisationUserListComponent
  ]
})
export class OrganisationModule { }
