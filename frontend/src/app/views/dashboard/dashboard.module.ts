import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import { DashboardEventListComponent } from './dashboard-event-list/dashboard-event-list.component';
import { DashboardJourneyListComponent } from './dashboard-journey-list/dashboard-journey-list.component';

@NgModule({
    imports: [
        FormsModule,
        DashboardRoutingModule,
        ChartsModule,
        BsDropdownModule,
        ButtonsModule.forRoot(),
        TranslateModule,
        CommonModule
    ],
  declarations: [ DashboardComponent, DashboardEventListComponent, DashboardJourneyListComponent ]
})
export class DashboardModule { }
