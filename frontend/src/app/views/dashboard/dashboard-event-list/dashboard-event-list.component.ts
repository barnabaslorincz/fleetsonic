import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {PagingHelper} from '../../../utils/paging.helper';
import {SortingHelper} from '../../../utils/sorting.helper';
import {VehicleModel} from '../../../models/vehicle/vehicle.model';
import {EventModel} from '../../../models/event/event.model';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {EventService} from '../../../services/event/event.service';
import {FormBuilder} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';
import {ResourceList} from '../../../models/shared/resource-list';
import {Role} from '../../../models/role/role.model';
import {ImageStorageService} from '../../../services/image/image-storage.service';

@Component({
  selector: 'app-dashboard-event-list',
  templateUrl: './dashboard-event-list.component.html',
  styleUrls: ['./dashboard-event-list.component.scss']
})
export class DashboardEventListComponent implements OnInit {
  searchParams: HttpParams = new HttpParams();

  events: EventModel[] = [];

  constructor(private vehicleService: VehicleService,
              private eventService: EventService,
              private fb: FormBuilder,
              private sanitizer: DomSanitizer,
              private imageStorageService: ImageStorageService,
              private tokenStorageService: TokenStorageService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loadEvents();
  }

  loadEvents(): void {
    this.searchParams = this.searchParams.set('page_number', 1 + '');
    this.searchParams = this.searchParams.set('order_field', 'time');
    this.searchParams = this.searchParams.set('order_direction', 'DESC');
    this.searchParams = this.searchParams.set('number_of_items', 5 + '');
    const observable = this.eventService.getAll(this.searchParams);
    observable.subscribe((result: ResourceList<EventModel>) => {
      this.events = result.items;
    });
  }

  goToEventDetail(id: number, vehicleId: number) {
    this.router.navigate(['vehicles/' + vehicleId + '/events/' + id + '/detail']);
  }

  getEventTypeStringKey(event: EventModel): string {
    let eventTypeStringKey = event.event_type;
    if (event.vehicle.fuel_type && event.event_type === 'REFUELING_OR_CHARGING') {
      switch (event.vehicle.fuel_type) {
        case 'PETROL':
        case 'DIESEL':
        case 'PLUGIN_HYBRID':
        case 'HYBRID':
        case 'LPG':
        case 'CNG':
          eventTypeStringKey = 'REFUELING';
          break;
        case 'BATTERY_ELECTRIC':
          eventTypeStringKey = 'CHARGING';
          break;
      }
    }
    return eventTypeStringKey;
  }

  getImage(imageId?: number) {
    if (imageId) {
      return this.imageStorageService.getImage(imageId).image;
    }
    return '';
  }
}

