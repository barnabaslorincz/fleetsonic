import {Component, Input, OnInit} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {JourneyModel} from '../../../models/journey/journey.model';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {JourneyService} from '../../../services/journey/journey.service';
import {FormBuilder} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';
import {ResourceList} from '../../../models/shared/resource-list';
import {ImageStorageService} from '../../../services/image/image-storage.service';

@Component({
  selector: 'app-dashboard-journey-list',
  templateUrl: './dashboard-journey-list.component.html',
  styleUrls: ['./dashboard-journey-list.component.scss']
})
export class DashboardJourneyListComponent implements OnInit {
  searchParams: HttpParams = new HttpParams();

  journeys: JourneyModel[] = [];

  constructor(private vehicleService: VehicleService,
              private journeyService: JourneyService,
              private imageStorageService: ImageStorageService,
              private fb: FormBuilder,
              private sanitizer: DomSanitizer,
              private tokenStorageService: TokenStorageService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loadJourneys();
  }

  loadJourneys(): void {
    this.searchParams = this.searchParams.set('page_number', 1 + '');
    this.searchParams = this.searchParams.set('order_field', 'start_time');
    this.searchParams = this.searchParams.set('order_direction', 'DESC');
    this.searchParams = this.searchParams.set('number_of_items', '5');
    const observable = this.journeyService.getAll(this.searchParams);
    observable.subscribe((result: ResourceList<JourneyModel>) => {
      this.journeys = result.items;
    });
  }

  goToJourneyDetail(id: number, vehicleId: number) {
    this.router.navigate(['vehicles/' + vehicleId + '/journeys/' + id + '/detail']);
  }

  getKmCount(journey: JourneyModel): number | undefined {
    return journey.start_km && journey.finish_km ? journey.finish_km - journey.start_km : undefined;
  }

  getImage(imageId?: number) {
    if (imageId) {
      return this.imageStorageService.getImage(imageId).image;
    }
    return '';
  }
}
