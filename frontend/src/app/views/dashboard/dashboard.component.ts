import {Component, OnInit} from '@angular/core';
import {JourneyService} from '../../services/journey/journey.service';
import {EventService} from '../../services/event/event.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  eventCount: number = 0;
  journeyCount: number = 0;
  kmCount: number = 0;
  amountCount: number = 0;

  constructor(private eventService: EventService,
              private journeyService: JourneyService,) {
  }

  ngOnInit(): void {
    this.eventService.statCount().subscribe(eventCount => {
      this.eventCount = eventCount;
    });
    this.eventService.statAmountCount().subscribe(amountCount => {
      this.amountCount = amountCount;
    });
    this.journeyService.statCount().subscribe(journeyCount => {
      this.journeyCount = journeyCount;
    });
    this.journeyService.statKmCount().subscribe(kmCount => {
      this.kmCount = kmCount;
    });
  }

  get30DaysAgo() {
    const dateOffset = (24 * 60 * 60 * 1000) * 30;
    const date = new Date();
    date.setTime(date.getTime() - dateOffset);
    return date;
  }

  getToday() {
    return new Date();
  }
}
