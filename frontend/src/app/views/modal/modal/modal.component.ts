import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalService} from '../../../services/modal/modal.service';
import {ModalData} from '../../../models/modal/modal-data';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {
  @ViewChild('primaryModal') public primaryModal: ModalDirective;
  modalData: ModalData = new ModalData();
  modalSubscription: Subscription;

  constructor(private modalService: ModalService) { }

  ngOnInit(): void {
    this.modalSubscription = this.modalService.getModalSubject().subscribe((modalData: ModalData) => {
      this.modalData = modalData;
      this.primaryModal.show();
    });
  }

  ngOnDestroy(): void {
    this.modalSubscription.unsubscribe();
  }

  onModalClosed(value: boolean) {
    this.primaryModal.hide();
    this.modalService.onModalClosed(value);
  }

}
