import { Component } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ServerErrorModel} from '../../models/shared/server-error.model';
import {AuthService} from '../../services/authentication/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginRequest} from '../../models/login/login-request';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  loginForm = this.fb.group({
    userName: ['', Validators.required],
    password: ['', Validators.required],
  });

  get userName() { return this.loginForm.get('userName'); }
  get password() { return this.loginForm.get('password'); }

  serverErrors: ServerErrorModel = new ServerErrorModel();
  loginReason: string = undefined;

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private tokenStorageService: TokenStorageService) {
    this.loginReason = this.route.snapshot.paramMap.get('reason');
  }

  onInit(): void {
    this.tokenStorageService.signOut();
  }

  onSubmit(): void {
    this.loginForm.markAllAsTouched();
    if (this.loginForm.invalid) {
      return;
    }
    this.serverErrors.clear();
    const request: LoginRequest = {
      user_name: this.userName.value,
      password: this.password.value,
    };
    this.authService.login(request).subscribe(
      response => {
        this.tokenStorageService.saveToken(response.token);
        this.tokenStorageService.saveUser(response);
        this.router.navigate(['/dashboard']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  hasLocalValidationError(fieldName: string): boolean {
    const control = this.loginForm.get(fieldName);
    return control.invalid && this.isTouched(fieldName);
  }

  hasRemoteValidationError(fieldName: string): boolean {
    return !!this.serverErrors.fieldErrors[fieldName];
  }

  hasValidationError(localName, remoteName): boolean {
    return this.hasLocalValidationError(localName) || this.hasRemoteValidationError(remoteName);
  }

  removeRemoteValidationError(remoteName): void {
    this.serverErrors.fieldErrors[remoteName] = undefined;
  }

  isTouched(fieldName: string): boolean {
    const control = this.loginForm.get(fieldName);
    return control.dirty || control.touched;
  }

  onRegisterButtonClicked(): void {
    this.router.navigate(['/register']);
  }

}
