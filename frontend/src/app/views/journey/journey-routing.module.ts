import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {JourneyComponent} from './journey/journey.component';
import {JourneyListComponent} from './journey-list/journey-list.component';

const routes: Routes = [
  {
    path: '',
    component: JourneyListComponent,
    data: {
      title: 'JOURNEY_LIST'
    }
  },
  {
    path: 'create',
    component: JourneyComponent,
    data: {
      title: 'JOURNEY_CREATE'
    }
  },
  {
    path: ':id/edit',
    component: JourneyComponent,
    data: {
      title: 'JOURNEY_EDIT'
    }
  },
  {
    path: ':id/detail',
    component: JourneyComponent,
    data: {
      title: 'JOURNEY_DETAIL'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JourneyRoutingModule {}
