import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {LeafletDirective} from '@asymmetrik/ngx-leaflet';
import {circle, featureGroup, icon, latLng, Layer, marker, tileLayer} from 'leaflet';
import {Subscription} from 'rxjs';
import {JourneyModel} from '../../../models/journey/journey.model';
import {VehicleModel} from '../../../models/vehicle/vehicle.model';
import {ComponentStateHelper} from '../../../models/component-state/component-state-helper';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServerErrorModel} from '../../../models/shared/server-error.model';
import {UserService} from '../../../services/user/user.service';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {JourneyService} from '../../../services/journey/journey.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {Role} from '../../../models/role/role.model';
import { SelectUtils } from '../../../utils/select.utils';
import {JourneyCreateUpdateRequest} from '../../../models/journey/journey-create-update-request';

@Component({
  selector: 'app-journey',
  templateUrl: './journey.component.html',
  styleUrls: ['./journey.component.scss']
})
export class JourneyComponent implements OnInit, AfterViewInit, OnDestroy {
  SelectUtils = SelectUtils;
  alerts: any = [];
  date: Date = new Date();

  @ViewChild(LeafletDirective) leaflet;

  leafletOptions = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
    ],
    center: latLng(47.4979, 19.0402),
    zoom: 13
  };
  leafletCenter = latLng({ lat: 47.4979, lng: 19.0402 });
  leafletMarkers: Layer[] = [];


  id: number;
  vehicleId?: number;
  private routeSubscription: Subscription;
  journeyModel: JourneyModel = new JourneyModel();
  vehicleModel: VehicleModel = new VehicleModel();
  componentStateHelper: ComponentStateHelper;

  journeyForm = this.fb.group({
    startTime: ['', Validators.required],
    finishTime: [''],
    startKm: ['', Validators.required],
    finishKm: [''],
    startLatitude: [''],
    startLongitude: [''],
    finishLatitude: [''],
    finishLongitude: [''],
    description: [''],
  }, { validator: [this.kmRangeValidator, this.dateRangeValidator]});

  get startTime() { return this.journeyForm.get('startTime'); }
  get finishTime() { return this.journeyForm.get('finishTime'); }
  get startKm() { return this.journeyForm.get('startKm'); }
  get finishKm() { return this.journeyForm.get('finishKm'); }
  get startLatitude() { return this.journeyForm.get('startLatitude'); }
  get startLongitude() { return this.journeyForm.get('startLongitude'); }
  get finishLatitude() { return this.journeyForm.get('finishLatitude'); }
  get finishLongitude() { return this.journeyForm.get('finishLongitude'); }
  get description() { return this.journeyForm.get('description'); }

  serverErrors: ServerErrorModel = new ServerErrorModel();

  constructor(private userService: UserService,
              private vehicleService: VehicleService,
              private journeyService: JourneyService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private tokenStorageService: TokenStorageService) { }

  ngOnInit() {
    this.routeSubscription = this.route.url.subscribe(urlSegments => {
      this.vehicleId = +this.route.snapshot.paramMap.get('vehicleId');
      if (this.vehicleId) {
        this.loadVehicle();
      }
      this.componentStateHelper = new ComponentStateHelper(urlSegments[urlSegments.length - 1].path);
      if (!this.componentStateHelper.isCreateView()) {
        this.id = +urlSegments[0].path;
        this.loadJourney();
      } else {
        this.setUpMap();
      }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  loadVehicle(): void {
    this.vehicleService.get({ id: this.vehicleId }).subscribe((vehicleModel: VehicleModel) => {
      this.vehicleModel = vehicleModel;
    });
  }

  loadJourney(): void {
    this.journeyService.get(this.vehicleId, { id: this.id }).subscribe((journeyModel: JourneyModel) => {
      this.journeyModel = journeyModel;
      this.startTime.setValue(journeyModel.start_time);
      this.finishTime.setValue(journeyModel.finish_time);
      this.startKm.setValue(journeyModel.start_km);
      this.finishKm.setValue(journeyModel.finish_km);
      this.startLatitude.setValue(journeyModel.start_latitude);
      this.startLongitude.setValue(journeyModel.start_longitude);
      this.finishLatitude.setValue(journeyModel.finish_latitude);
      this.finishLongitude.setValue(journeyModel.finish_longitude);
      this.description.setValue(journeyModel.description);
      this.setUpMap();
    });
  }

  onSubmit(): void {
    this.journeyForm.markAllAsTouched();
    if (this.journeyForm.invalid) {
      return;
    }
    this.serverErrors.clear();
    if (this.componentStateHelper.isCreateView()) {
      this.createJourney();
    } else {
      this.updateJourney();
    }
  }

  createJourney() {
    this.journeyService.create(this.vehicleId, this.createJourneyCreateUpdateRequest()).subscribe(
      response => {
        this.router.navigate(['vehicles/' + this.vehicleId + '/journeys/' + response.id + '/detail']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  updateJourney() {
    this.journeyService.update(this.vehicleId, this.id, this.createJourneyCreateUpdateRequest()).subscribe(
      response => {
        this.router.navigate(['vehicles/' + this.vehicleId + '/journeys/' + this.id + '/detail']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  hasLocalValidationError(fieldName: string): boolean {
    const control = this.journeyForm.get(fieldName);
    return control.invalid && this.isTouched(fieldName);
  }

  hasRemoteValidationError(fieldName: string): boolean {
    return !!this.serverErrors.fieldErrors[fieldName];
  }

  hasValidationError(localName, remoteName): boolean {
    return this.hasLocalValidationError(localName) || this.hasRemoteValidationError(remoteName);
  }

  removeRemoteValidationError(remoteName): void {
    this.serverErrors.fieldErrors[remoteName] = undefined;
  }

  isTouched(fieldName: string): boolean {
    const control = this.journeyForm.get(fieldName);
    return control.dirty || control.touched;
  }

  navigateBack(): void {
    const uri = this.id ? 'vehicles/' + this.vehicleId + '/journeys/' + this.id + '/detail' : 'vehicles/' + this.vehicleId + '/journeys/';
    this.router.navigate([uri]);
  }

  edit() {
    this.router.navigate(['vehicles/' + this.vehicleId + '/journeys/' + this.id + '/edit']);
  }

  isMine(): boolean {
    return this.tokenStorageService.getUser().user_name === this.journeyModel.creator_user;
  }

  isAdmin(): boolean {
    return Role[this.tokenStorageService.getUser().role as keyof typeof Role] === Role.ADMIN;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.leaflet.getMap().invalidateSize();
    }, 250);
  }

  setUpMap() {
    if (!this.componentStateHelper.isCreateView() && this.journeyModel.start_latitude && this.journeyModel.start_longitude) {
      this.addMarker(this.journeyModel.start_latitude, this.journeyModel.start_longitude, 'red');
      if (this.journeyModel.start_latitude && this.journeyModel.start_longitude) {
        this.addMarker(this.journeyModel.finish_latitude, this.journeyModel.finish_longitude, 'green');
      }
      this.leaflet.getMap().fitBounds(featureGroup(this.leafletMarkers).getBounds());
    } else if (!this.componentStateHelper.isReadonlyView() && (!this.journeyModel.start_latitude || !this.journeyModel.start_longitude)) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.leafletCenter = latLng(position.coords.latitude, position.coords.longitude);
        });
      } else {
        this.leafletCenter = latLng({ lat: 47.4979, lng: 19.0402 });
      }
    }
  }

  addMarker(lat: number, lon: number, color: string) {
    const iconUrl = `assets/markers/marker-icon-2x-${color}.png`;
    this.leafletMarkers.push(marker(
      [lat, lon],
      {
        icon: icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl,
          shadowUrl: 'assets/marker-shadow.png'
        })
      }
    ));
  }

  onMapClick($event) {
    if (!this.componentStateHelper.isReadonlyView()) {
      if (this.leafletMarkers.length === 1) {
        this.addMarker($event.latlng.lat, $event.latlng.lng, 'green');
        this.finishLatitude.setValue($event.latlng.lat);
        this.finishLongitude.setValue($event.latlng.lng);
      } else {
        this.leafletMarkers.splice(0, this.leafletMarkers.length);
        this.addMarker($event.latlng.lat, $event.latlng.lng, 'red');
        this.startLatitude.setValue($event.latlng.lat);
        this.startLongitude.setValue($event.latlng.lng);
        this.finishLatitude.setValue('');
        this.finishLongitude.setValue('');
      }
    }
  }

  resetMarkers() {
    this.leafletMarkers.splice(0, this.leafletMarkers.length);
    this.startLatitude.setValue('');
    this.startLongitude.setValue('');
  }

  private createJourneyCreateUpdateRequest(): JourneyCreateUpdateRequest {
    return {
      start_time: this.startTime.value,
      finish_time: this.finishTime.value,
      start_km: this.startKm.value,
      finish_km: this.finishKm.value,
      start_latitude: this.startLatitude.value,
      start_longitude: this.startLongitude.value,
      finish_latitude: this.finishLatitude.value,
      finish_longitude: this.finishLongitude.value,
      description: this.description.value,
    };
  }

  kmRangeValidator(formGroup: FormGroup) {
    const start = formGroup.get('startKm').value;
    const end = formGroup.get('finishKm').value;
    return (start !== null && end !== null && start <= end) || (end === '' || end === null)
      ? null
      : { kmRange: true };
  }

  dateRangeValidator(formGroup: FormGroup) {
    const start = formGroup.get('startTime').value;
    const end = formGroup.get('finishTime').value;
    return (start !== null && end !== null && start <= end) || (end === '' || end === null)
      ? null
      : { dateRange: true };
  }

  navigateToUserDetail(userId: number) {
    this.router.navigate(['users/' + userId + '/detail']);
  }
}
