import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AlertModule} from 'ngx-bootstrap/alert';
import {ModalModule} from 'ngx-bootstrap/modal';
import {JourneyRoutingModule} from './journey-routing.module';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {TableSorterModule} from '../../shared-components/table-sorter/table-sorter.module';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {VehicleModule} from '../vehicle/vehicle.module';
import {JourneyComponent} from './journey/journey.component';
import { JourneyListComponent } from './journey-list/journey-list.component';

@NgModule({
  imports: [
    FormsModule,
    JourneyRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    TranslateModule,
    ReactiveFormsModule,
    CommonModule,
    NgxSpinnerModule,
    AlertModule,
    ModalModule,
    PaginationModule,
    TableSorterModule,
    LeafletModule,
    VehicleModule
  ],
  declarations: [
    JourneyComponent,
    JourneyListComponent
  ]
})
export class JourneyModule { }
