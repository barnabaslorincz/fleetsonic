import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AlertModule} from 'ngx-bootstrap/alert';
import {ModalModule} from 'ngx-bootstrap/modal';
import {VehicleComponent} from './vehicle.component';
import {VehicleListComponent} from './vehicle-list/vehicle-list.component';
import {VehicleRoutingModule} from './vehicle-routing.module';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {TableSorterModule} from '../../shared-components/table-sorter/table-sorter.module';
import { VehicleEventListComponent } from './vehicle-event-list/vehicle-event-list.component';
import {TabsModule} from 'ngx-bootstrap/tabs';
import { VehicleJourneyListComponent } from './vehicle-journey-list/vehicle-journey-list.component';

@NgModule({
    imports: [
        FormsModule,
        VehicleRoutingModule,
        ChartsModule,
        BsDropdownModule,
        ButtonsModule.forRoot(),
        TranslateModule,
        ReactiveFormsModule,
        CommonModule,
        NgxSpinnerModule,
        AlertModule,
        ModalModule,
        PaginationModule,
        TableSorterModule,
        TabsModule
    ],
  exports: [
    VehicleEventListComponent,
    VehicleJourneyListComponent
  ],
  declarations: [
    VehicleComponent,
    VehicleListComponent,
    VehicleEventListComponent,
    VehicleJourneyListComponent
  ]
})
export class VehicleModule { }
