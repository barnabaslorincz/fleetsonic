import {Component, Input, OnInit} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {PagingHelper} from '../../../utils/paging.helper';
import {SortingHelper} from '../../../utils/sorting.helper';
import {VehicleModel} from '../../../models/vehicle/vehicle.model';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {FormBuilder, FormGroup, ValidatorFn} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';
import {ResourceList} from '../../../models/shared/resource-list';
import {Role} from '../../../models/role/role.model';
import { SelectUtils } from '../../../utils/select.utils';
import {JourneyModel} from '../../../models/journey/journey.model';
import {JourneyService} from '../../../services/journey/journey.service';
import {ImageStorageService} from '../../../services/image/image-storage.service';

@Component({
  selector: 'app-vehicle-journey-list',
  templateUrl: './vehicle-journey-list.component.html',
  styleUrls: ['./vehicle-journey-list.component.scss']
})
export class VehicleJourneyListComponent implements OnInit {
  SelectUtils = SelectUtils;
  searchParams: HttpParams = new HttpParams();
  pagingHelper: PagingHelper = new PagingHelper();
  sortingHelper: SortingHelper = new SortingHelper();
  alerts: any = [];

  @Input()
  vehicleModel?: VehicleModel;

  searchForm = this.fb.group({
    startTimeFrom: [''],
    startTimeTo: [''],
    finishTimeFrom: [''],
    finishTimeTo: [''],
    description: [''],
    licensePlate: [''],
    startKmFrom: [''],
    startKmTo: [''],
    finishKmFrom: [''],
    finishKmTo: [''],
    userName: [''],
  });

  get startTimeFrom() { return this.searchForm.get('startTimeFrom'); }
  get startTimeTo() { return this.searchForm.get('startTimeTo'); }
  get finishTimeFrom() { return this.searchForm.get('finishTimeFrom'); }
  get finishTimeTo() { return this.searchForm.get('finishTimeTo'); }
  get description() { return this.searchForm.get('description'); }
  get licensePlate() { return this.searchForm.get('licensePlate'); }
  get startKmFrom() { return this.searchForm.get('startKmFrom'); }
  get startKmTo() { return this.searchForm.get('startKmTo'); }
  get finishKmFrom() { return this.searchForm.get('finishKmFrom'); }
  get finishKmTo() { return this.searchForm.get('finishKmTo'); }
  get userName() { return this.searchForm.get('userName'); }

  get searchButtonStringKey () { return this.isSearchOpen ? 'CLOSE_SEARCH' : 'OPEN_SEARCH'; }

  journeys: JourneyModel[] = [];
  isSearchOpen: boolean = false;

  constructor(private vehicleService: VehicleService,
              private journeyService: JourneyService,
              private fb: FormBuilder,
              private sanitizer: DomSanitizer,
              private imageStorageService: ImageStorageService,
              private tokenStorageService: TokenStorageService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loadJourneys();
  }

  loadJourneys(): void {
    this.searchParams = this.searchParams.set('page_number', this.pagingHelper.currentPage + '');
    this.searchParams = this.searchParams.set('order_field', this.sortingHelper.orderField);
    this.searchParams = this.searchParams.set('order_direction', this.sortingHelper.orderDirection);
    const observable = this.vehicleModel ?
      this.journeyService.getForVehicle(this.vehicleModel.id, this.searchParams) : this.journeyService.getAll(this.searchParams);
    observable.subscribe((result: ResourceList<JourneyModel>) => {
      this.journeys = result.items;
      this.pagingHelper.totalItems = result.paging_result.current_number_of_items;
    });
  }

  isAdmin(role: string): boolean {
    return Role[role as keyof typeof Role] === Role.ADMIN;
  }

  isCurrentUserAdmin(): boolean {
    return Role[this.tokenStorageService.getUser().role as keyof typeof Role] === Role.ADMIN;
  }

  toggleSearch() {
    this.isSearchOpen = !this.isSearchOpen;
  }

  search() {
    let params = new HttpParams();
    if (this.startTimeFrom.value) {
      params = params.append('start_time_from', this.startTimeFrom.value);
    }
    if (this.startTimeTo.value) {
      params = params.append('start_time_to', this.startTimeTo.value);
    }
    if (this.finishTimeFrom.value) {
      params = params.append('finish_time_from', this.finishTimeFrom.value);
    }
    if (this.finishTimeTo.value) {
      params = params.append('finish_time_to', this.finishTimeTo.value);
    }
    if (this.startKmFrom.value) {
      params = params.append('start_km_from', this.startKmFrom.value);
    }
    if (this.startKmTo.value) {
      params = params.append('start_km_to', this.startKmTo.value);
    }
    if (this.finishKmFrom.value) {
      params = params.append('finish_km_from', this.finishKmFrom.value);
    }
    if (this.finishKmTo.value) {
      params = params.append('finish_km_to', this.finishKmTo.value);
    }
    if (this.description.value) {
      params = params.append('description', this.description.value);
    }
    if (this.licensePlate.value) {
      params = params.append('license_plate', this.licensePlate.value);
    }
    if (this.userName.value) {
      params = params.append('user_name', this.userName.value);
    }
    this.searchParams = params;
    this.loadJourneys();
  }

  resetSearch() {
    this.sortingHelper.reset();
    this.pagingHelper.reset();
    this.searchForm.reset();
    this.search();
  }

  pageChanged($event: any) {
    this.pagingHelper.currentPage = $event.page;
    this.loadJourneys();
  }

  goToJourneyDetail(id: number, vehicleId: number) {
    this.router.navigate(['vehicles/' + vehicleId + '/journeys/' + id + '/detail']);
  }

  navigateToCreate() {
    this.router.navigate(['vehicles/' + this.vehicleModel.id + '/journeys/create']);
  }

  hasLocation(journey: JourneyModel): boolean {
    return !!(journey.start_latitude && journey.start_longitude);
  }

  getKmCount(journey: JourneyModel): number | undefined {
    return journey.start_km && journey.finish_km ? journey.finish_km - journey.start_km : undefined;
  }

  navigateToUserDetail(userId: number) {
    this.router.navigate(['users/' + userId + '/detail']);
  }

  getImage(imageId?: number) {
    if (imageId) {
      return this.imageStorageService.getImage(imageId).image;
    }
    return '';
  }
}
