import {Component, Input, OnInit} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {PagingHelper} from '../../../utils/paging.helper';
import {SortingHelper} from '../../../utils/sorting.helper';
import {Role} from '../../../models/role/role.model';
import {OrganisationService} from '../../../services/organisation/organisation.service';
import {FormBuilder} from '@angular/forms';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {ResourceList} from '../../../models/shared/resource-list';
import {SelectUtils} from '../../../utils/select.utils';
import {VehicleModel} from '../../../models/vehicle/vehicle.model';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {ImageService} from '../../../services/image/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {ImageStorageService} from '../../../services/image/image-storage.service';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss']
})
export class VehicleListComponent implements OnInit {
  SelectUtils = SelectUtils;
  searchParams: HttpParams = new HttpParams();
  pagingHelper: PagingHelper = new PagingHelper();
  sortingHelper: SortingHelper = new SortingHelper();
  alerts: any = [];

  @Input()
  organisationId: number;

  fuelTypes = [
    { key: 'NONE', value: null},
    { key: 'PETROL', value: 'PETROL'},
    { key: 'DIESEL', value: 'DIESEL'},
    { key: 'BATTERY_ELECTRIC', value: 'BATTERY_ELECTRIC'},
    { key: 'PLUGIN_HYBRID', value: 'PLUGIN_HYBRID'},
    { key: 'HYBRID', value: 'HYBRID'},
    { key: 'LPG', value: 'LPG'},
    { key: 'CNG', value: 'CNG'},
  ];

  searchForm = this.fb.group({
    make: [''],
    model: [''],
    licensePlate: [''],
    kmCounterFrom: [''],
    kmCounterTo: [''],
    inspectionValidityFrom: [''],
    inspectionValidityTo: [''],
    fuelType: [this.fuelTypes[0].value],
    userName: [''],
  });

  get make() { return this.searchForm.get('make'); }
  get model() { return this.searchForm.get('model'); }
  get licensePlate() { return this.searchForm.get('licensePlate'); }
  get kmCounterFrom() { return this.searchForm.get('kmCounterFrom'); }
  get kmCounterTo() { return this.searchForm.get('kmCounterTo'); }
  get inspectionValidityFrom() { return this.searchForm.get('inspectionValidityFrom'); }
  get inspectionValidityTo() { return this.searchForm.get('inspectionValidityTo'); }
  get fuelType() { return this.searchForm.get('fuelType'); }
  get userName() { return this.searchForm.get('userName'); }

  get searchButtonStringKey () { return this.isSearchOpen ? 'CLOSE_SEARCH' : 'OPEN_SEARCH'; }

  vehicles: VehicleModel[] = [];
  isSearchOpen: boolean = false;

  constructor(private vehicleService: VehicleService,
              private organisationService: OrganisationService,
              private fb: FormBuilder,
              private sanitizer: DomSanitizer,
              private imageService: ImageService,
              private imageStorageService: ImageStorageService,
              private tokenStorageService: TokenStorageService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loadVehicles();
  }

  loadVehicles(): void {
    this.searchParams = this.searchParams.set('page_number', this.pagingHelper.currentPage + '');
    this.searchParams = this.searchParams.set('order_field', this.sortingHelper.orderField);
    this.searchParams = this.searchParams.set('order_direction', this.sortingHelper.orderDirection);
    this.vehicleService.getAll(this.searchParams).subscribe((result: ResourceList<VehicleModel>) => {
      this.vehicles = result.items;
      this.pagingHelper.totalItems = result.paging_result.current_number_of_items;
    });
  }

  isAdmin(role: string): boolean {
    return Role[role as keyof typeof Role] === Role.ADMIN;
  }

  isCurrentUserAdmin(): boolean {
    return Role[this.tokenStorageService.getUser().role as keyof typeof Role] === Role.ADMIN;
  }

  toggleSearch() {
    this.isSearchOpen = !this.isSearchOpen;
  }

  search() {
    let params = new HttpParams();
    if (this.make.value) {
      params = params.append('make', this.make.value);
    }
    if (this.model.value) {
      params = params.append('model', this.model.value);
    }
    if (this.licensePlate.value) {
      params = params.append('license_plate', this.licensePlate.value);
    }
    if (this.kmCounterFrom.value) {
      params = params.append('km_counter_from', this.kmCounterFrom.value);
    }
    if (this.kmCounterTo.value) {
      params = params.append('km_counter_to', this.kmCounterTo.value);
    }
    if (this.inspectionValidityFrom.value) {
      params = params.append('inspection_validity_from', this.inspectionValidityFrom.value);
    }
    if (this.inspectionValidityTo.value) {
      params = params.append('inspection_validity_to', this.inspectionValidityTo.value);
    }
    if (this.fuelType.value) {
      params = params.append('fuel_type', this.fuelType.value);
    }
    if (this.userName.value) {
      params = params.append('user_name', this.userName.value);
    }
    this.searchParams = params;
    this.loadVehicles();
  }

  resetSearch() {
    this.sortingHelper.reset();
    this.pagingHelper.reset();
    this.searchForm.reset();
    this.search();
  }

  pageChanged($event: any) {
    this.pagingHelper.currentPage = $event.page;
    this.loadVehicles();
  }

  goToVehicleDetail(id: number) {
    this.router.navigate(['vehicles/' + id + '/detail']);
  }

  navigateToCreate() {
    this.router.navigate(['vehicles/create']);
  }

  navigateToUserDetail(userId: number) {
    this.router.navigate(['users/' + userId + '/detail']);
  }

  getImage(imageId?: number) {
    if (imageId) {
      return this.imageStorageService.getImage(imageId).image;
    }
    return '';
  }
}
