import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ComponentStateHelper} from '../../models/component-state/component-state-helper';
import {FormBuilder, Validators} from '@angular/forms';
import {ServerErrorModel} from '../../models/shared/server-error.model';
import {UserService} from '../../services/user/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {ImageService} from '../../services/image/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {VehicleModel} from '../../models/vehicle/vehicle.model';
import {VehicleService} from '../../services/vehicle/vehicle.service';
import {VehicleUpdateRequest} from '../../models/vehicle/vehicle-update-request';
import {VehicleCreateRequest} from '../../models/vehicle/vehicle-create-request';
import {Role} from '../../models/role/role.model';
import { SelectUtils } from '../../utils/select.utils';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit, OnDestroy {
  SelectUtils = SelectUtils;
  alerts: any = [];

  fuelTypes = [
    'PETROL',
    'DIESEL',
    'BATTERY_ELECTRIC',
    'PLUGIN_HYBRID',
    'HYBRID',
    'LPG',
    'CNG',
  ];

  id: number;
  vehiclePictureUrl = null;
  vehiclePicturePlaceholderUrl = 'assets/img/car_image_placeholder.png';
  private routeSubscription: Subscription;
  vehicleModel: VehicleModel = new VehicleModel();
  componentStateHelper: ComponentStateHelper;

  vehicleForm = this.fb.group({
    make: ['', Validators.required],
    model: ['', Validators.required],
    licensePlate: ['', Validators.required],
    fuelType: ['', Validators.required],
    fuelCapacity: [''],
    kmCounter: ['', Validators.required],
    inspectionValidity: [''],
    type: ['private'],
  });

  uploadForm = this.fb.group({
    vehiclePictureUpload: ['']
  });

  get make() { return this.vehicleForm.get('make'); }
  get model() { return this.vehicleForm.get('model'); }
  get licensePlate() { return this.vehicleForm.get('licensePlate'); }
  get fuelType() { return this.vehicleForm.get('fuelType'); }
  get fuelCapacity() { return this.vehicleForm.get('fuelCapacity'); }
  get kmCounter() { return this.vehicleForm.get('kmCounter'); }
  get inspectionValidity() { return this.vehicleForm.get('inspectionValidity'); }
  get type() { return this.vehicleForm.get('type'); }

  get vehiclePictureUpload() { return this.uploadForm.get('vehiclePictureUpload'); }

  serverErrors: ServerErrorModel = new ServerErrorModel();

  constructor(private userService: UserService,
              private vehicleService: VehicleService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private tokenStorageService: TokenStorageService,
              private imageService: ImageService,
              private sanitizer: DomSanitizer,
              private translateService: TranslateService) { }

  ngOnInit() {
    this.routeSubscription = this.route.url.subscribe(urlSegments => {
      this.componentStateHelper = new ComponentStateHelper(urlSegments[urlSegments.length - 1].path);
      if (!this.componentStateHelper.isCreateView()) {
        this.id = +urlSegments[0].path;
        this.loadVehicle();
      }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  loadVehicle(): void {
    this.vehicleService.get({ id: this.id }).subscribe((vehicleModel: VehicleModel) => {
      this.vehicleModel = vehicleModel;
      this.make.setValue(vehicleModel.make);
      this.model.setValue(vehicleModel.model);
      this.licensePlate.setValue(vehicleModel.license_plate);
      this.fuelType.setValue(vehicleModel.fuel_type);
      this.fuelCapacity.setValue(vehicleModel.fuel_capacity);
      this.kmCounter.setValue(vehicleModel.km_counter);
      this.inspectionValidity.setValue(vehicleModel.inspection_validity);
      if (vehicleModel.image_id) {
        this.loadPicture(vehicleModel.image_id);
      }
    });
  }

  loadPicture(imagedId: number): void {
    this.imageService.get({ id: imagedId }).subscribe((blob: any) => {
      const objectURL = URL.createObjectURL(blob);
      this.vehiclePictureUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      this.vehiclePictureUpload.setValue('');
    });
  }

  onSubmit(): void {
    this.vehicleForm.markAllAsTouched();
    if (this.vehicleForm.invalid) {
      return;
    }
    this.serverErrors.clear();
    if (this.componentStateHelper.isCreateView()) {
      this.createVehicle();
    } else {
      this.updateVehicle();
    }
  }

  createVehicle() {
    const request: VehicleCreateRequest = {
      make: this.make.value,
      model: this.model.value,
      license_plate: this.licensePlate.value,
      fuel_capacity: this.fuelCapacity.value,
      fuel_type: this.fuelType.value,
      km_counter: this.kmCounter.value,
      inspection_validity: this.inspectionValidity.value,
      organisation_id: this.getOrganisationId(),
      owner_id: this.getOwnerId()
    };
    this.vehicleService.create(request).subscribe(
      response => {
        this.router.navigate(['vehicles/' + response.id + '/detail']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  updateVehicle() {
    const request: VehicleUpdateRequest = {
      id: this.id,
      license_plate: this.licensePlate.value,
      fuel_capacity: this.fuelCapacity.value,
      inspection_validity: this.inspectionValidity.value
    };
    this.vehicleService.update(request).subscribe(
      response => {
        this.router.navigate(['vehicles/' + request.id + '/detail']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  hasLocalValidationError(fieldName: string): boolean {
    const control = this.vehicleForm.get(fieldName);
    return control.invalid && this.isTouched(fieldName);
  }

  hasRemoteValidationError(fieldName: string): boolean {
    return !!this.serverErrors.fieldErrors[fieldName];
  }

  hasValidationError(localName, remoteName): boolean {
    return this.hasLocalValidationError(localName) || this.hasRemoteValidationError(remoteName);
  }

  removeRemoteValidationError(remoteName): void {
    this.serverErrors.fieldErrors[remoteName] = undefined;
  }

  isTouched(fieldName: string): boolean {
    const control = this.vehicleForm.get(fieldName);
    return control.dirty || control.touched;
  }

  navigateBack(): void {
    const uri = this.id ? 'vehicles/' + this.id + '/detail' : 'vehicles/';
    this.router.navigate([uri]);
  }

  edit() {
    this.router.navigate(['vehicles/' + this.id + '/edit']);
  }

  deletePicture() {
    this.vehicleService.updateImage(this.vehicleModel.id).subscribe(() => {
      this.vehiclePictureUrl = null;
      this.vehiclePictureUpload.setValue('');
    }, error => {
      this.onUploadError();
    });
  }

  isMine(): boolean {
    return this.tokenStorageService.getUser().user_name === this.vehicleModel.creator_user;
  }

  isAdmin(): boolean {
    return Role[this.tokenStorageService.getUser().role as keyof typeof Role] === Role.ADMIN;
  }

  hasOrganisation(): boolean {
    return this.tokenStorageService.getUser().organisation;
  }

  getOrganisationId(): number | undefined {
    if (this.type.value === 'organisational' && this.tokenStorageService.getUser().organisation) {
      return this.tokenStorageService.getUser().organisation.id;
    }
    return undefined;
  }

  getOwnerId(): number | undefined {
    if (this.type.value === 'private') {
      return this.tokenStorageService.getUser().id;
    }
    return undefined;
  }

  handleFileInput(files: FileList) {
    if (files[0]) {
      this.imageService.uploadImage(files[0]).subscribe(imageId => {
        this.vehicleService.updateImage(this.vehicleModel.id, { id: imageId}).subscribe(() => {
          this.onUploadSuccess();
          this.loadPicture(imageId);
        }, error => {
          this.onUploadError();
        });
      }, error => {
        console.log(error);
        this.onUploadError();
      });
    }
  }

  onUploadError(): void {
    this.alerts.push({
      type: 'danger',
      msg: this.translateService.instant('ALERT_FAILED_TO_UPLOAD_IMAGE'),
      timeout: 5000
    });
    this.vehiclePictureUpload.setValue('');
  }

  onUploadSuccess(): void {
    this.alerts.push({
      type: 'success',
      msg: this.translateService.instant('ALERT_SUCCESSFUL_IMAGE_UPLOAD'),
      timeout: 3000
    });
    this.vehiclePictureUpload.setValue('');
  }

  navigateToUserDetail(userId: number) {
    this.router.navigate(['users/' + userId + '/detail']);
  }

  getFuelTypeMeasurement(): string {
    let fuelTypeMeasurement = '';
    if (this.fuelType.value) {
      switch (this.fuelType.value) {
        case 'PETROL':
        case 'DIESEL':
        case 'PLUGIN_HYBRID':
        case 'HYBRID':
        case 'LPG':
        case 'CNG':
          fuelTypeMeasurement = 'liter';
          break;
        case 'BATTERY_ELECTRIC':
          fuelTypeMeasurement = 'kWh';
          break;
      }
    }
    return fuelTypeMeasurement;
  }

}
