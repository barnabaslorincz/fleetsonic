import {Component, Input, OnInit} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {PagingHelper} from '../../../utils/paging.helper';
import {SortingHelper} from '../../../utils/sorting.helper';
import {VehicleService} from '../../../services/vehicle/vehicle.service';
import {FormBuilder} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';
import {ResourceList} from '../../../models/shared/resource-list';
import {Role} from '../../../models/role/role.model';
import { SelectUtils } from '../../../utils/select.utils';
import {EventModel} from '../../../models/event/event.model';
import {EventService} from '../../../services/event/event.service';
import {VehicleModel} from '../../../models/vehicle/vehicle.model';
import {ImageService} from '../../../services/image/image.service';
import {ImageStorageService} from '../../../services/image/image-storage.service';

@Component({
  selector: 'app-vehicle-event-list',
  templateUrl: './vehicle-event-list.component.html',
  styleUrls: ['./vehicle-event-list.component.scss']
})
export class VehicleEventListComponent implements OnInit {
  SelectUtils = SelectUtils;
  searchParams: HttpParams = new HttpParams();
  pagingHelper: PagingHelper = new PagingHelper();
  sortingHelper: SortingHelper = new SortingHelper();
  alerts: any = [];

  @Input()
  vehicleModel?: VehicleModel;

  eventTypes = [
    'REFUELING_OR_CHARGING',
    'CLEANING',
    'MAINTENANCE',
    'INCIDENT'
  ];

  searchForm = this.fb.group({
    eventType: [''],
    timeFrom: [''],
    timeTo: [''],
    description: [''],
    licensePlate: [''],
    kmCounterFrom: [''],
    kmCounterTo: [''],
    priceFrom: [''],
    priceTo: [''],
    userName: [''],
  });

  get eventType() { return this.searchForm.get('eventType'); }
  get timeFrom() { return this.searchForm.get('timeFrom'); }
  get timeTo() { return this.searchForm.get('timeTo'); }
  get description() { return this.searchForm.get('description'); }
  get licensePlate() { return this.searchForm.get('licensePlate'); }
  get kmCounterFrom() { return this.searchForm.get('kmCounterFrom'); }
  get kmCounterTo() { return this.searchForm.get('kmCounterTo'); }
  get priceFrom() { return this.searchForm.get('priceFrom'); }
  get priceTo() { return this.searchForm.get('priceTo'); }
  get userName() { return this.searchForm.get('userName'); }

  get searchButtonStringKey () { return this.isSearchOpen ? 'CLOSE_SEARCH' : 'OPEN_SEARCH'; }

  events: EventModel[] = [];
  isSearchOpen: boolean = false;

  constructor(private vehicleService: VehicleService,
              private eventService: EventService,
              private fb: FormBuilder,
              private sanitizer: DomSanitizer,
              private imageStorageService: ImageStorageService,
              private tokenStorageService: TokenStorageService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loadEvents();
  }

  loadEvents(): void {
    this.searchParams = this.searchParams.set('page_number', this.pagingHelper.currentPage + '');
    this.searchParams = this.searchParams.set('order_field', this.sortingHelper.orderField);
    this.searchParams = this.searchParams.set('order_direction', this.sortingHelper.orderDirection);
    const observable = this.vehicleModel ?
      this.eventService.getForVehicle(this.vehicleModel.id, this.searchParams) : this.eventService.getAll(this.searchParams);
    observable.subscribe((result: ResourceList<EventModel>) => {
      this.events = result.items;
      this.pagingHelper.totalItems = result.paging_result.current_number_of_items;
    });
  }

  isAdmin(role: string): boolean {
    return Role[role as keyof typeof Role] === Role.ADMIN;
  }

  isCurrentUserAdmin(): boolean {
    return Role[this.tokenStorageService.getUser().role as keyof typeof Role] === Role.ADMIN;
  }

  toggleSearch() {
    this.isSearchOpen = !this.isSearchOpen;
  }

  search() {
    let params = new HttpParams();
    if (this.eventType.value) {
      params = params.append('event_type', this.eventType.value);
    }
    if (this.timeFrom.value) {
      params = params.append('time_from', this.timeFrom.value);
    }
    if (this.timeTo.value) {
      params = params.append('time_to', this.timeTo.value);
    }
    if (this.description.value) {
      params = params.append('description', this.description.value);
    }
    if (this.licensePlate.value) {
      params = params.append('license_plate', this.licensePlate.value);
    }
    if (this.kmCounterFrom.value) {
      params = params.append('km_counter_from', this.kmCounterFrom.value);
    }
    if (this.kmCounterTo.value) {
      params = params.append('km_counter_to', this.kmCounterTo.value);
    }
    if (this.priceFrom.value) {
      params = params.append('price_from', this.priceFrom.value);
    }
    if (this.priceTo.value) {
      params = params.append('price_to', this.priceTo.value);
    }
    if (this.userName.value) {
      params = params.append('user_name', this.userName.value);
    }
    this.searchParams = params;
    this.loadEvents();
  }

  resetSearch() {
    this.sortingHelper.reset();
    this.pagingHelper.reset();
    this.searchForm.reset();
    this.search();
  }

  pageChanged($event: any) {
    this.pagingHelper.currentPage = $event.page;
    this.loadEvents();
  }

  goToEventDetail(id: number, vehicleId: number) {
    this.router.navigate(['vehicles/' + vehicleId + '/events/' + id + '/detail']);
  }

  navigateToCreate() {
    this.router.navigate(['vehicles/' + this.vehicleModel.id + '/events/create']);
  }

  hasLocation(event: EventModel): boolean {
    return !!(event.longitude && event.latitude);
  }

  getEventTypeStringKey(event: EventModel): string {
    let eventTypeStringKey = event.event_type;
    if (event.vehicle.fuel_type && event.event_type === 'REFUELING_OR_CHARGING') {
      switch (event.vehicle.fuel_type) {
        case 'PETROL':
        case 'DIESEL':
        case 'PLUGIN_HYBRID':
        case 'HYBRID':
        case 'LPG':
        case 'CNG':
          eventTypeStringKey = 'REFUELING';
          break;
        case 'BATTERY_ELECTRIC':
          eventTypeStringKey = 'CHARGING';
          break;
      }
    }
    return eventTypeStringKey;
  }

  navigateToUserDetail(userId: number) {
    this.router.navigate(['users/' + userId + '/detail']);
  }

  getImage(imageId?: number) {
    if (imageId) {
      return this.imageStorageService.getImage(imageId).image;
    }
    return '';
  }
}
