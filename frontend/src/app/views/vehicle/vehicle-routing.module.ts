import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {VehicleListComponent} from './vehicle-list/vehicle-list.component';
import {VehicleComponent} from './vehicle.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleListComponent,
    data: {
      title: 'VEHICLE_LIST'
    }
  },
  {
    path: 'create',
    component: VehicleComponent,
    data: {
      title: 'VEHICLE_CREATE'
    }
  },
  {
    path: ':id/edit',
    component: VehicleComponent,
    data: {
      title: 'VEHICLE_EDIT'
    }
  },
  {
    path: ':id/detail',
    component: VehicleComponent,
    data: {
      title: 'VEHICLE_DETAIL'
    }
  },
  {
    path: ':vehicleId/events',
    data: {
      title: 'VEHICLE_EVENTS'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('../../views/event/event.module').then(m => m.EventModule)
      }
    ]
  },
  {
    path: ':vehicleId/journeys',
    data: {
      title: 'VEHICLE_JOURNEYS'
    },
    children: [
      {
        path: '',
        loadChildren: () => import('../../views/journey/journey.module').then(m => m.JourneyModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleRoutingModule {}
