import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/authentication/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServerErrorModel} from '../../models/shared/server-error.model';
import {RegisterRequest} from '../../models/register/register-request';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {

  registerForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    userName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
    passwordConfirm: [''],
    drivingLicenseExpiry: [''],
  }, { validators: this.passwordMatchValidator });

  get firstName() { return this.registerForm.get('firstName'); }
  get lastName() { return this.registerForm.get('lastName'); }
  get userName() { return this.registerForm.get('userName'); }
  get email() { return this.registerForm.get('email'); }
  get password() { return this.registerForm.get('password'); }
  get passwordConfirm() { return this.registerForm.get('passwordConfirm'); }
  get drivingLicenseExpiry() { return this.registerForm.get('drivingLicenseExpiry'); }

  serverErrors: ServerErrorModel = new ServerErrorModel();

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.registerForm.markAllAsTouched();
    if (this.registerForm.invalid) {
      return;
    }
    this.serverErrors.clear();
    const request: RegisterRequest = {
      first_name: this.firstName.value,
      last_name: this.lastName.value,
      user_name: this.userName.value,
      email: this.email.value,
      password: this.password.value,
      driving_license_expiry: this.drivingLicenseExpiry.value,
    };
    this.authService.register(request).subscribe(
      response => {
        this.router.navigate(['/login', { reason: 'register_success' }]);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  hasLocalValidationError(fieldName: string): boolean {
    const control = this.registerForm.get(fieldName);
    return control.invalid && this.isTouched(fieldName);
  }

  hasRemoteValidationError(fieldName: string): boolean {
    return !!this.serverErrors.fieldErrors[fieldName];
  }

  hasValidationError(localName, remoteName): boolean {
    return this.hasLocalValidationError(localName) || this.hasRemoteValidationError(remoteName);
  }

  removeRemoteValidationError(remoteName): void {
    this.serverErrors.fieldErrors[remoteName] = undefined;
  }

  passwordMatchValidator(group: FormGroup) {
    const password = group.get('password').value;
    const passwordConfirm = group.get('passwordConfirm').value;
    return password === passwordConfirm ? null : { passwordMismatch: true };
  }

  isTouched(fieldName: string): boolean {
    const control = this.registerForm.get(fieldName);
    return control.dirty || control.touched;
  }

}
