import {Component, OnDestroy, OnInit, SecurityContext} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ServerErrorModel} from '../../models/shared/server-error.model';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {UserService} from '../../services/user/user.service';
import {UserUpdateRequest} from '../../models/user/user-update-request';
import {Observable, Subscription} from 'rxjs';
import {UserModel} from '../../models/user/user.model';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {ComponentStateHelper} from '../../models/component-state/component-state-helper';
import {ImageService} from '../../services/image/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  alerts: any = [];

  id: number;
  profilePictureUrl = null;
  profilePicturePlaceholderUrl = 'assets/img/profile_picture_placeholder.jpeg';
  private routeSubscription: Subscription;
  userModel: UserModel = new UserModel();
  componentStateHelper: ComponentStateHelper;

  userForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    drivingLicenseExpiry: [''],
  });

  uploadForm = this.fb.group({
    profilePictureUpload: ['']
  });

  get firstName() { return this.userForm.get('firstName'); }
  get lastName() { return this.userForm.get('lastName'); }
  get drivingLicenseExpiry() { return this.userForm.get('drivingLicenseExpiry'); }

  get profilePictureUpload() { return this.uploadForm.get('profilePictureUpload'); }

  serverErrors: ServerErrorModel = new ServerErrorModel();

  constructor(private userService: UserService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private tokenStorageService: TokenStorageService,
              private imageService: ImageService,
              private sanitizer: DomSanitizer,
              private translateService: TranslateService) { }

  ngOnInit() {
    this.routeSubscription = this.route.url.subscribe(urlSegments => {
      this.componentStateHelper = new ComponentStateHelper(urlSegments[1].path);
      this.id = +urlSegments[0].path;
      if (isNaN(this.id)) {
        this.id = this.tokenStorageService.getUser().id;
      }
      this.loadUser();
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  loadUser(): void {
    this.userService.get({ id: this.id }).subscribe((userModel: UserModel) => {
      this.userModel = userModel;
      this.firstName.setValue(userModel.first_name);
      this.lastName.setValue(userModel.last_name);
      this.drivingLicenseExpiry.setValue(userModel.driving_license_expiry);
      if (userModel.image_id) {
        this.loadProfilePicture(userModel.image_id);
      }
    });
  }

  loadProfilePicture(imagedId: number): void {
    this.imageService.get({ id: imagedId }).subscribe((blob: any) => {
      const objectURL = URL.createObjectURL(blob);
      this.profilePictureUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      if (this.userModel.id
        && this.tokenStorageService.getUser()
        && this.userModel.id === this.tokenStorageService.getUser().id) {
        this.tokenStorageService
          .setProfilePictureUrl(this.sanitizer.sanitize(SecurityContext.HTML, this.sanitizer.bypassSecurityTrustHtml(objectURL)));
      }
      this.profilePictureUpload.setValue('');
    });
  }

  onSubmit(): void {
    this.userForm.markAllAsTouched();
    if (this.userForm.invalid) {
      return;
    }
    this.serverErrors.clear();
    const request: UserUpdateRequest = {
      id: this.id,
      first_name: this.firstName.value,
      last_name: this.lastName.value,
      driving_license_expiry: this.drivingLicenseExpiry.value,
    };
    this.userService.update(request).subscribe(
      response => {
        this.router.navigate(['users/' + request.id + '/detail']);
      },
      error => {
        this.serverErrors.parse(error.error);
      }
    );
  }

  hasLocalValidationError(fieldName: string): boolean {
    const control = this.userForm.get(fieldName);
    return control.invalid && this.isTouched(fieldName);
  }

  hasRemoteValidationError(fieldName: string): boolean {
    return !!this.serverErrors.fieldErrors[fieldName];
  }

  hasValidationError(localName, remoteName): boolean {
    return this.hasLocalValidationError(localName) || this.hasRemoteValidationError(remoteName);
  }

  removeRemoteValidationError(remoteName): void {
    this.serverErrors.fieldErrors[remoteName] = undefined;
  }

  isTouched(fieldName: string): boolean {
    const control = this.userForm.get(fieldName);
    return control.dirty || control.touched;
  }

  navigateBack(): void {
    this.router.navigate(['users/' + this.id + '/detail']);
  }

  edit() {
    this.router.navigate(['users/' + this.id + '/edit']);
  }

  deletePicture() {
    this.userService.updateImage(this.userModel.id).subscribe(() => {
      this.profilePictureUrl = null;
      this.profilePictureUpload.setValue('');
      this.tokenStorageService.setProfilePictureUrl(undefined);
    }, error => {
      this.onUploadError();
    });
  }

  isCurrentUser(): boolean {
    return this.tokenStorageService.getUser().id === this.userModel.id;
  }

  handleFileInput(files: FileList) {
    if (files[0]) {
      this.imageService.uploadImage(files[0]).subscribe(imageId => {
        this.userService.updateImage(this.userModel.id, { id: imageId}).subscribe(() => {
          this.onUploadSuccess();
          this.loadProfilePicture(imageId);
        }, error => {
          this.onUploadError();
        });
      }, error => {
        console.log(error);
        this.onUploadError();
      });
    }
  }

  onUploadError(): void {
    this.alerts.push({
      type: 'danger',
      msg: this.translateService.instant('ALERT_FAILED_TO_UPLOAD_IMAGE'),
      timeout: 5000
    });
    this.profilePictureUpload.setValue('');
  }

  onUploadSuccess(): void {
    this.alerts.push({
      type: 'success',
      msg: this.translateService.instant('ALERT_SUCCESSFUL_IMAGE_UPLOAD'),
      timeout: 3000
    });
    this.profilePictureUpload.setValue('');
  }

}
