import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {TranslateModule} from '@ngx-translate/core';
import {UserComponent} from './user.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserRoutingModule} from './user-routing.module';
import {CommonModule} from '@angular/common';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AlertModule} from 'ngx-bootstrap/alert';
import {ModalModule} from 'ngx-bootstrap/modal';

@NgModule({
  imports: [
    FormsModule,
    UserRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    TranslateModule,
    ReactiveFormsModule,
    CommonModule,
    NgxSpinnerModule,
    AlertModule,
    ModalModule
  ],
  declarations: [
    UserComponent,
    UserListComponent
  ]
})
export class UserModule { }
