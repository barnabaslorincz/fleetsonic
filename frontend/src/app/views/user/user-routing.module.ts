import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {UserComponent} from './user.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'me/detail',
  },
  {
    path: 'me/edit',
    component: UserComponent,
    data: {
      title: 'USER_EDIT'
    }
  },
  {
    path: 'me/detail',
    component: UserComponent,
    data: {
      title: 'USER_DETAILS'
    }
  },
  {
    path: ':id/edit',
    component: UserComponent,
    data: {
      title: 'USER_EDIT'
    }
  },
  {
    path: ':id/detail',
    component: UserComponent,
    data: {
      title: 'USER_DETAILS'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
