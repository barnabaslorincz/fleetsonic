import {Component, SecurityContext} from '@angular/core';
import { navItems } from '../../navigation-data';
import {TranslateService} from '@ngx-translate/core';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';
import {ImageService} from '../../services/image/image.service';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItems;
  public nameAndUserName = '';

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  constructor(private translateService: TranslateService,
              private tokenStorageService: TokenStorageService,
              private imageService: ImageService,
              private sanitizer: DomSanitizer,
              private router: Router) {
    this.navItems.forEach((navItem => {
      this.translateService.get(navItem.name).subscribe((result) => {
        navItem.name = result;
      });
      })
    );
    const user = this.tokenStorageService.getUser();
    this.nameAndUserName = user.user_name;
    this.initProfilePicture();
  }

  logout() {
    this.tokenStorageService.signOut();
    this.router.navigate(['/login']);
  }

  private initProfilePicture() {
    const user = this.tokenStorageService.getUser();
    if (user.image_id) {
      this.loadProfilePicture(user.image_id);
    }
  }

  private loadProfilePicture(imagedId: number): void {
    this.imageService.get({ id: imagedId }).subscribe((blob: any) => {
      const objectURL = URL.createObjectURL(blob);
      this.tokenStorageService
        .setProfilePictureUrl(this.sanitizer.sanitize(SecurityContext.HTML, this.sanitizer.bypassSecurityTrustHtml(objectURL)));
    });
  }

  getProfilePictureUrl() {
    const url = this.tokenStorageService.getProfilePictureUrl();
    return url ? url : 'assets/img/profile_picture_placeholder.jpeg';
  }
}
