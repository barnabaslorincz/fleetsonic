# Usage

```
# install app's dependencies
mvn install

# run the application on localhost:8080.
mvn spring-boot:run
```
