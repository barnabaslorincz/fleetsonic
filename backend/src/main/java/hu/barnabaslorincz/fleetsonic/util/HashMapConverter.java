package hu.barnabaslorincz.fleetsonic.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.Map;

public class HashMapConverter implements AttributeConverter<Map<String, Object>, String> {

    private static final Logger logger = LoggerFactory.getLogger(HashMapConverter.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(Map<String, Object> dataMap) {

        String data = null;
        try {
            data = objectMapper.writeValueAsString(dataMap);
        } catch (final JsonProcessingException e) {
            logger.error("JSON writing error", e);
        }

        return data;
    }

    @Override
    public Map<String, Object> convertToEntityAttribute(String data) {

        Map<String, Object> dataMap = null;
        try {
            dataMap = objectMapper.readValue(data, Map.class);
        } catch (final IOException e) {
            logger.error("JSON reading error", e);
        }

        return dataMap;
    }

}
