package hu.barnabaslorincz.fleetsonic.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class ControllerUtils {

    public static Set<Long> parseLongSet(String input) {
        if (input == null || input.length() == 0) {
            return Set.of();
        }
        return Arrays.stream(input.split(",")).map(Long::valueOf).collect(Collectors.toSet());
    }

    public static LocalDateTime parseLocalDateTime(String input) {
        if (input == null || input.length() == 0) {
            return null;
        }
        input = input.replace("Z", "");
        LocalDateTime parsedLocalDateTime = null;
        try {
             parsedLocalDateTime = LocalDateTime.parse(input, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        }
        catch (Exception ignored) {
        }
        return parsedLocalDateTime;
    }

    public static LocalDate parseLocalDate(String input) {
        if (input == null || input.length() == 0) {
            return null;
        }
        input = input.replace("Z", "");
        LocalDate parsedLocalDate = null;
        try {
             parsedLocalDate = LocalDate.parse(input, DateTimeFormatter.ISO_LOCAL_DATE);
        }
        catch (Exception ignored) {
        }
        return parsedLocalDate;
    }

}
