package hu.barnabaslorincz.fleetsonic.model.shared;

public enum VisibilityType {
    PRIVATE, SHARED
}
