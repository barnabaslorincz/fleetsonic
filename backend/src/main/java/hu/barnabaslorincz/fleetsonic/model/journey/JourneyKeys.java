package hu.barnabaslorincz.fleetsonic.model.journey;

public class JourneyKeys {
    public static final String VEHICLE_ID = "vehicle_id";
    public static final String VEHICLE = "vehicle";
    public static final String START_TIME_FROM = "start_time_from";
    public static final String START_TIME_TO = "start_time_to";
    public static final String START_TIME = "start_time";
    public static final String FINISH_TIME_FROM = "finish_time_from";
    public static final String FINISH_TIME_TO = "finish_time_to";
    public static final String FINISH_TIME = "finish_time";
    public static final String START_LATITUDE = "start_latitude";
    public static final String START_LONGITUDE = "start_longitude";
    public static final String FINISH_LATITUDE = "finish_latitude";
    public static final String FINISH_LONGITUDE = "finish_longitude";
    public static final String DESCRIPTION = "description";
    public static final String START_KM = "start_km";
    public static final String START_KM_FROM = "start_km_from";
    public static final String START_KM_TO = "start_km_to";
    public static final String FINISH_KM = "finish_km";
    public static final String FINISH_KM_FROM = "finish_km_from";
    public static final String FINISH_KM_TO = "finish_km_to";
}
