package hu.barnabaslorincz.fleetsonic.model.role;

public enum Role {
    SUPERUSER, ADMIN, USER
}
