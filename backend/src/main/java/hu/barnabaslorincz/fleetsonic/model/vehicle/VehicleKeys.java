package hu.barnabaslorincz.fleetsonic.model.vehicle;

public class VehicleKeys {
    public static final String MAKE = "make";
    public static final String MODEL = "model";
    public static final String LICENSE_PLATE = "license_plate";
    public static final String FUEL_CAPACITY = "fuel_capacity";
    public static final String KM_COUNTER = "km_counter";
    public static final String INSPECTION_VALIDITY = "inspection_validity";
    public static final String OWNER_ID = "owner_id";
    public static final String OWNER = "owner";
    public static final String ORGANISATION_ID = "organisation_id";
    public static final String ORGANISATION = "organisation";
    public static final String FUEL_TYPE = "fuel_type";
    public static final String IMAGE_ID = "image_id";
    public static final String INSPECTION_VALIDITY_FROM = "inspection_validity_from";
    public static final String INSPECTION_VALIDITY_TO = "inspection_validity_to";
    public static final String KM_COUNTER_FROM = "km_counter_from";
    public static final String KM_COUNTER_TO = "km_counter_to";
}
