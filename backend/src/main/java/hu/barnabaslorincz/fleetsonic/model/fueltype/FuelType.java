package hu.barnabaslorincz.fleetsonic.model.fueltype;

public enum FuelType {
    PETROL,
    DIESEL,
    BATTERY_ELECTRIC,
    PLUGIN_HYBRID,
    HYBRID,
    LPG,
    CNG
}
