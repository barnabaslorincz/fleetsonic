package hu.barnabaslorincz.fleetsonic.model.shared;

public class SharedKeys {
    public static final String ID = "id";
    public static final String IDS = "ids";
    public static final String CREATION_TIME = "creation_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String CREATOR_USER = "creator_user";
    public static final String CREATOR_USER_ID = "creator_user_id";
    public static final String ITEMS = "items";
    public static final String MESSAGE = "message";
    public static final String PAGING_RESULT = "paging_result";
    public static final String TOTAL_NUMBER_OF_ITEMS = "total_number_of_items";
    public static final String CURRENT_NUMBER_OF_ITEMS = "current_number_of_items";
    public static final String PAGE_NUMBER = "page_number";
    public static final String NUMBER_OF_ITEMS = "number_of_items";
    public static final String ORDER_DIRECTION = "order_direction";
    public static final String ORDER_FIELD = "order_field";
    public static final String GLOBAL_ERRORS = "global_errors";
    public static final String FIELD_ERRORS = "field_errors";
    public static final String FIELD = "field";
    public static final String ERROR = "error";
}
