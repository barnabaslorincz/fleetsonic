package hu.barnabaslorincz.fleetsonic.model.journey;

import com.querydsl.core.annotations.QueryEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.BaseEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@QueryEntity
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(	name = "journeys")
public class JourneyEntity extends BaseEntity {

    @NonNull
    @Column(nullable = false)
    private LocalDate startTime;

    @Nullable
    @Column(nullable = true)
    private LocalDate finishTime;

    @Nullable
    @Column(nullable = true)
    private Double startLatitude;

    @Nullable
    @Column(nullable = true)
    private Double startLongitude;

    @Nullable
    @Column(nullable = true)
    private Double finishLatitude;

    @Nullable
    @Column(nullable = true)
    private Double finishLongitude;

    @NonNull
    @Column(nullable = false)
    private Integer startKm;

    @Nullable
    @Column(nullable = true)
    private Integer finishKm;

    @NonNull
    @ManyToOne
    private VehicleEntity vehicle;

    @Nullable
    @Column(nullable = true)
    private String description;
}

