package hu.barnabaslorincz.fleetsonic.model.organisation;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.domain.PageRequest;

import java.util.Set;

@Builder
@Getter
public class OrganisationSearchRequest {

    private final String name;

    private final Set<Long> memberIds;

    private final PageRequest pageRequest;

}
