package hu.barnabaslorincz.fleetsonic.model.event;

public class EventKeys {
    public static final String EVENT_TYPE_ID = "eventTypeId";
    public static final String EVENT_TYPE = "event_type";
    public static final String VEHICLE_ID = "vehicleId";
    public static final String VEHICLE = "vehicle";
    public static final String TIME_FROM = "time_from";
    public static final String TIME_TO = "time_to";
    public static final String TIME = "time";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String AMOUNT = "amount";
    public static final String DESCRIPTION = "description";
    public static final String ISSUE = "issue";
    public static final String EXPENSES = "expenses";
    public static final String PRICE = "price";
    public static final String PRICE_FROM = "price_from";
    public static final String PRICE_TO = "price_to";
    public static final String KM_COUNTER = "km_counter";
    public static final String KM_COUNTER_FROM = "km_counter_from";
    public static final String KM_COUNTER_TO = "km_counter_to";
}
