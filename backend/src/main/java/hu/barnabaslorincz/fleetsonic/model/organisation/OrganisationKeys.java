package hu.barnabaslorincz.fleetsonic.model.organisation;

public class OrganisationKeys {
    public static final String NAME = "name";
    public static final String MEMBERS = "members";
    public static final String MEMBER_IDS = "member_ids";
    public static final String USER_ID = "user_id";
    public static final String INVITE_CODE = "invite_code";
}
