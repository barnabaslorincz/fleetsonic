package hu.barnabaslorincz.fleetsonic.model.vehicle;

import com.querydsl.core.annotations.QueryEntity;
import hu.barnabaslorincz.fleetsonic.model.event.EventEntity;
import hu.barnabaslorincz.fleetsonic.model.fueltype.FuelType;
import hu.barnabaslorincz.fleetsonic.model.image.ImageEntity;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.BaseEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;

@QueryEntity
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(	name = "vehicles")
public class VehicleEntity extends BaseEntity {

    @NonNull
    @Column(nullable = false)
    private String make;

    @NonNull
    @Column(nullable = false)
    private String model;

    @NonNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private FuelType fuelType;

    @NonNull
    @Column(nullable = false)
    private String licensePlate;

    @Nullable
    @Column(nullable = true)
    private Integer fuelCapacity;

    @NonNull
    @Column(nullable = false)
    private Integer kmCounter;

    @Nullable
    @Column(nullable = true)
    private LocalDate inspectionValidity;

    @Nullable
    @ManyToOne
    private UserEntity owner;

    @Nullable
    @ManyToOne
    private OrganisationEntity organisation;

    @OneToMany(mappedBy="vehicle")
    private List<EventEntity> events;

    @Nullable
    @OneToOne(mappedBy="vehicle")
    private ImageEntity image;

}

