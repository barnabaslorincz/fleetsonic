package hu.barnabaslorincz.fleetsonic.model.eventtype;

public enum EventType {
    REFUELING_OR_CHARGING,
    CLEANING,
    MAINTENANCE,
    INCIDENT
}
