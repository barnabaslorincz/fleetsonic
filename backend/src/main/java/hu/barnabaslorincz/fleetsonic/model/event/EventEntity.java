package hu.barnabaslorincz.fleetsonic.model.event;

import com.querydsl.core.annotations.QueryEntity;
import hu.barnabaslorincz.fleetsonic.model.eventtype.EventType;
import hu.barnabaslorincz.fleetsonic.model.shared.BaseEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;

@QueryEntity
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(	name = "events")
public class EventEntity extends BaseEntity {

    @NonNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private EventType eventType;

    @NonNull
    @Column(nullable = false)
    private LocalDate time;

    @Nullable
    @Column(nullable = true)
    private Double latitude;

    @Nullable
    @Column(nullable = true)
    private Double longitude;

    @Nullable
    @Column(nullable = true)
    private Double amount;

    @NonNull
    @Column(nullable = false)
    private Integer kmCounter;

    @Nullable
    @Column(nullable = true)
    private Integer price;

    @NonNull
    @ManyToOne
    private VehicleEntity vehicle;

    @Nullable
    @Column(nullable = true)
    private String description;
}

