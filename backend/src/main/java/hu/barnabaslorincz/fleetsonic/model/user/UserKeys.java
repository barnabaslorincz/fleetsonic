package hu.barnabaslorincz.fleetsonic.model.user;

public class UserKeys {
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String USER_NAME = "user_name";
    public static final String EMAIL = "email";
    public static final String ROLE = "role";
    public static final String PASSWORD = "password";
    public static final String CURRENT_PASSWORD = "current_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String PROFILE_PICTURE_URL = "profile_picture_url";
    public static final String DRIVING_LICENSE_EXPIRY = "driving_license_expiry";
    public static final String DRIVING_LICENSE_EXPIRY_FROM = "driving_license_expiry_from";
    public static final String DRIVING_LICENSE_EXPIRY_TO = "driving_license_expiry_to";
    public static final String TOKEN = "token";
    public static final String ORGANISATION = "organisation";
    public static final String IMAGE_ID = "image_id";
}
