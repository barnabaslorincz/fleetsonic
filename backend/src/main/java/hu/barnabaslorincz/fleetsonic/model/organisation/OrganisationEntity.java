package hu.barnabaslorincz.fleetsonic.model.organisation;

import com.querydsl.core.annotations.QueryEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.BaseEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@QueryEntity
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(	name = "organisations")
public class OrganisationEntity extends BaseEntity {

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false, unique = true)
    private String inviteCode;

    @OneToMany(mappedBy="organisation")
    private List<UserEntity> members;

    @OneToMany(mappedBy="organisation")
    private List<VehicleEntity> vehicles;

}
