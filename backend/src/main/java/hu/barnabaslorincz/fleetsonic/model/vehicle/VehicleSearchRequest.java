package hu.barnabaslorincz.fleetsonic.model.vehicle;

import hu.barnabaslorincz.fleetsonic.model.fueltype.FuelType;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;

@Builder
@Getter
public class VehicleSearchRequest {

    private final String make;

    private final String model;

    private final String licensePlate;

    private final FuelType fuelType;

    private final Integer kmCounterFrom;

    private final Integer kmCounterTo;

    private final LocalDate inspectionValidityFrom;

    private final LocalDate inspectionValidityTo;

    private final Long ownerId;

    private final Long organisationId;

    private final String userName;

    private final PageRequest pageRequest;

}
