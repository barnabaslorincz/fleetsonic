package hu.barnabaslorincz.fleetsonic.model.event;

import hu.barnabaslorincz.fleetsonic.model.eventtype.EventType;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@Getter
public class EventSearchRequest {

    private final EventType eventType;

    private final LocalDate timeFrom;

    private final LocalDate timeTo;

    private final Integer kmCounterFrom;

    private final Integer kmCounterTo;

    private final Integer priceFrom;

    private final Integer priceTo;

    private final String description;

    private final String licensePlate;

    private final String userName;

    private final PageRequest pageRequest;

}
