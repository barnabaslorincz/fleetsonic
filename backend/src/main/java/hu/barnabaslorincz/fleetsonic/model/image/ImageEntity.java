package hu.barnabaslorincz.fleetsonic.model.image;

import com.querydsl.core.annotations.QueryEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.BaseEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@QueryEntity
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(	name = "images")
public class ImageEntity extends BaseEntity {

    @NonNull
    @Column(nullable = false)
    private String fileName;

    @NonNull
    @Column(nullable = false)
    private String location;

    @Nullable
    @OneToOne
    private UserEntity user;

    @Nullable
    @OneToOne
    private VehicleEntity vehicle;

}

