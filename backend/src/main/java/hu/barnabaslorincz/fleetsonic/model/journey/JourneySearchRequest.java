package hu.barnabaslorincz.fleetsonic.model.journey;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;

@Builder
@Getter
public class JourneySearchRequest {

    private final LocalDate startTimeFrom;

    private final LocalDate startTimeTo;

    private final LocalDate finishTimeFrom;

    private final LocalDate finishTimeTo;

    private final Integer startKmFrom;

    private final Integer startKmTo;

    private final Integer finishKmFrom;

    private final Integer finishKmTo;

    private final String description;

    private final String licensePlate;

    private final String userName;

    private final PageRequest pageRequest;

}
