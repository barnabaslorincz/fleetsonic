package hu.barnabaslorincz.fleetsonic.model.user;

import hu.barnabaslorincz.fleetsonic.model.role.Role;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;

@Builder
@Getter
public class UserSearchRequest {

    private final Role role;

    private final String userName;

    private final String firstName;

    private final String lastName;

    private final String email;

    private final LocalDate drivingLicenseExpiryFrom;

    private final LocalDate drivingLicenseExpiryTo;

    private final PageRequest pageRequest;

}
