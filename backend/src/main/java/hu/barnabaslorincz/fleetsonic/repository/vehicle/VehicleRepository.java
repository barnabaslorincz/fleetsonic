package hu.barnabaslorincz.fleetsonic.repository.vehicle;

import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VehicleRepository extends JpaRepository<VehicleEntity, Long>, QuerydslPredicateExecutor<VehicleEntity> {

    Optional<VehicleEntity> findByIdentity(long id);

}
