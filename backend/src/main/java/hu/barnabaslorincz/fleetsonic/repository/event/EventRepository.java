package hu.barnabaslorincz.fleetsonic.repository.event;

import hu.barnabaslorincz.fleetsonic.model.event.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<EventEntity, Long>, QuerydslPredicateExecutor<EventEntity> {

    Optional<EventEntity> findByIdentity(long id);

}
