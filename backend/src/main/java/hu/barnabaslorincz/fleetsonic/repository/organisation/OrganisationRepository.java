package hu.barnabaslorincz.fleetsonic.repository.organisation;

import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrganisationRepository extends JpaRepository<OrganisationEntity, Long>, QuerydslPredicateExecutor<OrganisationEntity> {

    Optional<OrganisationEntity> findByIdentity(long id);

    Optional<OrganisationEntity> findByName(String name);

    Optional<OrganisationEntity> findByInviteCode(String inviteCode);

    Boolean existsByInviteCode(String inviteCode);

}
