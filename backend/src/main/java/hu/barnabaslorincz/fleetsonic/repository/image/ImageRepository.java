package hu.barnabaslorincz.fleetsonic.repository.image;

import hu.barnabaslorincz.fleetsonic.model.image.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, Long>, QuerydslPredicateExecutor<ImageEntity> {

    Optional<ImageEntity> findByIdentity(long id);

}
