package hu.barnabaslorincz.fleetsonic.repository.journey;

import hu.barnabaslorincz.fleetsonic.model.journey.JourneyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JourneyRepository extends JpaRepository<JourneyEntity, Long>, QuerydslPredicateExecutor<JourneyEntity> {

    Optional<JourneyEntity> findByIdentity(long id);

}
