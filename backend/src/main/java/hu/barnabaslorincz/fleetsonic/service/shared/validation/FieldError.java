package hu.barnabaslorincz.fleetsonic.service.shared.validation;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public final class FieldError {

    private final String fieldKey;

    private final String errorCode;

}
