package hu.barnabaslorincz.fleetsonic.service.organisation;

import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrganisationPermissionService {

    @Autowired
    UserRepository userRepository;

    public void checkOrganisationMember(long organisationId) {
        UserDetailsImpl currentUser = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        if (currentUser.getOrganisationEntity() != null
            && currentUser.getOrganisationEntity().getIdentity().equals(organisationId)) {
            return;
        }
        throw new AccessDeniedException("Access denied.");
    }

    public void checkOrganisationAdmin(long organisationId) {
        UserDetailsImpl currentUser = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        if (currentUser.getOrganisationEntity() != null
                && currentUser.getOrganisationEntity().getIdentity().equals(organisationId)
                && currentUser.getRole() == Role.ADMIN) {
            return;
        }
        throw new AccessDeniedException("Access denied.");
    }

}
