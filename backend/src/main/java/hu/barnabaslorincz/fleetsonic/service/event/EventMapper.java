package hu.barnabaslorincz.fleetsonic.service.event;

import hu.barnabaslorincz.fleetsonic.model.event.EventEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.BaseEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.payload.event.EventResource;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class EventMapper {

    @Autowired
    private VehicleMapper vehicleMapper;

    @Autowired
    private UserRepository userRepository;

    public EventResource toResource(EventEntity entity) {
        Optional<UserEntity> userEntityOptional = userRepository.findByUserName(entity.getCreatorUser());
        return EventResource.builder()
                .id(entity.getIdentity())
                .eventType(entity.getEventType())
                .creationTime(entity.getCreationTime())
                .updateTime(entity.getUpdateTime())
                .creatorUser(entity.getCreatorUser())
                .creatorUserId(userEntityOptional.map(BaseEntity::getIdentity).orElse(null))
                .time(entity.getTime())
                .latitude(entity.getLatitude())
                .longitude(entity.getLongitude())
                .amount(entity.getAmount())
                .kmCounter(entity.getKmCounter())
                .price(entity.getPrice())
                .description(entity.getDescription())
                .vehicle(vehicleMapper.toSimpleResource(entity.getVehicle()))
                .build();
    }

}
