package hu.barnabaslorincz.fleetsonic.service.vehicle;
import hu.barnabaslorincz.fleetsonic.model.shared.BaseEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleResource;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleSimpleResource;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.service.organisation.OrganisationMapper;
import hu.barnabaslorincz.fleetsonic.service.user.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class VehicleMapper {

    @Autowired
    OrganisationMapper organisationMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    private UserRepository userRepository;

    public VehicleResource toResource(VehicleEntity entity) {
        Optional<UserEntity> userEntityOptional = userRepository.findByUserName(entity.getCreatorUser());
        return VehicleResource.builder()
                .id(entity.getIdentity())
                .creationTime(entity.getCreationTime())
                .updateTime(entity.getUpdateTime())
                .creatorUser(entity.getCreatorUser())
                .creatorUserId(userEntityOptional.map(BaseEntity::getIdentity).orElse(null))
                .make(entity.getMake())
                .model(entity.getModel())
                .licensePlate(entity.getLicensePlate())
                .fuelCapacity(entity.getFuelCapacity())
                .kmCounter(entity.getKmCounter())
                .inspectionValidity(entity.getInspectionValidity())
                .imageId(entity.getImage() == null ? null : entity.getImage().getIdentity())
                .owner(entity.getOwner() == null ? null : userMapper.toSimpleResource(entity.getOwner()))
                .organisation(entity.getOrganisation() == null ? null : organisationMapper.toResource(entity.getOrganisation()))
                .fuelType(entity.getFuelType())
                .build();
    }

    public VehicleSimpleResource toSimpleResource(VehicleEntity entity) {
        Optional<UserEntity> userEntityOptional = userRepository.findByUserName(entity.getCreatorUser());
        return VehicleSimpleResource.builder()
                .id(entity.getIdentity())
                .creationTime(entity.getCreationTime())
                .updateTime(entity.getUpdateTime())
                .creatorUser(entity.getCreatorUser())
                .creatorUserId(userEntityOptional.map(BaseEntity::getIdentity).orElse(null))
                .make(entity.getMake())
                .model(entity.getModel())
                .licensePlate(entity.getLicensePlate())
                .fuelType(entity.getFuelType())
                .imageId(entity.getImage() == null ? null : entity.getImage().getIdentity())
                .build();
    }

}
