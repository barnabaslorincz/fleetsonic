package hu.barnabaslorincz.fleetsonic.service.shared.validation;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Getter
@NoArgsConstructor
public class ValidationErrors {

    private final ArrayList<String> globalErrors = new ArrayList<>();

    private final ArrayList<FieldError> fieldErrors = new ArrayList<>();

    public void addFieldError(String fieldKey, String errorCode) {
        this.fieldErrors.add(FieldError.builder()
                .fieldKey(fieldKey)
                .errorCode(errorCode)
                .build());
    }

    public void addGlobalError(String errorCode) {
        this.globalErrors.add(errorCode);
    }

    public boolean hasError() {
        return !globalErrors.isEmpty() || !fieldErrors.isEmpty();
    }

}


