package hu.barnabaslorincz.fleetsonic.service.organisation;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationEntity;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationResource;
import org.springframework.stereotype.Component;

@Component
public class OrganisationMapper {

    public OrganisationResource toResource(OrganisationEntity entity) {
        return OrganisationResource.builder()
                .id(entity.getIdentity())
                .creationTime(entity.getCreationTime())
                .updateTime(entity.getUpdateTime())
                .name(entity.getName())
                .inviteCode(entity.getInviteCode())
                .build();
    }

}
