package hu.barnabaslorincz.fleetsonic.service.shared.validation;

import hu.barnabaslorincz.fleetsonic.payload.shared.ErrorResponse;
import hu.barnabaslorincz.fleetsonic.service.shared.translation.TranslateService;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ValidationService {

    public void checkErrors(ValidationErrors validationErrors) {
        if (validationErrors.hasError()) {
            throw new RequestValidationException(ErrorResponse.builder()
                    .fieldErrors(validationErrors.getFieldErrors().stream()
                            .map(this::toLocalizedFieldError)
                            .collect(Collectors.toList()))
                    .globalErrors(validationErrors.getGlobalErrors().stream()
                            .map(this::toLocalizedGlobalError)
                            .collect(Collectors.toList()))
                    .build());
        }
    }

    private ErrorResponse.FieldError toLocalizedFieldError(FieldError fieldError) {
        return ErrorResponse.FieldError.builder()
                .field(fieldError.getFieldKey())
                .error(TranslateService.toLocale(fieldError.getErrorCode()))
                .build();
    }

    private String toLocalizedGlobalError(String errorKey) {
        return TranslateService.toLocale(errorKey);
    }

}
