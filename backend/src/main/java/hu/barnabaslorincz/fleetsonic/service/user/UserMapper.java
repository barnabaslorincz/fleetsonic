package hu.barnabaslorincz.fleetsonic.service.user;

import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.payload.user.UserResource;
import hu.barnabaslorincz.fleetsonic.payload.user.UserSimpleResource;
import hu.barnabaslorincz.fleetsonic.service.organisation.OrganisationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    @Autowired
    OrganisationMapper organisationMapper;

    public UserResource toResource(UserEntity entity) {
        return UserResource.builder()
                .creationTime(entity.getCreationTime())
                .updateTime(entity.getUpdateTime())
                .firstName(entity.getFirstName())
                .id(entity.getIdentity())
                .lastName(entity.getLastName())
                .email(entity.getEmail())
                .role(entity.getRole())
                .userName(entity.getUserName())
                .drivingLicenseExpiry(entity.getDrivingLicenseExpiry())
                .imageId(entity.getImage() == null ? null : entity.getImage().getIdentity())
                .organisation(entity.getOrganisation() == null ? null : organisationMapper.toResource(entity.getOrganisation()))
                .build();
    }

    public UserSimpleResource toSimpleResource(UserEntity entity) {
        return UserSimpleResource.builder()
                .creationTime(entity.getCreationTime())
                .updateTime(entity.getUpdateTime())
                .firstName(entity.getFirstName())
                .id(entity.getIdentity())
                .lastName(entity.getLastName())
                .userName(entity.getUserName())
                .build();
    }

}
