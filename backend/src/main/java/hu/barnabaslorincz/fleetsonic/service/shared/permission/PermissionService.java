package hu.barnabaslorincz.fleetsonic.service.shared.permission;

import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class PermissionService {

    public void checkRole(Role requiredRole) {
        Role userRole = ((UserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getRole();
        if (userRole == Role.ADMIN) {
            return;
        }
        if (requiredRole == Role.ADMIN) {
            throw new PermissionDeniedException("ADMIN role needed for this operation");
        }
    }

}
