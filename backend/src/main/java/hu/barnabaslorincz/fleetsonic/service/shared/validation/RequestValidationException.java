package hu.barnabaslorincz.fleetsonic.service.shared.validation;

import hu.barnabaslorincz.fleetsonic.payload.shared.ErrorResponse;
import lombok.Getter;

public class RequestValidationException extends RuntimeException {

    @Getter
    private final ErrorResponse errorResponse;

    public RequestValidationException(ErrorResponse errorResponse) {
        super();
        this.errorResponse = errorResponse;
    }

}