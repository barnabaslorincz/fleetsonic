package hu.barnabaslorincz.fleetsonic.service.organisation;

import com.querydsl.jpa.impl.JPAQueryFactory;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationEntity;
import hu.barnabaslorincz.fleetsonic.model.organisation.QOrganisationEntity;
import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.user.QUserEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationCreateUpdateRequest;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationJoinRequest;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityRequest;
import hu.barnabaslorincz.fleetsonic.repository.organisation.OrganisationRepository;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationMessageKeys;
import hu.barnabaslorincz.fleetsonic.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class OrganisationService {

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EntityManager entityManager;

    QUserEntity qUserEntity = QUserEntity.userEntity;

    public OrganisationEntity reqById(long organisationId) {
        return organisationRepository.getOne(organisationId);
    }

    public OrganisationEntity create(OrganisationCreateUpdateRequest request, Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        OrganisationEntity entity = new OrganisationEntity();
        entity.setName(request.getName());
        String inviteCode = UUID.randomUUID().toString();
        while (organisationRepository.existsByInviteCode(inviteCode)) {
            inviteCode = UUID.randomUUID().toString();
        }
        entity.setInviteCode(inviteCode);
        UserEntity currentUser = userService.reqById(userDetails.getId());
        entity.setMembers(List.of(currentUser));
        if (currentUser.getRole() != Role.SUPERUSER) {
            currentUser.setRole(Role.ADMIN);
        }
        organisationRepository.save(entity);
        currentUser.setOrganisation(entity);
        return entity;
    }

    public OrganisationEntity update(long organisationId, OrganisationCreateUpdateRequest request) {
        OrganisationEntity entity = reqById(organisationId);
        entity.setName(request.getName());
        return organisationRepository.save(entity);
    }

    public OrganisationEntity refreshInviteCode(long organisationId) {
        OrganisationEntity entity = reqById(organisationId);
        String inviteCode = UUID.randomUUID().toString();
        Optional<OrganisationEntity> entityWithSameInviteCode = organisationRepository.findByInviteCode(inviteCode);
        while (entityWithSameInviteCode.isPresent()
                && !entityWithSameInviteCode.get().getIdentity().equals(entity.getIdentity())) {
            inviteCode = UUID.randomUUID().toString();
        }
        entity.setInviteCode(inviteCode);
        return organisationRepository.save(entity);
    }

    public OrganisationEntity join(OrganisationJoinRequest request, Authentication authentication) {
        Optional<OrganisationEntity> optEntity = organisationRepository.findByInviteCode(request.getInviteCode());
        if (optEntity.isPresent()) {
            UserEntity currentUser = userService.reqById(((UserDetailsImpl) authentication.getPrincipal()).getId());
            OrganisationEntity organisationEntity = optEntity.get();
            organisationEntity.getMembers().add(currentUser);
            currentUser.setOrganisation(organisationEntity);
            if (currentUser.getRole() != Role.SUPERUSER) {
                currentUser.setRole(Role.USER);
            }
            userRepository.save(currentUser);
            return organisationRepository.save(organisationEntity);
        }
        throw new EntityNotFoundException();
    }

    public OrganisationEntity addMember(long organisationId, IdentityRequest request) {
        OrganisationEntity entity = reqById(organisationId);
        UserEntity userEntity = userService.reqById(request.getId());
        if (userEntity.getRole() != Role.SUPERUSER) {
            userEntity.setRole(Role.USER);
            userRepository.save(userEntity);
        }
        entity.getMembers().add(userEntity);
        return organisationRepository.save(entity);
    }

    public OrganisationEntity removeMember(long organisationId, IdentityRequest request) {
        OrganisationEntity entity = reqById(organisationId);
        UserEntity userEntity = userService.reqById(request.getId());
        entity.getMembers().remove(userEntity);
        userEntity.setOrganisation(null);
        return organisationRepository.save(entity);
    }

    public OrganisationEntity makeAdmin(long organisationId, IdentityRequest request) {
        OrganisationEntity entity = reqById(organisationId);
        UserEntity userEntity = userService.reqById(request.getId());
        if (userEntity.getRole() != Role.SUPERUSER) {
            userEntity.setRole(Role.ADMIN);
            userRepository.save(userEntity);
        }
        return organisationRepository.save(entity);
    }

    public OrganisationEntity removeAdmin(long organisationId, IdentityRequest request) {
        OrganisationEntity entity = reqById(organisationId);
        UserEntity userEntity = userService.reqById(request.getId());
        if (userEntity.getRole() != Role.SUPERUSER) {
            userEntity.setRole(Role.USER);
            userRepository.save(userEntity);
        }
        return organisationRepository.save(entity);
    }

    public long countAdmins(long organisationId) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qUserEntity)
                .from(qUserEntity)
                .where(qUserEntity.organisation.identity.eq(organisationId).and(qUserEntity.role.eq(Role.ADMIN)))
                .fetchCount();
    }
}
