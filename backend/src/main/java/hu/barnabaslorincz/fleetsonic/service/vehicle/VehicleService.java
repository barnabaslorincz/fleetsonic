package hu.barnabaslorincz.fleetsonic.service.vehicle;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.jpa.impl.JPAQueryFactory;
import hu.barnabaslorincz.fleetsonic.model.image.ImageEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.QVehicleEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleSearchRequest;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleCreateRequest;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleUpdateRequest;
import hu.barnabaslorincz.fleetsonic.repository.vehicle.VehicleRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.image.ImageService;
import hu.barnabaslorincz.fleetsonic.service.organisation.OrganisationService;
import hu.barnabaslorincz.fleetsonic.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class VehicleService {

    QVehicleEntity qVehicleEntity = QVehicleEntity.vehicleEntity;

    @Autowired
    EntityManager entityManager;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    UserService userService;

    @Autowired
    OrganisationService organisationService;

    @Autowired
    ImageService imageService;

    public VehicleEntity reqById(long vehicleId) {
        return vehicleRepository.getOne(vehicleId);
    }

    public VehicleEntity create(VehicleCreateRequest request) {
        VehicleEntity entity = new VehicleEntity();
        entity.setMake(request.getMake());
        entity.setModel(request.getModel());
        entity.setLicensePlate(request.getLicensePlate());
        entity.setFuelCapacity(request.getFuelCapacity());
        entity.setFuelType(request.getFuelType());
        entity.setKmCounter(request.getKmCounter());
        entity.setInspectionValidity(request.getInspectionValidity());
        entity.setOwner(request.getOwnerId() == null ? null : userService.reqById(request.getOwnerId()));
        entity.setOrganisation(request.getOrganisationId() == null ? null : organisationService.reqById(request.getOrganisationId()));
        return vehicleRepository.save(entity);
    }

    public VehicleEntity update(long vehicleId, VehicleUpdateRequest request) {
        VehicleEntity entity = reqById(vehicleId);
        entity.setLicensePlate(request.getLicensePlate());
        entity.setFuelCapacity(request.getFuelCapacity());
        entity.setInspectionValidity(request.getInspectionValidity());
        return vehicleRepository.save(entity);
    }

    public VehicleEntity updateImage(long vehicleId, Long imageId) {
        VehicleEntity entity = reqById(vehicleId);
        ImageEntity imageEntity = null;
        ImageEntity oldImageEntity = entity.getImage();
        if (oldImageEntity != null) {
            oldImageEntity.setVehicle(null);
        }
        if (imageId != null) {
            imageEntity = imageService.reqById(imageId);
            imageEntity.setVehicle(entity);
        }
        entity.setImage(imageEntity);
        return vehicleRepository.save(entity);
    }

    public List<VehicleEntity> read(VehicleSearchRequest searchRequest, Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qVehicleEntity)
                .from(qVehicleEntity)
                .where(createFilter(searchRequest)
                        .and(createVisibilityFilter(authentication)))
                .limit(searchRequest.getPageRequest().getPageSize())
                .offset(searchRequest.getPageRequest().getOffset())
                .orderBy(createOrderSpecifier(searchRequest.getPageRequest()))
                .fetch();
    }

    public long totalNumberOfItems(Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qVehicleEntity)
                .where(createVisibilityFilter(authentication)).fetchCount();
    }

    public long currentNumberOfItems(VehicleSearchRequest searchRequest, Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qVehicleEntity).where(createFilter(searchRequest)
                .and(createVisibilityFilter(authentication))).fetchCount();
    }

    private BooleanBuilder createFilter(VehicleSearchRequest searchRequest) {

        BooleanBuilder builder = new BooleanBuilder();

        if (searchRequest.getMake() != null) {
            builder.and(qVehicleEntity.make.containsIgnoreCase(searchRequest.getMake()));
        }
        if (searchRequest.getModel() != null) {
            builder.and(qVehicleEntity.model.containsIgnoreCase(searchRequest.getModel()));
        }
        if (searchRequest.getLicensePlate() != null) {
            builder.and(qVehicleEntity.licensePlate.containsIgnoreCase(searchRequest.getLicensePlate()));
        }
        if (searchRequest.getInspectionValidityFrom() != null) {
            builder.and(qVehicleEntity.inspectionValidity.goe(searchRequest.getInspectionValidityFrom()));
        }
        if (searchRequest.getInspectionValidityTo() != null) {
            builder.and(qVehicleEntity.inspectionValidity.loe(searchRequest.getInspectionValidityTo()));
        }
        if (searchRequest.getKmCounterFrom() != null) {
            builder.and(qVehicleEntity.kmCounter.goe(searchRequest.getKmCounterFrom()));
        }
        if (searchRequest.getKmCounterTo() != null) {
            builder.and(qVehicleEntity.kmCounter.loe(searchRequest.getKmCounterTo()));
        }
        if (searchRequest.getFuelType() != null) {
            builder.and(qVehicleEntity.fuelType.eq(searchRequest.getFuelType()));
        }
        if (searchRequest.getUserName() != null) {
            builder.and(qVehicleEntity.creatorUser.containsIgnoreCase(searchRequest.getUserName()));
        }

        return builder;
    }

    private BooleanBuilder createVisibilityFilter(Authentication authentication) {

        BooleanBuilder builder = new BooleanBuilder();

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        BooleanExpression mineOrInMyOrganisation = qVehicleEntity.owner.identity.eq(userDetails.getId());

        if (userDetails.getOrganisationEntity() != null) {
            mineOrInMyOrganisation = mineOrInMyOrganisation.or(qVehicleEntity.organisation.identity.eq(userDetails.getOrganisationEntity().getIdentity()));
        }

        builder.and(mineOrInMyOrganisation);

        return builder;
    }

    private OrderSpecifier<?> createOrderSpecifier(PageRequest pageRequest) {

        QVehicleEntity vehicleEntity = QVehicleEntity.vehicleEntity;

        Map<String, ComparableExpressionBase<?>> fields = Map.ofEntries(
                Map.entry(SharedKeys.ID, vehicleEntity.identity),
                Map.entry(VehicleKeys.MAKE, vehicleEntity.make),
                Map.entry(VehicleKeys.MODEL, vehicleEntity.model),
                Map.entry(VehicleKeys.LICENSE_PLATE, vehicleEntity.licensePlate),
                Map.entry(VehicleKeys.FUEL_CAPACITY, vehicleEntity.fuelCapacity),
                Map.entry(VehicleKeys.KM_COUNTER, vehicleEntity.kmCounter)
        );

        Optional<Map.Entry<String, ComparableExpressionBase<?>>> orderSpecifier = fields.entrySet().stream()
                .filter(f -> pageRequest.getSort().getOrderFor(f.getKey()) != null)
                .findFirst();

        if (orderSpecifier.isPresent()) {
            return pageRequest.getSort().getOrderFor(orderSpecifier.get().getKey()).getDirection().isAscending() ?
                    orderSpecifier.get().getValue().asc() :
                    orderSpecifier.get().getValue().desc();
        }

        return vehicleEntity.identity.desc();
    }

}
