package hu.barnabaslorincz.fleetsonic.service.user;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.jpa.impl.JPAQueryFactory;
import hu.barnabaslorincz.fleetsonic.model.image.ImageEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.user.QUserEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.model.user.UserSearchRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserChangePasswordRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserChangeRoleRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserUpdateRequest;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.image.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class UserService {

    QUserEntity qUserEntity = QUserEntity.userEntity;

    @Autowired
    EntityManager entityManager;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    ImageService imageService;

    public UserEntity reqById(long userId) {
        return userRepository.findById(userId)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public UserEntity updateUser(long userId, UserUpdateRequest request) {
        UserEntity entity = reqById(userId);
        entity.setFirstName(request.getFirstName());
        entity.setLastName(request.getLastName());
        entity.setDrivingLicenseExpiry(request.getDrivingLicenceExpiry());
        return userRepository.save(entity);
    }

    public UserEntity updateRole(long userId, UserChangeRoleRequest request) {
        UserEntity entity = reqById(userId);
        entity.setRole(request.getRole());
        return userRepository.save(entity);
    }

    public UserEntity updateImage(long userId, Long imageId) {
        UserEntity entity = reqById(userId);
        ImageEntity imageEntity = null;
        ImageEntity oldImageEntity = entity.getImage();
        if (oldImageEntity != null) {
            oldImageEntity.setUser(null);
        }
        if (imageId != null) {
            imageEntity = imageService.reqById(imageId);
            imageEntity.setUser(entity);
        }
        entity.setImage(imageEntity);
        return userRepository.save(entity);
    }

    public UserEntity updatePassword(long userId, UserChangePasswordRequest request) {
        UserEntity entity = reqById(userId);
        entity.setPassword(encoder.encode(request.getNewPassword()));
        return userRepository.save(entity);
    }

    public List<UserEntity> readAll(UserSearchRequest searchRequest, Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qUserEntity)
                .from(qUserEntity)
                .where(createFilter(searchRequest).and(createVisibilityFilter(authentication)))
                .limit(searchRequest.getPageRequest().getPageSize())
                .offset(searchRequest.getPageRequest().getOffset())
                .orderBy(createOrderSpecifier(searchRequest.getPageRequest()))
                .fetch();
    }

    public long totalNumberOfItems(Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qUserEntity).where(createVisibilityFilter(authentication)).fetchCount();
    }

    public long currentNumberOfItems(UserSearchRequest searchRequest, Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qUserEntity).where(createFilter(searchRequest)
                .and(createVisibilityFilter(authentication))).fetchCount();
    }

    private BooleanBuilder createVisibilityFilter(Authentication authentication) {

        BooleanBuilder builder = new BooleanBuilder();

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        BooleanExpression mineOrInMyOrganisation = qUserEntity.identity.eq(userDetails.getId());

        if (userDetails.getOrganisationEntity() != null) {
            mineOrInMyOrganisation = mineOrInMyOrganisation
                    .or(qUserEntity.organisation.identity.eq(userDetails.getOrganisationEntity().getIdentity()));
        }

        builder.and(mineOrInMyOrganisation);

        return builder;
    }

    private BooleanBuilder createFilter(UserSearchRequest searchRequest) {

        BooleanBuilder builder = new BooleanBuilder();

        if (searchRequest.getRole() != null) {
            builder.and(qUserEntity.role.eq(searchRequest.getRole()));
        }
        if (searchRequest.getUserName() != null) {
            builder.and(qUserEntity.userName.containsIgnoreCase(searchRequest.getUserName()));
        }
        if (searchRequest.getFirstName() != null) {
            builder.and(qUserEntity.firstName.containsIgnoreCase(searchRequest.getFirstName()));
        }
        if (searchRequest.getLastName() != null) {
            builder.and(qUserEntity.lastName.containsIgnoreCase(searchRequest.getLastName()));
        }
        if (searchRequest.getEmail() != null) {
            builder.and(qUserEntity.email.containsIgnoreCase(searchRequest.getEmail()));
        }
        if (searchRequest.getDrivingLicenseExpiryFrom() != null) {
            builder.and(qUserEntity.drivingLicenseExpiry.goe(searchRequest.getDrivingLicenseExpiryFrom()));
        }
        if (searchRequest.getDrivingLicenseExpiryTo() != null) {
            builder.and(qUserEntity.drivingLicenseExpiry.loe(searchRequest.getDrivingLicenseExpiryTo()));
        }

        return builder;
    }

    private OrderSpecifier<?> createOrderSpecifier(PageRequest pageRequest) {

        QUserEntity userEntity = QUserEntity.userEntity;

        Map<String, ComparableExpressionBase<?>> fields = Map.ofEntries(
                Map.entry(SharedKeys.ID, userEntity.identity),
                Map.entry(UserKeys.FIRST_NAME, userEntity.firstName),
                Map.entry(UserKeys.LAST_NAME, userEntity.lastName),
                Map.entry(UserKeys.EMAIL, userEntity.email),
                Map.entry(UserKeys.USER_NAME, userEntity.userName),
                Map.entry(UserKeys.DRIVING_LICENSE_EXPIRY, userEntity.drivingLicenseExpiry),
                Map.entry(UserKeys.ROLE, userEntity.role)
        );

        Optional<Map.Entry<String, ComparableExpressionBase<?>>> orderSpecifier = fields.entrySet().stream()
                .filter(f -> pageRequest.getSort().getOrderFor(f.getKey()) != null)
                .findFirst();

        if (orderSpecifier.isPresent()) {
            return pageRequest.getSort().getOrderFor(orderSpecifier.get().getKey()).getDirection().isAscending() ?
                    orderSpecifier.get().getValue().asc() :
                    orderSpecifier.get().getValue().desc();
        }

        return userEntity.identity.desc();
    }

}
