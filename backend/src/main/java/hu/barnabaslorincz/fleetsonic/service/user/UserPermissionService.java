package hu.barnabaslorincz.fleetsonic.service.user;

import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserPermissionService {

    @Autowired
    UserRepository userRepository;

    public void checkReadRight(long userId) {
        UserDetailsImpl currentUser = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(userId);
        if (userEntityOptional.isEmpty()) {
            throw new ResourceNotFoundException("User not found.");
        }
        if (currentUser.getId().equals(userEntityOptional.get().getIdentity())) {
            return;
        }
        if (currentUser.getOrganisationEntity() != null
        && userEntityOptional.get().getOrganisation() != null
        && currentUser.getOrganisationEntity().getIdentity().equals(userEntityOptional.get().getOrganisation().getIdentity())) {
            return;
        }
        throw new AccessDeniedException("Access denied.");
    }

    public void checkUpdateRight(long userId) {
        UserDetailsImpl currentUser = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(userId);
        if (userEntityOptional.isEmpty()) {
            throw new ResourceNotFoundException("User not found.");
        }
        if (currentUser.getId().equals(userEntityOptional.get().getIdentity())) {
            return;
        }
        throw new AccessDeniedException("Access denied.");
    }

    public void checkChangeRoleRight(long userId) {
        UserDetailsImpl currentUser = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(userId);
        if (userEntityOptional.isEmpty()) {
            throw new ResourceNotFoundException("User not found.");
        }
        if (currentUser.getId().equals(userEntityOptional.get().getIdentity())) {
            return;
        }
        if (currentUser.getOrganisationEntity() != null
                && userEntityOptional.get().getOrganisation() != null
                && currentUser.getOrganisationEntity().getIdentity().equals(userEntityOptional.get().getOrganisation().getIdentity())
                && currentUser.getRole().equals(Role.ADMIN)) {
            return;
        }
        throw new AccessDeniedException("Access denied.");
    }

}
