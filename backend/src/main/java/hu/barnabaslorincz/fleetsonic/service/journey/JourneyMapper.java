package hu.barnabaslorincz.fleetsonic.service.journey;

import hu.barnabaslorincz.fleetsonic.model.journey.JourneyEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.BaseEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.payload.journey.JourneyResource;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class JourneyMapper {

    @Autowired
    private VehicleMapper vehicleMapper;

    @Autowired
    private UserRepository userRepository;

    public JourneyResource toResource(JourneyEntity entity) {
        Optional<UserEntity> userEntityOptional = userRepository.findByUserName(entity.getCreatorUser());
        return JourneyResource.builder()
                .id(entity.getIdentity())
                .creationTime(entity.getCreationTime())
                .updateTime(entity.getUpdateTime())
                .creatorUser(entity.getCreatorUser())
                .creatorUserId(userEntityOptional.map(BaseEntity::getIdentity).orElse(null))
                .startTime(entity.getStartTime())
                .finishTime(entity.getFinishTime())
                .startLatitude(entity.getStartLatitude())
                .startLongitude(entity.getStartLongitude())
                .finishLatitude(entity.getFinishLatitude())
                .finishLongitude(entity.getFinishLongitude())
                .startKm(entity.getStartKm())
                .finishKm(entity.getFinishKm())
                .description(entity.getDescription())
                .vehicle(vehicleMapper.toSimpleResource(entity.getVehicle()))
                .build();
    }

}
