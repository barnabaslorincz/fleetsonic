package hu.barnabaslorincz.fleetsonic.service.journey;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.jpa.impl.JPAQueryFactory;
import hu.barnabaslorincz.fleetsonic.model.journey.JourneyEntity;
import hu.barnabaslorincz.fleetsonic.model.journey.JourneyKeys;
import hu.barnabaslorincz.fleetsonic.model.journey.JourneySearchRequest;
import hu.barnabaslorincz.fleetsonic.model.journey.QJourneyEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import hu.barnabaslorincz.fleetsonic.payload.journey.JourneyCreateUpdateRequest;
import hu.barnabaslorincz.fleetsonic.repository.journey.JourneyRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class JourneyService {

    QJourneyEntity qJourneyEntity = QJourneyEntity.journeyEntity;

    @Autowired
    EntityManager entityManager;

    @Autowired
    JourneyRepository journeyRepository;

    @Autowired
    VehicleService vehicleService;

    public JourneyEntity reqById(long journeyId) {
        return journeyRepository.getOne(journeyId);
    }

    public JourneyEntity create(Long vehicleId, JourneyCreateUpdateRequest request) {
        JourneyEntity entity = new JourneyEntity();
        entity.setStartTime(request.getStartTime());
        entity.setStartLatitude(request.getStartLatitude());
        entity.setStartLongitude(request.getStartLongitude());
        entity.setDescription(request.getDescription());
        entity.setStartKm(request.getStartKm());
        entity.setFinishTime(request.getFinishTime());
        entity.setFinishLatitude(request.getFinishLatitude());
        entity.setFinishLongitude(request.getFinishLongitude());
        entity.setDescription(request.getDescription());
        entity.setFinishKm(request.getFinishKm());
        VehicleEntity vehicleEntity = vehicleService.reqById(vehicleId);
        entity.setVehicle(vehicleEntity);
        if (request.getFinishKm() != null && vehicleEntity.getKmCounter() < request.getFinishKm()) {
            vehicleEntity.setKmCounter(request.getFinishKm());
        }
        return journeyRepository.save(entity);
    }

    public JourneyEntity update(long journeyId, JourneyCreateUpdateRequest request) {
        JourneyEntity entity = reqById(journeyId);
        entity.setStartTime(request.getStartTime());
        entity.setStartLatitude(request.getStartLatitude());
        entity.setStartLongitude(request.getStartLongitude());
        entity.setDescription(request.getDescription());
        entity.setStartKm(request.getStartKm());
        entity.setFinishTime(request.getFinishTime());
        entity.setFinishLatitude(request.getFinishLatitude());
        entity.setFinishLongitude(request.getFinishLongitude());
        entity.setDescription(request.getDescription());
        entity.setFinishKm(request.getFinishKm());
        VehicleEntity vehicleEntity = vehicleService.reqById(entity.getVehicle().getIdentity());
        if (request.getFinishKm() != null && vehicleEntity.getKmCounter() < request.getFinishKm()) {
            vehicleEntity.setKmCounter(request.getFinishKm());
        }
        return journeyRepository.save(entity);
    }

    public Long statCount(Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qJourneyEntity)
                .from(qJourneyEntity)
                .where(createVisibilityFilter(authentication).and(qJourneyEntity.finishTime.goe(LocalDate.now().minusDays(30))))
                .fetchCount();
    }

    public Long statKmCount(Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        List<JourneyEntity> entities = query
                .select(qJourneyEntity)
                .from(qJourneyEntity)
                .where(createVisibilityFilter(authentication)
                        .and(qJourneyEntity.finishTime.goe(LocalDate.now().minusDays(30)))
                .and(qJourneyEntity.startKm.isNotNull())
                .and(qJourneyEntity.finishKm.isNotNull()))
                .fetch();
        return entities
                .stream()
                .filter(journeyEntity -> journeyEntity.getFinishKm() != null)
                .map(journeyEntity -> journeyEntity.getFinishKm() - journeyEntity.getStartKm())
                .mapToLong(Integer::longValue).sum();
    }

    public void delete(long journeyId) {
        JourneyEntity entity = reqById(journeyId);
        journeyRepository.delete(entity);
    }

    public List<JourneyEntity> readAllByVehicle(long vehicleId, JourneySearchRequest searchRequest) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qJourneyEntity)
                .from(qJourneyEntity)
                .where(qJourneyEntity.vehicle.identity.eq(vehicleId).and(createFilter(searchRequest)))
                .limit(searchRequest.getPageRequest().getPageSize())
                .offset(searchRequest.getPageRequest().getOffset())
                .orderBy(createOrderSpecifier(searchRequest.getPageRequest()))
                .fetch();
    }

    public long totalNumberOfItemsByVehicle(long vehicleId) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qJourneyEntity).where(qJourneyEntity.vehicle.identity.eq(vehicleId)).fetchCount();
    }

    public long currentNumberOfItemsByVehicle(long vehicleId, JourneySearchRequest searchRequest) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qJourneyEntity).where(qJourneyEntity.vehicle.identity.eq(vehicleId).and(createFilter(searchRequest))).fetchCount();
    }

    public List<JourneyEntity> readAll(JourneySearchRequest searchRequest, Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qJourneyEntity)
                .from(qJourneyEntity)
                .where(createVisibilityFilter(authentication).and(createFilter(searchRequest)))
                .limit(searchRequest.getPageRequest().getPageSize())
                .offset(searchRequest.getPageRequest().getOffset())
                .orderBy(createOrderSpecifier(searchRequest.getPageRequest()))
                .fetch();
    }

    public long totalNumberOfItems(Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qJourneyEntity).where(createVisibilityFilter(authentication)).fetchCount();
    }

    public long currentNumberOfItems(JourneySearchRequest searchRequest, Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qJourneyEntity).where(createVisibilityFilter(authentication).and(createFilter(searchRequest))).fetchCount();
    }



    private BooleanBuilder createFilter(JourneySearchRequest searchRequest) {

        BooleanBuilder builder = new BooleanBuilder();
        if (searchRequest.getDescription() != null) {
            builder.and(qJourneyEntity.description.containsIgnoreCase(searchRequest.getDescription()));
        }
        if (searchRequest.getLicensePlate() != null) {
            builder.and(qJourneyEntity.vehicle.licensePlate.containsIgnoreCase(searchRequest.getLicensePlate()));
        }
        if (searchRequest.getUserName() != null) {
            builder.and(qJourneyEntity.creatorUser.containsIgnoreCase(searchRequest.getUserName()));
        }
        if (searchRequest.getStartTimeFrom() != null) {
            builder.and(qJourneyEntity.startTime.goe(searchRequest.getStartTimeFrom()));
        }
        if (searchRequest.getStartTimeTo() != null) {
            builder.and(qJourneyEntity.startTime.loe(searchRequest.getStartTimeTo()));
        }
        if (searchRequest.getFinishTimeFrom() != null) {
            builder.and(qJourneyEntity.finishTime.goe(searchRequest.getFinishTimeFrom()));
        }
        if (searchRequest.getFinishTimeTo() != null) {
            builder.and(qJourneyEntity.finishTime.loe(searchRequest.getFinishTimeTo()));
        }
        if (searchRequest.getStartKmFrom() != null) {
            builder.and(qJourneyEntity.startKm.goe(searchRequest.getStartKmFrom()));
        }
        if (searchRequest.getStartKmTo() != null) {
            builder.and(qJourneyEntity.startKm.loe(searchRequest.getStartKmTo()));
        }
        if (searchRequest.getFinishKmFrom() != null) {
            builder.and(qJourneyEntity.finishKm.goe(searchRequest.getFinishKmFrom()));
        }
        if (searchRequest.getFinishKmTo() != null) {
            builder.and(qJourneyEntity.finishKm.loe(searchRequest.getFinishKmTo()));
        }

        return builder;
    }

    private OrderSpecifier<?> createOrderSpecifier(PageRequest pageRequest) {

        QJourneyEntity journeyEntity = QJourneyEntity.journeyEntity;

        Map<String, ComparableExpressionBase<?>> fields = Map.ofEntries(
                Map.entry(SharedKeys.ID, journeyEntity.identity),
                Map.entry(JourneyKeys.START_KM, journeyEntity.startKm),
                Map.entry(JourneyKeys.FINISH_KM, journeyEntity.finishKm),
                Map.entry(JourneyKeys.START_TIME, journeyEntity.startTime),
                Map.entry(JourneyKeys.FINISH_TIME, journeyEntity.finishTime)
        );

        Optional<Map.Entry<String, ComparableExpressionBase<?>>> orderSpecifier = fields.entrySet().stream()
                .filter(f -> pageRequest.getSort().getOrderFor(f.getKey()) != null)
                .findFirst();

        if (orderSpecifier.isPresent()) {
            return pageRequest.getSort().getOrderFor(orderSpecifier.get().getKey()).getDirection().isAscending() ?
                    orderSpecifier.get().getValue().asc() :
                    orderSpecifier.get().getValue().desc();
        }

        return journeyEntity.identity.desc();
    }

    private BooleanBuilder createVisibilityFilter(Authentication authentication) {

        BooleanBuilder builder = new BooleanBuilder();

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        BooleanExpression mineOrInMyOrganisation = qJourneyEntity.vehicle.owner.identity.eq(userDetails.getId());

        if (userDetails.getOrganisationEntity() != null) {
            mineOrInMyOrganisation = mineOrInMyOrganisation.or(qJourneyEntity.vehicle.organisation.identity.eq(userDetails.getOrganisationEntity().getIdentity()));
        }

        builder.and(mineOrInMyOrganisation);

        return builder;
    }

}
