package hu.barnabaslorincz.fleetsonic.service.shared.permission;

import lombok.Getter;

public class PermissionDeniedException extends RuntimeException {

    @Getter
    private final String message;

    public PermissionDeniedException(String message) {
        super();
        this.message = message;
    }

}
