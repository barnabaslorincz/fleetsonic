package hu.barnabaslorincz.fleetsonic.service.image;

import hu.barnabaslorincz.fleetsonic.model.image.ImageEntity;
import hu.barnabaslorincz.fleetsonic.repository.file.FileRepository;
import hu.barnabaslorincz.fleetsonic.repository.image.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class ImageService {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    FileRepository fileRepository;

    public Long save(byte[] bytes, String imageName) throws Exception {
        String location = fileRepository.save(bytes, imageName);

        return imageRepository.save(new ImageEntity(imageName, location, null, null))
                .getIdentity();
    }

    public FileSystemResource find(Long imageId) {
        ImageEntity image = imageRepository.findById(imageId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        return fileRepository.findInFileSystem(image.getLocation());
    }

    public ImageEntity reqById(Long imageId) {
        return imageRepository.findById(imageId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }


}
