package hu.barnabaslorincz.fleetsonic.service.shared.validation;

public @interface ValidationMessageKeys {

    public @interface User {
        public static final String USER_NAME_ALREADY_IN_USE="user.user_name.already_in_use";
        public static final String EMAIL_ALREADY_IN_USE="user.email.already_in_use";
        public static final String USER_NOT_FOUND="user.not_found";
        public static final String USER_UPDATE_NOT_ALLOWED="user.update.not_allowed";
        public static final String USER_CHANGE_ROLE_INVALID_PRIVILEGE="user.change_role.invalid_privilege";
        public static final String USER_CHANGE_PASSWORD_CURRENT_PASSWORD_REQUIRED="user.change_password.current_password.required";
        public static final String USER_CHANGE_PASSWORD_CURRENT_PASSWORD_INVALID="user.change_password.current_password.invalid";
    }

    public @interface Authorization {
        public static final String AUTH_USER_NAME_INVALID = "auth.user_name.invalid";
        public static final String AUTH_PASSWORD_INVALID = "auth.password.invalid";
        public static final String USER_CHANGE_PASSWORD_CURRENT_PASSWORD_INVALID = "user.change_password.current_password.invalid";
    }

    public @interface Organisation {
        public static final String ORGANISATION_NOT_FOUND = "organisation.not_found";
        public static final String ORGANISATION_USER_NOT_MEMBER = "organisation.user_not_in_organisation";
        public static final String ORGANISATION_NAME_ALREADY_IN_USE = "organisation.name_already_in_use";
        public static final String USER_ALREADY_IN_ORGANISATION = "organisation.user_already_in_organisation";
        public static final String ORGANISATION_INVITE_CODE_INVALID = "organisation.invite_code_invalid";
        public static final String CANNOT_REMOVE_LAST_ADMIN = "organisation.cannot_remove_last_admin";
    }

    public @interface Vehicle {
        public static final String VEHICLE_BOTH_USER_AND_ORGANISATION_GIVEN = "vehicle.both_user_and_organisation_given";
        public static final String VEHICLE_NOT_FOUND="vehicle.not_found";
        public static final String OWNER_ID_DIFFERS="vehicle.owner_id_differs";
        public static final String ORGANISATION_ID_DIFFERS="vehicle.organisation_id_differs";
        public static final String MUST_BE_ADMIN_OR_CREATOR="vehicle.must_be_admin_or_creator";
    }

    public @interface Event {
        public static final String EVENT_NOT_FOUND="event.not_found";
        public static final String MUST_BE_ADMIN_OR_CREATOR="event.must_be_admin_or_creator";
        public static final String ORGANISATION_ID_DIFFERS="event.organisation_id_differs";
    }

    public @interface Journey {
        public static final String JOURNEY_NOT_FOUND="journey.not_found";
        public static final String MUST_BE_ADMIN_OR_CREATOR="journey.must_be_admin_or_creator";
        public static final String ORGANISATION_ID_DIFFERS="journey.organisation_id_differs";
    }

    public @interface Shared {
        public static final String ID_NOT_PRESENT = "shared.id_not_present";
    }

}
