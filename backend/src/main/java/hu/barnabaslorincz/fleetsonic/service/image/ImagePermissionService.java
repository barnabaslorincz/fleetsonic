package hu.barnabaslorincz.fleetsonic.service.image;

import hu.barnabaslorincz.fleetsonic.model.image.ImageEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.repository.image.ImageRepository;
import hu.barnabaslorincz.fleetsonic.repository.organisation.OrganisationRepository;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.repository.vehicle.VehicleRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ImagePermissionService {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    public void checkReadRight(long imageId) {
        UserDetailsImpl currentUser = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        Optional<ImageEntity> imageEntityOptional = imageRepository.findByIdentity(imageId);
        if (imageEntityOptional.isEmpty()) {
            throw new ResourceNotFoundException("Image not found.");
        }
        ImageEntity imageEntity = imageEntityOptional.get();
        if (imageEntity.getUser() != null && imageEntity.getUser().getIdentity().equals(currentUser.getId())) {
            return;
        }
        if (imageEntity.getVehicle() != null && imageEntity.getVehicle().getCreatorUser().equals(currentUser.getUsername())) {
            return;
        }
        if (currentUser.getOrganisationEntity() != null) {
            if (imageEntity.getVehicle() != null
                    && imageEntity.getVehicle().getOrganisation() != null
                    && imageEntity.getVehicle().getOrganisation().getIdentity().equals(currentUser.getOrganisationEntity().getIdentity())) {
                return;
            }
            if (imageEntity.getUser() != null
                    && imageEntity.getUser().getOrganisation() != null
                    && imageEntity.getUser().getOrganisation().getIdentity().equals(currentUser.getOrganisationEntity().getIdentity())) {
                return;
            }
        }
        throw new AccessDeniedException("Access denied.");
    }
}
