package hu.barnabaslorincz.fleetsonic.service.event;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.jpa.impl.JPAQueryFactory;
import hu.barnabaslorincz.fleetsonic.model.event.EventEntity;
import hu.barnabaslorincz.fleetsonic.model.event.EventKeys;
import hu.barnabaslorincz.fleetsonic.model.event.EventSearchRequest;
import hu.barnabaslorincz.fleetsonic.model.event.QEventEntity;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import hu.barnabaslorincz.fleetsonic.payload.event.EventCreateRequest;
import hu.barnabaslorincz.fleetsonic.payload.event.EventUpdateRequest;
import hu.barnabaslorincz.fleetsonic.repository.event.EventRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class EventService {

    QEventEntity qEventEntity = QEventEntity.eventEntity;

    @Autowired
    EntityManager entityManager;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    VehicleService vehicleService;

    public EventEntity reqById(long eventId) {
        return eventRepository.getOne(eventId);
    }

    public EventEntity create(Long vehicleId, EventCreateRequest request) {
        EventEntity entity = new EventEntity();
        entity.setTime(request.getTime());
        entity.setEventType(request.getEventType());
        entity.setLatitude(request.getLatitude());
        entity.setLongitude(request.getLongitude());
        entity.setAmount(request.getAmount());
        entity.setDescription(request.getDescription());
        entity.setKmCounter(request.getKmCounter());
        entity.setPrice(request.getPrice());
        VehicleEntity vehicleEntity = vehicleService.reqById(vehicleId);
        entity.setVehicle(vehicleEntity);
        if (vehicleEntity.getKmCounter() < request.getKmCounter()) {
            vehicleEntity.setKmCounter(request.getKmCounter());
        }
        return eventRepository.save(entity);
    }

    public EventEntity update(long eventId, EventUpdateRequest request) {
        EventEntity entity = reqById(eventId);
        entity.setTime(request.getTime());
        entity.setLatitude(request.getLatitude());
        entity.setLongitude(request.getLongitude());
        entity.setAmount(request.getAmount());
        entity.setDescription(request.getDescription());
        entity.setKmCounter(request.getKmCounter());
        entity.setPrice(request.getPrice());
        VehicleEntity vehicleEntity = vehicleService.reqById(entity.getVehicle().getIdentity());
        if (vehicleEntity.getKmCounter() < request.getKmCounter()) {
            vehicleEntity.setKmCounter(request.getKmCounter());
        }
        return eventRepository.save(entity);
    }

    public void delete(long eventId) {
        EventEntity entity = reqById(eventId);
        eventRepository.delete(entity);
    }

    public List<EventEntity> readAllByVehicle(long vehicleId, EventSearchRequest searchRequest) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qEventEntity)
                .from(qEventEntity)
                .where(qEventEntity.vehicle.identity.eq(vehicleId).and(createFilter(searchRequest)))
                .limit(searchRequest.getPageRequest().getPageSize())
                .offset(searchRequest.getPageRequest().getOffset())
                .orderBy(createOrderSpecifier(searchRequest.getPageRequest()))
                .fetch();
    }

    public long totalNumberOfItemsByVehicle(long vehicleId) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qEventEntity).where(qEventEntity.vehicle.identity.eq(vehicleId)).fetchCount();
    }

    public long currentNumberOfItemsByVehicle(long vehicleId, EventSearchRequest searchRequest) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qEventEntity).where(qEventEntity.vehicle.identity.eq(vehicleId).and(createFilter(searchRequest))).fetchCount();
    }

    public List<EventEntity> readAll(EventSearchRequest searchRequest, Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qEventEntity)
                .from(qEventEntity)
                .where(createVisibilityFilter(authentication).and(createFilter(searchRequest)))
                .limit(searchRequest.getPageRequest().getPageSize())
                .offset(searchRequest.getPageRequest().getOffset())
                .orderBy(createOrderSpecifier(searchRequest.getPageRequest()))
                .fetch();
    }

    public long totalNumberOfItems(Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qEventEntity).where(createVisibilityFilter(authentication)).fetchCount();
    }

    public long currentNumberOfItems(EventSearchRequest searchRequest, Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query.selectFrom(qEventEntity).where(createVisibilityFilter(authentication).and(createFilter(searchRequest))).fetchCount();
    }

    public Long statCount(Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qEventEntity)
                .from(qEventEntity)
                .where(createVisibilityFilter(authentication).and(qEventEntity.time.goe(LocalDate.now().minusDays(30))))
                .fetchCount();
    }

    public Long statAmountCount(Authentication authentication) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        List<Integer> results = query
                .select(qEventEntity.price)
                .from(qEventEntity)
                .where(createVisibilityFilter(authentication).and(qEventEntity.time.goe(LocalDate.now().minusDays(30))))
                .fetch();
        return results.stream().mapToLong(Integer::longValue).sum();
    }

    public EventEntity getLatestEvent(long vehicleId) {
        JPAQueryFactory query = new JPAQueryFactory(entityManager);
        return query
                .select(qEventEntity)
                .from(qEventEntity)
                .where(qEventEntity.vehicle.identity.eq(vehicleId))
                .fetchFirst();
    }


    private BooleanBuilder createFilter(EventSearchRequest searchRequest) {

        BooleanBuilder builder = new BooleanBuilder();

        if (searchRequest.getEventType() != null) {
            builder.and(qEventEntity.eventType.eq(searchRequest.getEventType()));
        }
        if (searchRequest.getDescription() != null) {
            builder.and(qEventEntity.description.containsIgnoreCase(searchRequest.getDescription()));
        }
        if (searchRequest.getLicensePlate() != null) {
            builder.and(qEventEntity.vehicle.licensePlate.containsIgnoreCase(searchRequest.getLicensePlate()));
        }
        if (searchRequest.getUserName() != null) {
            builder.and(qEventEntity.creatorUser.containsIgnoreCase(searchRequest.getUserName()));
        }
        if (searchRequest.getTimeFrom() != null) {
            builder.and(qEventEntity.time.goe(searchRequest.getTimeFrom()));
        }
        if (searchRequest.getTimeTo() != null) {
            builder.and(qEventEntity.time.loe(searchRequest.getTimeTo()));
        }
        if (searchRequest.getKmCounterFrom() != null) {
            builder.and(qEventEntity.kmCounter.goe(searchRequest.getKmCounterFrom()));
        }
        if (searchRequest.getKmCounterTo() != null) {
            builder.and(qEventEntity.kmCounter.loe(searchRequest.getKmCounterTo()));
        }
        if (searchRequest.getPriceFrom() != null) {
            builder.and(qEventEntity.price.goe(searchRequest.getPriceFrom()));
        }
        if (searchRequest.getPriceTo() != null) {
            builder.and(qEventEntity.price.loe(searchRequest.getPriceTo()));
        }

        return builder;
    }

    private OrderSpecifier<?> createOrderSpecifier(PageRequest pageRequest) {

        QEventEntity eventEntity = QEventEntity.eventEntity;

        Map<String, ComparableExpressionBase<?>> fields = Map.ofEntries(
                Map.entry(SharedKeys.ID, eventEntity.identity),
                Map.entry(EventKeys.KM_COUNTER, eventEntity.kmCounter),
                Map.entry(EventKeys.PRICE, eventEntity.price),
                Map.entry(EventKeys.TIME, eventEntity.time)
        );

        Optional<Map.Entry<String, ComparableExpressionBase<?>>> orderSpecifier = fields.entrySet().stream()
                .filter(f -> pageRequest.getSort().getOrderFor(f.getKey()) != null)
                .findFirst();

        if (orderSpecifier.isPresent()) {
            return pageRequest.getSort().getOrderFor(orderSpecifier.get().getKey()).getDirection().isAscending() ?
                    orderSpecifier.get().getValue().asc() :
                    orderSpecifier.get().getValue().desc();
        }

        return eventEntity.identity.desc();
    }

    private BooleanBuilder createVisibilityFilter(Authentication authentication) {

        BooleanBuilder builder = new BooleanBuilder();

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        BooleanExpression mineOrInMyOrganisation = qEventEntity.vehicle.owner.identity.eq(userDetails.getId());

        if (userDetails.getOrganisationEntity() != null) {
            mineOrInMyOrganisation = mineOrInMyOrganisation.or(qEventEntity.vehicle.organisation.identity.eq(userDetails.getOrganisationEntity().getIdentity()));
        }

        builder.and(mineOrInMyOrganisation);

        return builder;
    }

}
