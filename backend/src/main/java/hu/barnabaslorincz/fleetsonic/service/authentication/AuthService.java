package hu.barnabaslorincz.fleetsonic.service.authentication;

import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.payload.authentication.LoginRequest;
import hu.barnabaslorincz.fleetsonic.payload.authentication.LoginResponse;
import hu.barnabaslorincz.fleetsonic.payload.authentication.RegisterRequest;
import hu.barnabaslorincz.fleetsonic.repository.organisation.OrganisationRepository;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.security.jwt.JwtUtils;
import hu.barnabaslorincz.fleetsonic.service.organisation.OrganisationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AuthService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    OrganisationMapper organisationMapper;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    public LoginResponse login(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return LoginResponse.builder()
                .id(userDetails.getId())
                .token("Bearer " + jwt)
                .userName(userDetails.getUsername())
                .imageId(userDetails.getImageId())
                .email(userDetails.getEmail())
                .role(userDetails.getRole())
                .organisation(userDetails.getOrganisationEntity() == null
                        ? null : organisationMapper.toResource(userDetails.getOrganisationEntity()))
                .build();
    }

    public UserEntity register(RegisterRequest registerRequest) {
        UserEntity user = UserEntity.builder()
                .firstName(registerRequest.getFirstName())
                .lastName(registerRequest.getLastName())
                .email(registerRequest.getEmail())
                .password(encoder.encode(registerRequest.getPassword()))
                .role(Role.USER)
                .userName(registerRequest.getUserName())
                .drivingLicenseExpiry(registerRequest.getDrivingLicenceExpiry())
                .build();
        return userRepository.save(user);
    }
}
