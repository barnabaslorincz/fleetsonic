package hu.barnabaslorincz.fleetsonic.controller.journey;

import hu.barnabaslorincz.fleetsonic.model.journey.JourneyEntity;
import hu.barnabaslorincz.fleetsonic.model.journey.JourneyKeys;
import hu.barnabaslorincz.fleetsonic.model.journey.JourneySearchRequest;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import hu.barnabaslorincz.fleetsonic.payload.journey.JourneyCreateUpdateRequest;
import hu.barnabaslorincz.fleetsonic.payload.journey.JourneyResource;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityResponse;
import hu.barnabaslorincz.fleetsonic.payload.shared.ListResource;
import hu.barnabaslorincz.fleetsonic.service.journey.JourneyMapper;
import hu.barnabaslorincz.fleetsonic.service.journey.JourneyService;
import hu.barnabaslorincz.fleetsonic.service.shared.permission.PermissionService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationService;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleService;
import hu.barnabaslorincz.fleetsonic.util.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("")
public class JourneyController {

    @Autowired
    PermissionService permissionService;

    @Autowired
    VehicleService vehicleService;

    @Autowired
    JourneyService journeyService;

    @Autowired
    JourneyMapper journeyMapper;

    @Autowired
    ValidationService validationService;

    @Autowired
    JourneyRequestValidator journeyRequestValidator;

    @GetMapping("/vehicles/{vehicleId}/journeys")
    public ResponseEntity<ListResource<JourneyResource>> readByVehicle(
            @PathVariable(value = "vehicleId") long vehicleId,
            @RequestParam(name = JourneyKeys.START_TIME_FROM, required = false) String startTimeFrom,
            @RequestParam(name = JourneyKeys.START_TIME_TO, required = false) String startTimeTo,
            @RequestParam(name = JourneyKeys.FINISH_TIME_FROM, required = false) String finishTimeFrom,
            @RequestParam(name = JourneyKeys.FINISH_TIME_TO, required = false) String finishTimeTo,
            @RequestParam(name = JourneyKeys.START_KM_FROM, required = false) Integer startKmFrom,
            @RequestParam(name = JourneyKeys.START_KM_TO, required = false) Integer startKmTo,
            @RequestParam(name = JourneyKeys.FINISH_KM_FROM, required = false) Integer finishKmFrom,
            @RequestParam(name = JourneyKeys.FINISH_KM_TO, required = false) Integer finishKmTo,
            @RequestParam(name = JourneyKeys.DESCRIPTION, required = false) String description,
            @RequestParam(name = UserKeys.USER_NAME, required = false) String userName,
            @RequestParam(name = SharedKeys.PAGE_NUMBER, required=false , defaultValue="1") Integer pageNumber,
            @RequestParam(name = SharedKeys.NUMBER_OF_ITEMS, required=false , defaultValue="10") Integer numberOfItems,
            @RequestParam(name = SharedKeys.ORDER_DIRECTION, required=false , defaultValue="DESC") Sort.Direction orderDirection,
            @RequestParam(name = SharedKeys.ORDER_FIELD, required=false , defaultValue=SharedKeys.ID) String orderField
            ) {
        PageRequest request = PageRequest.of(pageNumber-1, numberOfItems, orderDirection, orderField);
        JourneySearchRequest searchRequest = JourneySearchRequest.builder()
                .startTimeFrom(ControllerUtils.parseLocalDate(startTimeFrom))
                .startTimeTo(ControllerUtils.parseLocalDate(startTimeTo))
                .finishTimeFrom(ControllerUtils.parseLocalDate(finishTimeFrom))
                .finishTimeTo(ControllerUtils.parseLocalDate(finishTimeTo))
                .startKmFrom(startKmFrom)
                .startKmTo(startKmTo)
                .finishKmFrom(finishKmFrom)
                .finishKmTo(finishKmTo)
                .description(description)
                .userName(userName)
                .pageRequest(request)
                .build();
        List<JourneyEntity> journeyEntityList = journeyService.readAllByVehicle(vehicleId, searchRequest);
        long totalNumberOfItems = journeyService.totalNumberOfItemsByVehicle(vehicleId);
        long currentNumberOfItems = journeyService.currentNumberOfItemsByVehicle(vehicleId, searchRequest);
        ListResource<JourneyResource> journeyResourceListResource =
                ListResource.<JourneyResource>builder()
                .items(journeyEntityList.stream().map(journeyMapper::toResource).collect(Collectors.toList()))
                .pagingResult(ListResource.PagingResult.builder()
                        .totalNumberOfItems(totalNumberOfItems)
                        .currentNumberOfItems(currentNumberOfItems)
                        .build())
                .build();
        return ResponseEntity.ok(journeyResourceListResource);
    }

    @GetMapping("/journeys")
    public ResponseEntity<ListResource<JourneyResource>> readAll(
            @RequestParam(name = JourneyKeys.START_TIME_FROM, required = false) String startTimeFrom,
            @RequestParam(name = JourneyKeys.START_TIME_TO, required = false) String startTimeTo,
            @RequestParam(name = JourneyKeys.FINISH_TIME_FROM, required = false) String finishTimeFrom,
            @RequestParam(name = JourneyKeys.FINISH_TIME_TO, required = false) String finishTimeTo,
            @RequestParam(name = JourneyKeys.START_KM_FROM, required = false) Integer startKmFrom,
            @RequestParam(name = JourneyKeys.START_KM_TO, required = false) Integer startKmTo,
            @RequestParam(name = JourneyKeys.FINISH_KM_FROM, required = false) Integer finishKmFrom,
            @RequestParam(name = JourneyKeys.FINISH_KM_TO, required = false) Integer finishKmTo,
            @RequestParam(name = JourneyKeys.DESCRIPTION, required = false) String description,
            @RequestParam(name = UserKeys.USER_NAME, required = false) String userName,
            @RequestParam(name = VehicleKeys.LICENSE_PLATE, required = false) String licensePlate,
            @RequestParam(name = SharedKeys.PAGE_NUMBER, required=false , defaultValue="1") Integer pageNumber,
            @RequestParam(name = SharedKeys.NUMBER_OF_ITEMS, required=false , defaultValue="10") Integer numberOfItems,
            @RequestParam(name = SharedKeys.ORDER_DIRECTION, required=false , defaultValue="DESC") Sort.Direction orderDirection,
            @RequestParam(name = SharedKeys.ORDER_FIELD, required=false , defaultValue=SharedKeys.ID) String orderField,
            Authentication authentication
            ) {
        PageRequest request = PageRequest.of(pageNumber-1, numberOfItems, orderDirection, orderField);
        JourneySearchRequest searchRequest = JourneySearchRequest.builder()
                .startTimeFrom(ControllerUtils.parseLocalDate(startTimeFrom))
                .startTimeTo(ControllerUtils.parseLocalDate(startTimeTo))
                .finishTimeFrom(ControllerUtils.parseLocalDate(finishTimeFrom))
                .finishTimeTo(ControllerUtils.parseLocalDate(finishTimeTo))
                .startKmFrom(startKmFrom)
                .startKmTo(startKmTo)
                .finishKmFrom(finishKmFrom)
                .finishKmTo(finishKmTo)
                .description(description)
                .userName(userName)
                .licensePlate(licensePlate)
                .pageRequest(request)
                .build();
        List<JourneyEntity> journeyEntityList = journeyService.readAll(searchRequest, authentication);
        long totalNumberOfItems = journeyService.totalNumberOfItems(authentication);
        long currentNumberOfItems = journeyService.currentNumberOfItems(searchRequest, authentication);
        ListResource<JourneyResource> journeyResourceListResource =
                ListResource.<JourneyResource>builder()
                .items(journeyEntityList.stream().map(journeyMapper::toResource).collect(Collectors.toList()))
                .pagingResult(ListResource.PagingResult.builder()
                        .totalNumberOfItems(totalNumberOfItems)
                        .currentNumberOfItems(currentNumberOfItems)
                        .build())
                .build();
        return ResponseEntity.ok(journeyResourceListResource);
    }

    @GetMapping("/journeys/statCount")
    public ResponseEntity<Long> statCount(Authentication authentication) {
        return ResponseEntity.ok(journeyService.statCount(authentication));
    }

    @GetMapping("/journeys/statKmCount")
    public ResponseEntity<Long> statKmCount(Authentication authentication) {
        return ResponseEntity.ok(journeyService.statKmCount(authentication));
    }

    @GetMapping("vehicles/{vehicleId}/journeys/{journeyId}")
    public ResponseEntity<JourneyResource> getJourney(
            @PathVariable(value = "journeyId") long journeyId,
            @PathVariable(value = "vehicleId") long vehicleId,
            Authentication authentication) {
        validationService.checkErrors(journeyRequestValidator.validateGetRequest(vehicleId, authentication));
        return ResponseEntity
                .ok(journeyMapper.toResource(journeyService.reqById(journeyId)));
    }

    @PostMapping("/vehicles/{vehicleId}/journeys")
    public ResponseEntity<IdentityResponse> createJourney(
            @PathVariable(value = "vehicleId") long vehicleId,
            @RequestBody JourneyCreateUpdateRequest request,
            Authentication authentication) {
        validationService.checkErrors(journeyRequestValidator.validateCreateRequest(vehicleId, authentication));
        JourneyEntity entity = journeyService.create(vehicleId, request);
        return ResponseEntity
                .ok(IdentityResponse.builder()
                        .id(entity.getIdentity())
                        .build());
    }

    @PutMapping("/vehicles/{vehicleId}/journeys/{journeyId}")
    public ResponseEntity<Object> updateJourney(
            @PathVariable(value = "vehicleId") long vehicleId,
            @PathVariable(value = "journeyId") long journeyId,
            @RequestBody JourneyCreateUpdateRequest request,
            Authentication authentication) {
        validationService.checkErrors(journeyRequestValidator.validateUpdateDeleteRequest(vehicleId, journeyId, authentication));
        journeyService.update(journeyId, request);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/vehicles/{vehicleId}/journeys/{journeyId}")
    public ResponseEntity<Object> deleteJourney(
            @PathVariable(value = "vehicleId") long vehicleId,
            @PathVariable(value = "journeyId") long journeyId,
            Authentication authentication) {
        validationService.checkErrors(journeyRequestValidator.validateUpdateDeleteRequest(vehicleId, journeyId, authentication));
        journeyService.delete(journeyId);
        return ResponseEntity
                .ok().build();
    }

}
