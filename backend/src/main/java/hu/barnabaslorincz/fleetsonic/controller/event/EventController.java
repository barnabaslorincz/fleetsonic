package hu.barnabaslorincz.fleetsonic.controller.event;

import hu.barnabaslorincz.fleetsonic.model.event.EventEntity;
import hu.barnabaslorincz.fleetsonic.model.event.EventKeys;
import hu.barnabaslorincz.fleetsonic.model.event.EventSearchRequest;
import hu.barnabaslorincz.fleetsonic.model.eventtype.EventType;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import hu.barnabaslorincz.fleetsonic.payload.event.EventCreateRequest;
import hu.barnabaslorincz.fleetsonic.payload.event.EventResource;
import hu.barnabaslorincz.fleetsonic.payload.event.EventUpdateRequest;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityResponse;
import hu.barnabaslorincz.fleetsonic.payload.shared.ListResource;
import hu.barnabaslorincz.fleetsonic.service.event.EventMapper;
import hu.barnabaslorincz.fleetsonic.service.event.EventService;
import hu.barnabaslorincz.fleetsonic.service.shared.permission.PermissionService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationService;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleService;
import hu.barnabaslorincz.fleetsonic.util.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("")
public class EventController {

    @Autowired
    PermissionService permissionService;

    @Autowired
    VehicleService vehicleService;

    @Autowired
    EventService eventService;

    @Autowired
    EventMapper eventMapper;

    @Autowired
    ValidationService validationService;

    @Autowired
    EventRequestValidator eventRequestValidator;

    @GetMapping("/vehicles/{vehicleId}/events")
    public ResponseEntity<ListResource<EventResource>> readByVehicle(
            @PathVariable(value = "vehicleId") long vehicleId,
            @RequestParam(name = EventKeys.EVENT_TYPE, required = false) EventType eventType,
            @RequestParam(name = EventKeys.TIME_FROM, required = false) String timeFrom,
            @RequestParam(name = EventKeys.TIME_TO, required = false) String timeTo,
            @RequestParam(name = EventKeys.KM_COUNTER_FROM, required = false) Integer kmCounterFrom,
            @RequestParam(name = EventKeys.KM_COUNTER_TO, required = false) Integer kmCounterTo,
            @RequestParam(name = EventKeys.PRICE_FROM, required = false) Integer priceFrom,
            @RequestParam(name = EventKeys.PRICE_TO, required = false) Integer priceTo,
            @RequestParam(name = EventKeys.DESCRIPTION, required = false) String description,
            @RequestParam(name = UserKeys.USER_NAME, required = false) String userName,
            @RequestParam(name = SharedKeys.PAGE_NUMBER, required=false , defaultValue="1") Integer pageNumber,
            @RequestParam(name = SharedKeys.NUMBER_OF_ITEMS, required=false , defaultValue="10") Integer numberOfItems,
            @RequestParam(name = SharedKeys.ORDER_DIRECTION, required=false , defaultValue="DESC") Sort.Direction orderDirection,
            @RequestParam(name = SharedKeys.ORDER_FIELD, required=false , defaultValue=SharedKeys.ID) String orderField
            ) {
        PageRequest request = PageRequest.of(pageNumber-1, numberOfItems, orderDirection, orderField);
        EventSearchRequest searchRequest = EventSearchRequest.builder()
                .eventType(eventType)
                .timeFrom(ControllerUtils.parseLocalDate(timeFrom))
                .timeTo(ControllerUtils.parseLocalDate(timeTo))
                .description(description)
                .userName(userName)
                .kmCounterFrom(kmCounterFrom)
                .kmCounterTo(kmCounterTo)
                .priceFrom(priceFrom)
                .priceTo(priceTo)
                .pageRequest(request)
                .build();
        List<EventEntity> eventEntityList = eventService.readAllByVehicle(vehicleId, searchRequest);
        long totalNumberOfItems = eventService.totalNumberOfItemsByVehicle(vehicleId);
        long currentNumberOfItems = eventService.currentNumberOfItemsByVehicle(vehicleId, searchRequest);
        ListResource<EventResource> eventResourceListResource =
                ListResource.<EventResource>builder()
                .items(eventEntityList.stream().map(eventMapper::toResource).collect(Collectors.toList()))
                .pagingResult(ListResource.PagingResult.builder()
                        .totalNumberOfItems(totalNumberOfItems)
                        .currentNumberOfItems(currentNumberOfItems)
                        .build())
                .build();
        return ResponseEntity.ok(eventResourceListResource);
    }

    @GetMapping("/events")
    public ResponseEntity<ListResource<EventResource>> readAll(
            @RequestParam(name = EventKeys.EVENT_TYPE, required = false) EventType eventType,
            @RequestParam(name = EventKeys.TIME_FROM, required = false) String timeFrom,
            @RequestParam(name = EventKeys.TIME_TO, required = false) String timeTo,
            @RequestParam(name = EventKeys.KM_COUNTER_FROM, required = false) Integer kmCounterFrom,
            @RequestParam(name = EventKeys.KM_COUNTER_TO, required = false) Integer kmCounterTo,
            @RequestParam(name = EventKeys.PRICE_FROM, required = false) Integer priceFrom,
            @RequestParam(name = EventKeys.PRICE_TO, required = false) Integer priceTo,
            @RequestParam(name = EventKeys.DESCRIPTION, required = false) String description,
            @RequestParam(name = UserKeys.USER_NAME, required = false) String userName,
            @RequestParam(name = VehicleKeys.LICENSE_PLATE, required = false) String licensePlate,
            @RequestParam(name = SharedKeys.PAGE_NUMBER, required=false , defaultValue="1") Integer pageNumber,
            @RequestParam(name = SharedKeys.NUMBER_OF_ITEMS, required=false , defaultValue="10") Integer numberOfItems,
            @RequestParam(name = SharedKeys.ORDER_DIRECTION, required=false , defaultValue="DESC") Sort.Direction orderDirection,
            @RequestParam(name = SharedKeys.ORDER_FIELD, required=false , defaultValue=SharedKeys.ID) String orderField,
            Authentication authentication
            ) {
        PageRequest request = PageRequest.of(pageNumber-1, numberOfItems, orderDirection, orderField);
        EventSearchRequest searchRequest = EventSearchRequest.builder()
                .eventType(eventType)
                .timeFrom(ControllerUtils.parseLocalDate(timeFrom))
                .timeTo(ControllerUtils.parseLocalDate(timeTo))
                .description(description)
                .userName(userName)
                .licensePlate(licensePlate)
                .kmCounterFrom(kmCounterFrom)
                .kmCounterTo(kmCounterTo)
                .priceFrom(priceFrom)
                .priceTo(priceTo)
                .pageRequest(request)
                .build();
        List<EventEntity> eventEntityList = eventService.readAll(searchRequest, authentication);
        long totalNumberOfItems = eventService.totalNumberOfItems(authentication);
        long currentNumberOfItems = eventService.currentNumberOfItems(searchRequest, authentication);
        ListResource<EventResource> eventResourceListResource =
                ListResource.<EventResource>builder()
                .items(eventEntityList.stream().map(eventMapper::toResource).collect(Collectors.toList()))
                .pagingResult(ListResource.PagingResult.builder()
                        .totalNumberOfItems(totalNumberOfItems)
                        .currentNumberOfItems(currentNumberOfItems)
                        .build())
                .build();
        return ResponseEntity.ok(eventResourceListResource);
    }

    @GetMapping("/events/statCount")
    public ResponseEntity<Long> statCount(Authentication authentication) {
        return ResponseEntity.ok(eventService.statCount(authentication));
    }

    @GetMapping("/events/statAmountCount")
    public ResponseEntity<Long> statAmountCount(Authentication authentication) {
        return ResponseEntity.ok(eventService.statAmountCount(authentication));
    }

    @GetMapping("vehicles/{vehicleId}/events/{eventId}")
    public ResponseEntity<EventResource> getEvent(
            @PathVariable(value = "eventId") long eventId,
            @PathVariable(value = "vehicleId") long vehicleId,
            Authentication authentication) {
        validationService.checkErrors(eventRequestValidator.validateGetRequest(vehicleId, authentication));
        return ResponseEntity
                .ok(eventMapper.toResource(eventService.reqById(eventId)));
    }

    @PostMapping("/vehicles/{vehicleId}/events")
    public ResponseEntity<IdentityResponse> createEvent(
            @PathVariable(value = "vehicleId") long vehicleId,
            @RequestBody EventCreateRequest request,
            Authentication authentication) {
        validationService.checkErrors(eventRequestValidator.validateCreateRequest(vehicleId, authentication));
        EventEntity entity = eventService.create(vehicleId, request);
        return ResponseEntity
                .ok(IdentityResponse.builder()
                        .id(entity.getIdentity())
                        .build());
    }

    @PutMapping("/vehicles/{vehicleId}/events/{eventId}")
    public ResponseEntity<Object> updateEvent(
            @PathVariable(value = "vehicleId") long vehicleId,
            @PathVariable(value = "eventId") long eventId,
            @RequestBody EventUpdateRequest request,
            Authentication authentication) {
        validationService.checkErrors(eventRequestValidator.validateUpdateDeleteRequest(vehicleId, eventId, authentication));
        eventService.update(eventId, request);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/vehicles/{vehicleId}/events/{eventId}")
    public ResponseEntity<Object> deleteEvent(
            @PathVariable(value = "vehicleId") long vehicleId,
            @PathVariable(value = "eventId") long eventId,
            Authentication authentication) {
        validationService.checkErrors(eventRequestValidator.validateUpdateDeleteRequest(vehicleId, eventId, authentication));
        eventService.delete(eventId);
        return ResponseEntity
                .ok().build();
    }

}
