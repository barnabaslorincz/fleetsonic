package hu.barnabaslorincz.fleetsonic.controller.journey;

import hu.barnabaslorincz.fleetsonic.model.journey.JourneyEntity;
import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import hu.barnabaslorincz.fleetsonic.repository.journey.JourneyRepository;
import hu.barnabaslorincz.fleetsonic.repository.organisation.OrganisationRepository;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.repository.vehicle.VehicleRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.journey.JourneyService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationErrors;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationMessageKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class JourneyRequestValidator {

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    JourneyRepository journeyRepository;

    @Autowired
    JourneyService journeyService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    ValidationErrors validateGetRequest(long vehicleId, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        Optional<VehicleEntity> vehicleEntityOptional = vehicleRepository.findByIdentity(vehicleId);
        if (vehicleEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.VEHICLE_NOT_FOUND);
            return validationErrors;
        }
        boolean isMyCar = vehicleEntityOptional.get().getOwner() != null
                && vehicleEntityOptional.get().getOwner().getIdentity().equals(authenticatedUser.getId());
        boolean isCarInMyOrganisation = vehicleEntityOptional.get().getOrganisation() != null
                && authenticatedUser.getOrganisationEntity() != null
                && vehicleEntityOptional.get().getOrganisation().getIdentity().equals(authenticatedUser.getOrganisationEntity().getIdentity());
        if (!isMyCar && !isCarInMyOrganisation) {
            validationErrors.addGlobalError(ValidationMessageKeys.Journey.ORGANISATION_ID_DIFFERS);
            return validationErrors;
        }
        return validationErrors;
    }

    ValidationErrors validateCreateRequest(long vehicleId, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        Optional<VehicleEntity> vehicleEntityOptional = vehicleRepository.findByIdentity(vehicleId);
        if (vehicleEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.VEHICLE_NOT_FOUND);
            return validationErrors;
        }
        boolean isMyCar = vehicleEntityOptional.get().getOwner() != null
                && vehicleEntityOptional.get().getOwner().getIdentity().equals(authenticatedUser.getId());
        boolean isCarInMyOrganisation = vehicleEntityOptional.get().getOrganisation() != null
                && authenticatedUser.getOrganisationEntity() != null
                && vehicleEntityOptional.get().getOrganisation().getIdentity().equals(authenticatedUser.getOrganisationEntity().getIdentity());
        if (!isMyCar && !isCarInMyOrganisation) {
            validationErrors.addGlobalError(ValidationMessageKeys.Journey.ORGANISATION_ID_DIFFERS);
            return validationErrors;
        }
        return validationErrors;
    }

    ValidationErrors validateUpdateDeleteRequest(long vehicleId, long journeyId, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        Optional<VehicleEntity> vehicleEntityOptional = vehicleRepository.findByIdentity(vehicleId);
        if (vehicleEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.VEHICLE_NOT_FOUND);
            return validationErrors;
        }
        Optional<JourneyEntity> journeyEntityOptional = journeyRepository.findByIdentity(journeyId);
        if (journeyEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.Journey.JOURNEY_NOT_FOUND);
            return validationErrors;
        }
        boolean iAmJourneyCreator = journeyEntityOptional.get().getCreatorUser().equals(authenticatedUser.getUsername());
        boolean isMyCar = vehicleEntityOptional.get().getOwner() != null
                && vehicleEntityOptional.get().getOwner().getIdentity().equals(authenticatedUser.getId());
        boolean isCarInMyOrganisationAndIAmAdmin = vehicleEntityOptional.get().getOrganisation() != null
                && authenticatedUser.getOrganisationEntity() != null
                && vehicleEntityOptional.get().getOrganisation().getIdentity().equals(authenticatedUser.getOrganisationEntity().getIdentity())
                && authenticatedUser.getRole() == Role.ADMIN;
        if (!isMyCar && !isCarInMyOrganisationAndIAmAdmin && !iAmJourneyCreator) {
            validationErrors.addGlobalError(ValidationMessageKeys.Journey.MUST_BE_ADMIN_OR_CREATOR);
            return validationErrors;
        }
        return validationErrors;
    }
}
