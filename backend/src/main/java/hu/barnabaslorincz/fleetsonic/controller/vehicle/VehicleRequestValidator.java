package hu.barnabaslorincz.fleetsonic.controller.vehicle;

import hu.barnabaslorincz.fleetsonic.model.event.EventEntity;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationEntity;
import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleCreateRequest;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleUpdateRequest;
import hu.barnabaslorincz.fleetsonic.repository.organisation.OrganisationRepository;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.repository.vehicle.VehicleRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.event.EventService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationErrors;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationMessageKeys;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class VehicleRequestValidator {

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    EventService eventService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    ValidationErrors validateGetRequest(long vehicleId, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        Optional<VehicleEntity> vehicleEntityOptional = vehicleRepository.findByIdentity(vehicleId);
        if (vehicleEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.VEHICLE_NOT_FOUND);
            return validationErrors;
        } else {
            VehicleEntity entity = vehicleEntityOptional.get();
            if (entity.getOwner() != null && !entity.getOwner().getIdentity().equals(authenticatedUser.getId())) {
                validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.OWNER_ID_DIFFERS);
                return validationErrors;
            }
            if (entity.getOrganisation() != null && authenticatedUser.getOrganisationEntity() == null) {
                validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.ORGANISATION_ID_DIFFERS);
                return validationErrors;
            } else if (entity.getOrganisation() != null
                    && !entity.getOrganisation().getIdentity().equals(authenticatedUser.getOrganisationEntity().getIdentity())) {
                validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.ORGANISATION_ID_DIFFERS);
                return validationErrors;
            }
        }
        return validationErrors;
    }

    ValidationErrors validateCreateRequest(VehicleCreateRequest createRequest, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        if (createRequest.getOrganisationId() != null && createRequest.getOwnerId() != null) {
            validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.VEHICLE_BOTH_USER_AND_ORGANISATION_GIVEN);
            return validationErrors;
        }
        if (createRequest.getOwnerId() != null) {
            Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(createRequest.getOwnerId());
            if (userEntityOptional.isEmpty()) {
                validationErrors.addGlobalError(ValidationMessageKeys.User.USER_NOT_FOUND);
                return validationErrors;
            }
            else if (!userEntityOptional.get().getIdentity().equals(authenticatedUser.getId())) {
                validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.OWNER_ID_DIFFERS);
                return validationErrors;
            }
        }
        if (createRequest.getOrganisationId() != null) {
            Optional<OrganisationEntity> organisationEntityOptional = organisationRepository.findByIdentity(createRequest.getOrganisationId());
            if (organisationEntityOptional.isEmpty()) {
                validationErrors.addGlobalError(ValidationMessageKeys.Organisation.ORGANISATION_NOT_FOUND);
                return validationErrors;
            }
            else if (authenticatedUser.getOrganisationEntity() != null
                    && !organisationEntityOptional.get().getIdentity().equals(authenticatedUser.getOrganisationEntity().getIdentity())) {
                validationErrors.addGlobalError(ValidationMessageKeys.Organisation.ORGANISATION_USER_NOT_MEMBER);
                return validationErrors;
            }
        }
        return validationErrors;
    }

    ValidationErrors validateUpdateRequest(long vehicleId, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl authenticatedUser = (UserDetailsImpl) authentication.getPrincipal();
        Optional<VehicleEntity> vehicleEntityOptional = vehicleRepository.findByIdentity(vehicleId);
        if (vehicleEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.VEHICLE_NOT_FOUND);
            return validationErrors;
        } else {
            VehicleEntity entity = vehicleEntityOptional.get();
            if (entity.getOwner() != null && !entity.getOwner().getIdentity().equals(authenticatedUser.getId())) {
                validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.OWNER_ID_DIFFERS);
                return validationErrors;
            }
            if (entity.getOrganisation() != null && authenticatedUser.getOrganisationEntity() == null) {
                validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.ORGANISATION_ID_DIFFERS);
                return validationErrors;
            }
            else if (entity.getOrganisation() != null
                    && !entity.getOrganisation().getIdentity().equals(authenticatedUser.getOrganisationEntity().getIdentity())) {
                validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.ORGANISATION_ID_DIFFERS);
                return validationErrors;
            } else if (entity.getOrganisation() != null
                    && authenticatedUser.getRole() != Role.ADMIN
                    && !authenticatedUser.getUsername().equals(entity.getCreatorUser())) {
                validationErrors.addGlobalError(ValidationMessageKeys.Vehicle.MUST_BE_ADMIN_OR_CREATOR);
            }
        }
        return validationErrors;
    }

}
