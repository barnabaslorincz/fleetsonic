package hu.barnabaslorincz.fleetsonic.controller.vehicle;

import hu.barnabaslorincz.fleetsonic.model.fueltype.FuelType;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleEntity;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleSearchRequest;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationResource;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityRequest;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityResponse;
import hu.barnabaslorincz.fleetsonic.payload.shared.ListResource;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleCreateRequest;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleResource;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleUpdateRequest;
import hu.barnabaslorincz.fleetsonic.service.shared.permission.PermissionService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationService;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleMapper;
import hu.barnabaslorincz.fleetsonic.service.vehicle.VehicleService;
import hu.barnabaslorincz.fleetsonic.util.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    @Autowired
    PermissionService permissionService;

    @Autowired
    VehicleService vehicleService;

    @Autowired
    VehicleMapper vehicleMapper;

    @Autowired
    ValidationService validationService;

    @Autowired
    VehicleRequestValidator vehicleRequestValidator;

    @GetMapping("")
    public ResponseEntity<ListResource<VehicleResource>> readByUser(
            @RequestParam(name = VehicleKeys.MAKE, required = false) String make,
            @RequestParam(name = VehicleKeys.MODEL, required = false) String model,
            @RequestParam(name = VehicleKeys.LICENSE_PLATE, required = false) String licensePlate,
            @RequestParam(name = VehicleKeys.FUEL_TYPE, required = false) FuelType fuelType,
            @RequestParam(name = VehicleKeys.INSPECTION_VALIDITY_FROM, required = false) String inspectionValidityFrom,
            @RequestParam(name = VehicleKeys.INSPECTION_VALIDITY_TO, required = false) String inspectionValidityTo,
            @RequestParam(name = VehicleKeys.KM_COUNTER_FROM, required = false) Integer kmCounterFrom,
            @RequestParam(name = VehicleKeys.KM_COUNTER_TO, required = false) Integer kmCounterTo,
            @RequestParam(name = VehicleKeys.OWNER_ID, required = false) Long ownerId,
            @RequestParam(name = VehicleKeys.ORGANISATION_ID, required = false) Long organisationId,
            @RequestParam(name = UserKeys.USER_NAME, required = false) String userName,
            @RequestParam(name = SharedKeys.PAGE_NUMBER, required=false , defaultValue="1") Integer pageNumber,
            @RequestParam(name = SharedKeys.NUMBER_OF_ITEMS, required=false , defaultValue="10") Integer numberOfItems,
            @RequestParam(name = SharedKeys.ORDER_DIRECTION, required=false , defaultValue="DESC") Sort.Direction orderDirection,
            @RequestParam(name = SharedKeys.ORDER_FIELD, required=false , defaultValue=SharedKeys.ID) String orderField,
            Authentication authentication
            ) {
        PageRequest request = PageRequest.of(pageNumber-1, numberOfItems, orderDirection, orderField);
        VehicleSearchRequest searchRequest = VehicleSearchRequest.builder()
                .make(make)
                .model(model)
                .inspectionValidityFrom(ControllerUtils.parseLocalDate(inspectionValidityFrom))
                .inspectionValidityTo(ControllerUtils.parseLocalDate(inspectionValidityTo))
                .licensePlate(licensePlate)
                .fuelType(fuelType)
                .kmCounterFrom(kmCounterFrom)
                .kmCounterTo(kmCounterTo)
                .ownerId(ownerId)
                .organisationId(organisationId)
                .userName(userName)
                .pageRequest(request)
                .build();
        List<VehicleEntity> vehicleEntityList = vehicleService.read(searchRequest, authentication);
        long totalNumberOfItems = vehicleService.totalNumberOfItems(authentication);
        long currentNumberOfItems = vehicleService.currentNumberOfItems(searchRequest, authentication);
        ListResource<VehicleResource> vehicleResourceListResource =
                ListResource.<VehicleResource>builder()
                .items(vehicleEntityList.stream().map(vehicleMapper::toResource).collect(Collectors.toList()))
                .pagingResult(ListResource.PagingResult.builder()
                        .totalNumberOfItems(totalNumberOfItems)
                        .currentNumberOfItems(currentNumberOfItems)
                        .build())
                .build();
        return ResponseEntity.ok(vehicleResourceListResource);
    }

    @GetMapping("/{vehicleId}")
    public ResponseEntity<VehicleResource> getVehicle(
            @PathVariable(value = "vehicleId") long vehicleId,
            Authentication authentication) {
        validationService.checkErrors(vehicleRequestValidator.validateGetRequest(vehicleId, authentication));
        return ResponseEntity
                .ok(vehicleMapper.toResource(vehicleService.reqById(vehicleId)));
    }

    @PostMapping("")
    public ResponseEntity<IdentityResponse> createVehicle(
            @RequestBody VehicleCreateRequest request,
            Authentication authentication) {
        validationService.checkErrors(vehicleRequestValidator.validateCreateRequest(request, authentication));
        VehicleEntity entity = vehicleService.create(request);
        return ResponseEntity
                .ok(IdentityResponse.builder()
                        .id(entity.getIdentity())
                        .build());
    }

    @PutMapping("/{vehicleId}")
    public ResponseEntity<Object> updateVehicle(
            @PathVariable(value = "vehicleId") long vehicleId,
            @RequestBody VehicleUpdateRequest request,
            Authentication authentication) {
        validationService.checkErrors(vehicleRequestValidator.validateUpdateRequest(vehicleId, authentication));
        vehicleService.update(vehicleId, request);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{vehicleId}/image")
    public ResponseEntity<Object> updateImage(
            @PathVariable(value = "vehicleId") long vehicleId,
            @RequestBody IdentityRequest request,
            Authentication authentication) {
        validationService.checkErrors(vehicleRequestValidator.validateUpdateRequest(vehicleId, authentication));
        vehicleService.updateImage(vehicleId, request.getId());
        return ResponseEntity
                .ok().build();
    }

}
