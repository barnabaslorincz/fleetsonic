package hu.barnabaslorincz.fleetsonic.controller.user;

import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.payload.user.UserChangePasswordRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserChangeRoleRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserUpdateRequest;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationErrors;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationMessageKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserRequestValidator {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    ValidationErrors validateGetRequest(long userId) {
        ValidationErrors validationErrors = new ValidationErrors();
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(userId);
        if (!userEntityOptional.isPresent()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_NOT_FOUND);
        }
        return validationErrors;
    }

    ValidationErrors validateUpdateRequest(long userId, UserUpdateRequest request, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (userDetails.getRole() != Role.ADMIN && userId != userDetails.getId()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_UPDATE_NOT_ALLOWED);
            return validationErrors;
        }
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(userId);
        if (!userEntityOptional.isPresent()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_NOT_FOUND);
        }
        return validationErrors;
    }

    ValidationErrors validateUpdateRoleRequest(long userId, UserChangeRoleRequest request) {
        ValidationErrors validationErrors = new ValidationErrors();
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(userId);
        if (!userEntityOptional.isPresent()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_NOT_FOUND);
            return validationErrors;
        }
        UserEntity entity = userEntityOptional.get();
        if (request.getRole().ordinal() > entity.getRole().ordinal()) {
            validationErrors.addFieldError(UserKeys.ROLE, ValidationMessageKeys.User.USER_CHANGE_ROLE_INVALID_PRIVILEGE);
            return validationErrors;
        }
        return validationErrors;
    }

    public ValidationErrors validateUpdatePasswordRequest(long userId, UserChangePasswordRequest request, Authentication authentication) {

        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (userDetails.getRole() != Role.ADMIN && userId != userDetails.getId()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_UPDATE_NOT_ALLOWED);
            return validationErrors;
        }
        if (userDetails.getRole() != Role.ADMIN && request.getCurrentPassword() == null) {
            validationErrors.addFieldError(UserKeys.CURRENT_PASSWORD, ValidationMessageKeys.User.USER_CHANGE_PASSWORD_CURRENT_PASSWORD_REQUIRED);
            return validationErrors;
        }
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(userId);
        if (!userEntityOptional.isPresent()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_NOT_FOUND);
            return validationErrors;
        }
        UserEntity entity = userEntityOptional.get();
        if (userDetails.getRole() != Role.ADMIN && !encoder.matches(request.getCurrentPassword(), entity.getPassword())) {
            validationErrors.addFieldError(UserKeys.CURRENT_PASSWORD, ValidationMessageKeys.User.USER_CHANGE_PASSWORD_CURRENT_PASSWORD_INVALID);
            return validationErrors;
        }
        return validationErrors;
    }
}
