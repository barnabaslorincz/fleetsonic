package hu.barnabaslorincz.fleetsonic.controller.user;

import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.model.user.UserSearchRequest;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityRequest;
import hu.barnabaslorincz.fleetsonic.payload.shared.ListResource;
import hu.barnabaslorincz.fleetsonic.payload.user.UserChangePasswordRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserChangeRoleRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserResource;
import hu.barnabaslorincz.fleetsonic.payload.user.UserUpdateRequest;
import hu.barnabaslorincz.fleetsonic.service.shared.permission.PermissionService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationService;
import hu.barnabaslorincz.fleetsonic.service.user.UserPermissionService;
import hu.barnabaslorincz.fleetsonic.service.user.UserService;
import hu.barnabaslorincz.fleetsonic.service.user.UserMapper;
import hu.barnabaslorincz.fleetsonic.util.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    PermissionService permissionService;

    @Autowired
    UserPermissionService userPermissionService;

    @Autowired
    UserService userService;

    @Autowired
    UserMapper userMapper;

    @Autowired
    ValidationService validationService;

    @Autowired
    UserRequestValidator userRequestValidator;

    @GetMapping("")
    public ResponseEntity<ListResource<UserResource>> loadList(
            @RequestParam(name = UserKeys.USER_NAME, required = false) String userName,
            @RequestParam(name = UserKeys.FIRST_NAME, required = false) String firstName,
            @RequestParam(name = UserKeys.LAST_NAME, required = false) String lastName,
            @RequestParam(name = UserKeys.EMAIL, required = false) String email,
            @RequestParam(name = UserKeys.ROLE, required = false) Role role,
            @RequestParam(name = UserKeys.DRIVING_LICENSE_EXPIRY_FROM, required = false) String drivingLicenseExpiryFrom,
            @RequestParam(name = UserKeys.DRIVING_LICENSE_EXPIRY_TO, required = false) String drivingLicenseExpiryTo,
            @RequestParam(name = SharedKeys.PAGE_NUMBER, required=false , defaultValue="1") Integer pageNumber,
            @RequestParam(name = SharedKeys.NUMBER_OF_ITEMS, required=false , defaultValue="10") Integer numberOfItems,
            @RequestParam(name = SharedKeys.ORDER_DIRECTION, required=false , defaultValue="DESC") Sort.Direction orderDirection,
            @RequestParam(name = SharedKeys.ORDER_FIELD, required=false , defaultValue=SharedKeys.ID) String orderField,
            Authentication authentication
            ) {
        PageRequest request = PageRequest.of(pageNumber-1, numberOfItems, orderDirection, orderField);
        UserSearchRequest searchRequest = UserSearchRequest.builder()
                .userName(userName)
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .role(role)
                .drivingLicenseExpiryFrom(ControllerUtils.parseLocalDate(drivingLicenseExpiryFrom))
                .drivingLicenseExpiryTo(ControllerUtils.parseLocalDate(drivingLicenseExpiryTo))
                .pageRequest(request)
                .build();
        List<UserEntity> userEntityList = userService.readAll(searchRequest, authentication);
        long totalNumberOfItems = userService.totalNumberOfItems(authentication);
        long currentNumberOfItems = userService.currentNumberOfItems(searchRequest, authentication);
        ListResource<UserResource> userResourceListResource =
                ListResource.<UserResource>builder()
                .items(userEntityList.stream().map(userMapper::toResource).collect(Collectors.toList()))
                .pagingResult(ListResource.PagingResult.builder()
                        .totalNumberOfItems(totalNumberOfItems)
                        .currentNumberOfItems(currentNumberOfItems)
                        .build())
                .build();
        return ResponseEntity.ok(userResourceListResource);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserResource> getUser(@PathVariable(value = "userId") long userId) {
        userPermissionService.checkReadRight(userId);
        validationService.checkErrors(userRequestValidator.validateGetRequest(userId));
        return ResponseEntity
                .ok(userMapper.toResource(userService.reqById(userId)));
    }

    @PutMapping("/{userId}")
    public ResponseEntity<Object> updateUser(
            @PathVariable(value = "userId") long userId,
            @RequestBody UserUpdateRequest request,
            Authentication authentication) {
        userPermissionService.checkUpdateRight(userId);
        validationService.checkErrors(userRequestValidator.validateUpdateRequest(userId, request, authentication));
        userService.updateUser(userId, request);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{userId}/change-role")
    public ResponseEntity<Object> updateRole(
            @PathVariable(value = "userId") long userId,
            @RequestBody UserChangeRoleRequest request) {
        userPermissionService.checkChangeRoleRight(userId);
        validationService.checkErrors(userRequestValidator.validateUpdateRoleRequest(userId, request));
        userService.updateRole(userId, request);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{userId}/change-password")
    public ResponseEntity<Object> updatePassword(
            @PathVariable(value = "userId") long userId,
            @RequestBody UserChangePasswordRequest request,
            Authentication authentication) {
        userPermissionService.checkUpdateRight(userId);
        validationService.checkErrors(userRequestValidator.validateUpdatePasswordRequest(userId, request, authentication));
        userService.updatePassword(userId, request);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{userId}/image")
    public ResponseEntity<Object> updateImage(
            @PathVariable(value = "userId") long userId,
            @RequestBody IdentityRequest request,
            Authentication authentication) {
        // validationService.checkErrors(userRequestValidator.validateGetRequest(userId));
        userService.updateImage(userId, request.getId());
        return ResponseEntity
                .ok().build();
    }
}
