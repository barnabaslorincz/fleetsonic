package hu.barnabaslorincz.fleetsonic.controller.authentication;

import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.payload.authentication.LoginRequest;
import hu.barnabaslorincz.fleetsonic.payload.authentication.RegisterRequest;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationErrors;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationMessageKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthRequestValidator {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    public ValidationErrors validateLoginRequest(LoginRequest loginRequest) {
        ValidationErrors validationErrors = new ValidationErrors();
        Optional<UserEntity> userEntityOptional = userRepository.findByUserName(loginRequest.getUsername());
        if (!userEntityOptional.isPresent()) {
            validationErrors.addFieldError(UserKeys.USER_NAME, ValidationMessageKeys.Authorization.AUTH_USER_NAME_INVALID);
            return validationErrors;
        }
        UserEntity entity = userEntityOptional.get();
        if (!encoder.matches(loginRequest.getPassword(), entity.getPassword())) {
            validationErrors.addFieldError(UserKeys.PASSWORD, ValidationMessageKeys.Authorization.AUTH_PASSWORD_INVALID);
            return validationErrors;
        }
        return validationErrors;
    }

    public ValidationErrors validateRegisterRequest(RegisterRequest registerRequest) {
        ValidationErrors validationErrors = new ValidationErrors();
        if (userRepository.existsByUserName(registerRequest.getUserName())) {
            validationErrors.addFieldError(UserKeys.USER_NAME, ValidationMessageKeys.User.USER_NAME_ALREADY_IN_USE);
        }
        if (userRepository.existsByEmail(registerRequest.getEmail())) {
            validationErrors.addFieldError(UserKeys.EMAIL, ValidationMessageKeys.User.EMAIL_ALREADY_IN_USE);
        }
        return validationErrors;
    }
}
