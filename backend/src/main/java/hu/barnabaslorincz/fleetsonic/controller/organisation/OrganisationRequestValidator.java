package hu.barnabaslorincz.fleetsonic.controller.organisation;

import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationEntity;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationKeys;
import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationCreateUpdateRequest;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationJoinRequest;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserChangePasswordRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserChangeRoleRequest;
import hu.barnabaslorincz.fleetsonic.payload.user.UserUpdateRequest;
import hu.barnabaslorincz.fleetsonic.repository.organisation.OrganisationRepository;
import hu.barnabaslorincz.fleetsonic.repository.user.UserRepository;
import hu.barnabaslorincz.fleetsonic.security.UserDetailsImpl;
import hu.barnabaslorincz.fleetsonic.service.organisation.OrganisationService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationErrors;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationMessageKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Optional;

@Component
public class OrganisationRequestValidator {

    @Autowired
    UserRepository userRepository;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    OrganisationService organisationService;

    @Autowired
    PasswordEncoder encoder;

    ValidationErrors validateCreateRequest(OrganisationCreateUpdateRequest request, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (userDetails.getOrganisationEntity() != null) {
            validationErrors.addGlobalError(ValidationMessageKeys.Organisation.USER_ALREADY_IN_ORGANISATION);
            return validationErrors;
        }
        Optional<OrganisationEntity> organisationEntityOptional = organisationRepository.findByName(request.getName());
        if (organisationEntityOptional.isPresent()) {
            validationErrors.addFieldError(OrganisationKeys.NAME, ValidationMessageKeys.Organisation.ORGANISATION_NAME_ALREADY_IN_USE);
        }
        return validationErrors;
    }

    ValidationErrors validateUpdateRequest(long organisationId, OrganisationCreateUpdateRequest request, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        Optional<OrganisationEntity> organisationEntityOptional = organisationRepository.findByName(request.getName());
        if (organisationEntityOptional.isPresent() && !organisationEntityOptional.get().getIdentity().equals(organisationId)) {
            validationErrors.addFieldError(OrganisationKeys.NAME, ValidationMessageKeys.Organisation.ORGANISATION_NAME_ALREADY_IN_USE);
        }
        return validationErrors;
    }

    ValidationErrors validateJoinRequest(OrganisationJoinRequest request, Authentication authentication) {
        ValidationErrors validationErrors = new ValidationErrors();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (userDetails.getOrganisationEntity() != null) {
            validationErrors.addGlobalError(ValidationMessageKeys.Organisation.USER_ALREADY_IN_ORGANISATION);
            return validationErrors;
        }
        Optional<OrganisationEntity> organisationEntityOptional = organisationRepository.findByInviteCode(request.getInviteCode());
        if (organisationEntityOptional.isEmpty()) {
            validationErrors.addFieldError(OrganisationKeys.INVITE_CODE, ValidationMessageKeys.Organisation.ORGANISATION_INVITE_CODE_INVALID);
        }
        return validationErrors;
    }

    ValidationErrors validateRemoveMemberRequest(long organisationId, IdentityRequest request) {
        ValidationErrors validationErrors = new ValidationErrors();
        if (request.getId() == null) {
            validationErrors.addGlobalError(ValidationMessageKeys.Shared.ID_NOT_PRESENT);
            return validationErrors;
        }
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(request.getId());
        if (userEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_NOT_FOUND);
            return validationErrors;
        }
        if (userEntityOptional.get().getOrganisation() != null && !userEntityOptional.get().getOrganisation().getIdentity().equals(organisationId)) {
            validationErrors.addGlobalError(ValidationMessageKeys.Organisation.ORGANISATION_USER_NOT_MEMBER);
            return validationErrors;
        }
        if (userEntityOptional.get().getRole() == Role.ADMIN && organisationService.countAdmins(organisationId) == 1) {
            validationErrors.addGlobalError(ValidationMessageKeys.Organisation.CANNOT_REMOVE_LAST_ADMIN);
            return validationErrors;
        }
        return validationErrors;
    }

    ValidationErrors validateMakeAdminRequest(long organisationId, IdentityRequest request) {
        ValidationErrors validationErrors = new ValidationErrors();
        if (request.getId() == null) {
            validationErrors.addGlobalError(ValidationMessageKeys.Shared.ID_NOT_PRESENT);
            return validationErrors;
        }
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(request.getId());
        if (userEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_NOT_FOUND);
            return validationErrors;
        }
        if (userEntityOptional.get().getOrganisation() != null && !userEntityOptional.get().getOrganisation().getIdentity().equals(organisationId)) {
            validationErrors.addGlobalError(ValidationMessageKeys.Organisation.ORGANISATION_USER_NOT_MEMBER);
            return validationErrors;
        }
        return validationErrors;
    }

    ValidationErrors validateRemoveAdminRequest(long organisationId, IdentityRequest request) {
        ValidationErrors validationErrors = new ValidationErrors();
        if (request.getId() == null) {
            validationErrors.addGlobalError(ValidationMessageKeys.Shared.ID_NOT_PRESENT);
            return validationErrors;
        }
        Optional<UserEntity> userEntityOptional = userRepository.findByIdentity(request.getId());
        if (userEntityOptional.isEmpty()) {
            validationErrors.addGlobalError(ValidationMessageKeys.User.USER_NOT_FOUND);
            return validationErrors;
        }
        if (userEntityOptional.get().getOrganisation() != null && !userEntityOptional.get().getOrganisation().getIdentity().equals(organisationId)) {
            validationErrors.addGlobalError(ValidationMessageKeys.Organisation.ORGANISATION_USER_NOT_MEMBER);
            return validationErrors;
        }
        if (userEntityOptional.get().getRole() == Role.ADMIN && organisationService.countAdmins(organisationId) == 1) {
            validationErrors.addGlobalError(ValidationMessageKeys.Organisation.CANNOT_REMOVE_LAST_ADMIN);
            return validationErrors;
        }
        return validationErrors;
    }

}
