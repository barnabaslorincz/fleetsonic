package hu.barnabaslorincz.fleetsonic.controller.organisation;

import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationEntity;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationCreateUpdateRequest;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationJoinRequest;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationResource;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityRequest;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityResponse;
import hu.barnabaslorincz.fleetsonic.service.organisation.OrganisationMapper;
import hu.barnabaslorincz.fleetsonic.service.organisation.OrganisationPermissionService;
import hu.barnabaslorincz.fleetsonic.service.organisation.OrganisationService;
import hu.barnabaslorincz.fleetsonic.service.shared.permission.PermissionService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/organisations")
public class OrganisationController {

    @Autowired
    PermissionService permissionService;

    @Autowired
    ValidationService validationService;

    @Autowired
    OrganisationService organisationService;

    @Autowired
    OrganisationMapper organisationMapper;

    @Autowired
    OrganisationPermissionService organisationPermissionService;

    @Autowired
    OrganisationRequestValidator organisationRequestValidator;

    @GetMapping("/{organisationId}")
    public ResponseEntity<OrganisationResource> getOrganisation(@PathVariable(value = "organisationId") long organisationId) {
        organisationPermissionService.checkOrganisationMember(organisationId);
        return ResponseEntity
                .ok(organisationMapper.toResource(organisationService.reqById(organisationId)));
    }

    @PostMapping("")
    @Transactional
    public ResponseEntity<IdentityResponse> createOrganisation(
            @RequestBody OrganisationCreateUpdateRequest request,
            Authentication authentication) {
        validationService.checkErrors(organisationRequestValidator.validateCreateRequest(request, authentication));
        OrganisationEntity entity = organisationService.create(request, authentication);
        return ResponseEntity
                .ok(IdentityResponse.builder()
                        .id(entity.getIdentity())
                        .build());
    }

    @PutMapping("/{organisationId}")
    public ResponseEntity<Object> updateOrganisation(
            @PathVariable(value = "organisationId") long organisationId,
            @RequestBody OrganisationCreateUpdateRequest request,
            Authentication authentication) {
        organisationPermissionService.checkOrganisationAdmin(organisationId);
        validationService.checkErrors(organisationRequestValidator.validateUpdateRequest(organisationId, request, authentication));
        organisationService.update(organisationId, request);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{organisationId}/refresh-code")
    public ResponseEntity<Object> refreshInviteCode(
            @PathVariable(value = "organisationId") long organisationId,
            Authentication authentication) {
        organisationPermissionService.checkOrganisationAdmin(organisationId);
        organisationService.refreshInviteCode(organisationId);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/join")
    public ResponseEntity<Object> join(
            @RequestBody OrganisationJoinRequest request,
            Authentication authentication) {
        validationService.checkErrors(organisationRequestValidator.validateJoinRequest(request, authentication));
        organisationService.join(request, authentication);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{organisationId}/add-member")
    public ResponseEntity<Object> addMember(
            @PathVariable(value = "organisationId") long organisationId,
            @RequestBody IdentityRequest request,
            Authentication authentication) {
        organisationService.addMember(organisationId, request);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{organisationId}/remove-member")
    public ResponseEntity<Object> removeMember(
            @PathVariable(value = "organisationId") long organisationId,
            @RequestBody IdentityRequest request,
            Authentication authentication) {
        organisationPermissionService.checkOrganisationAdmin(organisationId);
        validationService.checkErrors(organisationRequestValidator.validateRemoveMemberRequest(organisationId, request));
        organisationService.removeMember(organisationId, request);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{organisationId}/add-admin")
    public ResponseEntity<Object> makeAdmin(
            @PathVariable(value = "organisationId") long organisationId,
            @RequestBody IdentityRequest request,
            Authentication authentication) {
        organisationPermissionService.checkOrganisationAdmin(organisationId);
        validationService.checkErrors(organisationRequestValidator.validateMakeAdminRequest(organisationId, request));
        organisationService.makeAdmin(organisationId, request);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{organisationId}/remove-admin")
    public ResponseEntity<Object> removeAdmin(
            @PathVariable(value = "organisationId") long organisationId,
            @RequestBody IdentityRequest request,
            Authentication authentication) {
        organisationPermissionService.checkOrganisationAdmin(organisationId);
        validationService.checkErrors(organisationRequestValidator.validateRemoveAdminRequest(organisationId, request));
        organisationService.removeAdmin(organisationId, request);
        return ResponseEntity.ok().build();
    }
}