package hu.barnabaslorincz.fleetsonic.controller.authentication;

import hu.barnabaslorincz.fleetsonic.model.user.UserEntity;
import hu.barnabaslorincz.fleetsonic.payload.authentication.LoginRequest;
import hu.barnabaslorincz.fleetsonic.payload.authentication.LoginResponse;
import hu.barnabaslorincz.fleetsonic.payload.authentication.RegisterRequest;
import hu.barnabaslorincz.fleetsonic.payload.shared.IdentityResponse;
import hu.barnabaslorincz.fleetsonic.service.authentication.AuthService;
import hu.barnabaslorincz.fleetsonic.service.shared.validation.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("")
public class AuthController {

    @Autowired
    ValidationService validationService;

    @Autowired
    AuthRequestValidator authRequestValidator;

    @Autowired
    AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginRequest) {
        validationService.checkErrors(authRequestValidator.validateLoginRequest(loginRequest));
        LoginResponse loginResponse = authService.login(loginRequest);
        return ResponseEntity.ok(loginResponse);
    }

    @PostMapping("/register")
    public ResponseEntity<IdentityResponse> register(@RequestBody RegisterRequest registerRequest) {
        validationService.checkErrors(authRequestValidator.validateRegisterRequest(registerRequest));
        UserEntity entity = authService.register(registerRequest);
        return ResponseEntity.ok(IdentityResponse.builder()
                .id(entity.getIdentity())
                .build());
    }

}
