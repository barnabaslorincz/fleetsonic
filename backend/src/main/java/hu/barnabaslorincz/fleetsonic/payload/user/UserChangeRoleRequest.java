package hu.barnabaslorincz.fleetsonic.payload.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class UserChangeRoleRequest {

    @NonNull
    @SerializedName(UserKeys.ROLE) @JsonProperty(UserKeys.ROLE)
    private Role role;

}
