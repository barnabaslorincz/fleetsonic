package hu.barnabaslorincz.fleetsonic.payload.organisation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class OrganisationCreateUpdateRequest {

    @NonNull
    @SerializedName(OrganisationKeys.NAME) @JsonProperty(OrganisationKeys.NAME)
    private String name;

}
