package hu.barnabaslorincz.fleetsonic.payload.shared;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import lombok.Builder;

import java.util.List;

@Builder
public class ErrorResponse {

    @SerializedName(SharedKeys.GLOBAL_ERRORS) @JsonProperty(SharedKeys.GLOBAL_ERRORS)
    private final List<String> globalErrors;

    @SerializedName(SharedKeys.FIELD_ERRORS) @JsonProperty(SharedKeys.FIELD_ERRORS)
    private final List<FieldError> fieldErrors;

    @Builder
    public static final class FieldError {

        @SerializedName(SharedKeys.FIELD) @JsonProperty(SharedKeys.FIELD)
        private final String field;

        @SerializedName(SharedKeys.ERROR) @JsonProperty(SharedKeys.ERROR)
        private final String error;

    }

}
