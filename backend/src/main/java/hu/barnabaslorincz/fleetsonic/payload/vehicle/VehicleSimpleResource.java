package hu.barnabaslorincz.fleetsonic.payload.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.fueltype.FuelType;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import hu.barnabaslorincz.fleetsonic.payload.shared.BaseResource;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

@SuperBuilder
@Getter
public class VehicleSimpleResource extends BaseResource {

    @NonNull
    @SerializedName(VehicleKeys.MAKE) @JsonProperty(VehicleKeys.MAKE)
    private final String make;

    @NonNull
    @SerializedName(VehicleKeys.MODEL) @JsonProperty(VehicleKeys.MODEL)
    private final String model;

    @NonNull
    @SerializedName(VehicleKeys.LICENSE_PLATE) @JsonProperty(VehicleKeys.LICENSE_PLATE)
    private final String licensePlate;

    @Nullable
    @SerializedName(VehicleKeys.FUEL_TYPE) @JsonProperty(VehicleKeys.FUEL_TYPE)
    private final FuelType fuelType;

    @Nullable
    @SerializedName(UserKeys.IMAGE_ID) @JsonProperty(UserKeys.IMAGE_ID)
    private final Long imageId;

}
