package hu.barnabaslorincz.fleetsonic.payload.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.annotation.Nullable;

@Getter
@Setter
public class UserChangePasswordRequest {

    @Nullable
    @SerializedName(UserKeys.CURRENT_PASSWORD) @JsonProperty(UserKeys.CURRENT_PASSWORD)
    private String currentPassword;

    @NonNull
    @SerializedName(UserKeys.NEW_PASSWORD) @JsonProperty(UserKeys.NEW_PASSWORD)
    private String newPassword;

}
