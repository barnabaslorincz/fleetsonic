package hu.barnabaslorincz.fleetsonic.payload.journey;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.event.EventKeys;
import hu.barnabaslorincz.fleetsonic.model.eventtype.EventType;
import hu.barnabaslorincz.fleetsonic.model.journey.JourneyKeys;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleSimpleResource;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.time.LocalDate;

@Getter
@Setter
public class JourneyCreateUpdateRequest {

    @NonNull
    @SerializedName(JourneyKeys.START_TIME) @JsonProperty(JourneyKeys.START_TIME)
    private LocalDate startTime;

    @Nullable
    @SerializedName(JourneyKeys.FINISH_TIME) @JsonProperty(JourneyKeys.FINISH_TIME)
    private LocalDate finishTime;

    @NonNull
    @SerializedName(JourneyKeys.START_KM) @JsonProperty(JourneyKeys.START_KM)
    private Integer startKm;

    @Nullable
    @SerializedName(JourneyKeys.FINISH_KM) @JsonProperty(JourneyKeys.FINISH_KM)
    private Integer finishKm;

    @Nullable
    @SerializedName(JourneyKeys.START_LATITUDE) @JsonProperty(JourneyKeys.START_LATITUDE)
    private Double startLatitude;

    @Nullable
    @SerializedName(JourneyKeys.START_LONGITUDE) @JsonProperty(JourneyKeys.START_LONGITUDE)
    private Double startLongitude;

    @Nullable
    @SerializedName(JourneyKeys.FINISH_LATITUDE) @JsonProperty(JourneyKeys.FINISH_LATITUDE)
    private Double finishLatitude;

    @Nullable
    @SerializedName(JourneyKeys.FINISH_LONGITUDE) @JsonProperty(JourneyKeys.FINISH_LONGITUDE)
    private Double finishLongitude;

    @Nullable
    @SerializedName(JourneyKeys.DESCRIPTION) @JsonProperty(JourneyKeys.DESCRIPTION)
    private String description;

}
