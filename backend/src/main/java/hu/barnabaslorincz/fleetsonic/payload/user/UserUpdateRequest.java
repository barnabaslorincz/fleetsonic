package hu.barnabaslorincz.fleetsonic.payload.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.annotation.Nullable;
import java.time.LocalDate;

@Getter
@Setter
public class UserUpdateRequest {

    @NonNull
    @SerializedName(UserKeys.FIRST_NAME) @JsonProperty(UserKeys.FIRST_NAME)
    private String firstName;

    @NonNull
    @SerializedName(UserKeys.LAST_NAME) @JsonProperty(UserKeys.LAST_NAME)
    private String lastName;

    @Nullable
    @SerializedName(UserKeys.PROFILE_PICTURE_URL) @JsonProperty(UserKeys.PROFILE_PICTURE_URL)
    private String profilePictureUrl;

    @Nullable
    @SerializedName(UserKeys.DRIVING_LICENSE_EXPIRY) @JsonProperty(UserKeys.DRIVING_LICENSE_EXPIRY)
    private LocalDate drivingLicenceExpiry;

}
