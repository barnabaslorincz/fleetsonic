package hu.barnabaslorincz.fleetsonic.payload.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.role.Role;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationResource;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.time.LocalDate;

@Builder
@Getter
@Setter
public class LoginResponse {

    @NonNull
    @SerializedName(UserKeys.TOKEN) @JsonProperty(UserKeys.TOKEN)
    private String token;

    @NonNull
    @SerializedName(SharedKeys.ID) @JsonProperty(SharedKeys.ID)
    private Long id;

    @NonNull
    @SerializedName(UserKeys.USER_NAME) @JsonProperty(UserKeys.USER_NAME)
    private final String userName;

    @NonNull
    @SerializedName(UserKeys.EMAIL) @JsonProperty(UserKeys.EMAIL)
    private final String email;

    @Nullable
    @SerializedName(UserKeys.ORGANISATION) @JsonProperty(UserKeys.ORGANISATION)
    private final OrganisationResource organisation;

    @Nullable
    @SerializedName(UserKeys.IMAGE_ID) @JsonProperty(UserKeys.IMAGE_ID)
    private final Long imageId;

    @NonNull
    @SerializedName(UserKeys.ROLE) @JsonProperty(UserKeys.ROLE)
    private final Role role;

}
