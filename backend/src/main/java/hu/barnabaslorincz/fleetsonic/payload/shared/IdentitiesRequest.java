package hu.barnabaslorincz.fleetsonic.payload.shared;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class IdentitiesRequest {

    @NonNull
    @SerializedName(SharedKeys.IDS) @JsonProperty(SharedKeys.IDS)
    private List<Long> ids;

}
