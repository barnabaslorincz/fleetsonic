package hu.barnabaslorincz.fleetsonic.payload.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.event.EventKeys;
import hu.barnabaslorincz.fleetsonic.model.eventtype.EventType;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class EventCreateRequest {

    @NonNull
    @SerializedName(EventKeys.EVENT_TYPE) @JsonProperty(EventKeys.EVENT_TYPE)
    private EventType eventType;

    @NonNull
    @SerializedName(EventKeys.TIME) @JsonProperty(EventKeys.TIME)
    private LocalDate time;

    @NonNull
    @SerializedName(EventKeys.KM_COUNTER) @JsonProperty(EventKeys.KM_COUNTER)
    private Integer kmCounter;

    @Nullable
    @SerializedName(EventKeys.PRICE) @JsonProperty(EventKeys.PRICE)
    private Integer price;

    @Nullable
    @SerializedName(EventKeys.LATITUDE) @JsonProperty(EventKeys.LATITUDE)
    private Double latitude;

    @Nullable
    @SerializedName(EventKeys.LONGITUDE) @JsonProperty(EventKeys.LONGITUDE)
    private Double longitude;

    @Nullable
    @SerializedName(EventKeys.AMOUNT) @JsonProperty(EventKeys.AMOUNT)
    private Double amount;

    @Nullable
    @SerializedName(EventKeys.DESCRIPTION) @JsonProperty(EventKeys.DESCRIPTION)
    private String description;

}
