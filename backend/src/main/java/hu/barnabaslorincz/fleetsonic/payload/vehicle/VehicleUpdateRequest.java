package hu.barnabaslorincz.fleetsonic.payload.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.fueltype.FuelType;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.time.LocalDate;

@Getter
@Setter
public class VehicleUpdateRequest {

    @NonNull
    @SerializedName(VehicleKeys.LICENSE_PLATE) @JsonProperty(VehicleKeys.LICENSE_PLATE)
    private String licensePlate;

    @Nullable
    @SerializedName(VehicleKeys.FUEL_CAPACITY) @JsonProperty(VehicleKeys.FUEL_CAPACITY)
    private Integer fuelCapacity;

    @Nullable
    @SerializedName(VehicleKeys.INSPECTION_VALIDITY) @JsonProperty(VehicleKeys.INSPECTION_VALIDITY)
    private LocalDate inspectionValidity;

}
