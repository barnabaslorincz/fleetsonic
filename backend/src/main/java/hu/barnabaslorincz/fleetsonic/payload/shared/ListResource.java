package hu.barnabaslorincz.fleetsonic.payload.shared;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import lombok.Builder;
import lombok.Getter;

import javax.annotation.Nonnull;
import java.util.List;

@Builder
@Getter
public class ListResource<T> {

    @Nonnull
    @SerializedName(SharedKeys.ITEMS) @JsonProperty(SharedKeys.ITEMS)
    List<T> items;

    @Nonnull
    @SerializedName(SharedKeys.PAGING_RESULT) @JsonProperty(SharedKeys.PAGING_RESULT)
    PagingResult pagingResult;

    @Builder
    @Getter
    public static class PagingResult {

        @SerializedName(SharedKeys.TOTAL_NUMBER_OF_ITEMS) @JsonProperty(SharedKeys.TOTAL_NUMBER_OF_ITEMS)
        long totalNumberOfItems;

        @SerializedName(SharedKeys.CURRENT_NUMBER_OF_ITEMS) @JsonProperty(SharedKeys.CURRENT_NUMBER_OF_ITEMS)
        long currentNumberOfItems;

    }

}
