package hu.barnabaslorincz.fleetsonic.payload.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.fueltype.FuelType;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import hu.barnabaslorincz.fleetsonic.payload.organisation.OrganisationResource;
import hu.barnabaslorincz.fleetsonic.payload.shared.BaseResource;
import hu.barnabaslorincz.fleetsonic.payload.user.UserSimpleResource;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

import java.time.LocalDate;

@SuperBuilder
@Getter
public class VehicleResource extends BaseResource {

    @NonNull
    @SerializedName(VehicleKeys.MAKE) @JsonProperty(VehicleKeys.MAKE)
    private final String make;

    @NonNull
    @SerializedName(VehicleKeys.MODEL) @JsonProperty(VehicleKeys.MODEL)
    private final String model;

    @NonNull
    @SerializedName(VehicleKeys.FUEL_TYPE) @JsonProperty(VehicleKeys.FUEL_TYPE)
    private final FuelType fuelType;

    @NonNull
    @SerializedName(VehicleKeys.LICENSE_PLATE) @JsonProperty(VehicleKeys.LICENSE_PLATE)
    private final String licensePlate;

    @Nullable
    @SerializedName(VehicleKeys.FUEL_CAPACITY) @JsonProperty(VehicleKeys.FUEL_CAPACITY)
    private final Integer fuelCapacity;

    @NonNull
    @SerializedName(VehicleKeys.KM_COUNTER) @JsonProperty(VehicleKeys.KM_COUNTER)
    private final Integer kmCounter;

    @Nullable
    @SerializedName(VehicleKeys.INSPECTION_VALIDITY) @JsonProperty(VehicleKeys.INSPECTION_VALIDITY)
    private final LocalDate inspectionValidity;

    @Nullable
    @SerializedName(VehicleKeys.OWNER) @JsonProperty(VehicleKeys.OWNER)
    private final UserSimpleResource owner;

    @Nullable
    @SerializedName(VehicleKeys.ORGANISATION) @JsonProperty(VehicleKeys.ORGANISATION)
    private final OrganisationResource organisation;

    @Nullable
    @SerializedName(UserKeys.IMAGE_ID) @JsonProperty(UserKeys.IMAGE_ID)
    private final Long imageId;

}
