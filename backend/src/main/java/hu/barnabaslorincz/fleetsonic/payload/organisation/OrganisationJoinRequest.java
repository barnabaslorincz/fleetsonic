package hu.barnabaslorincz.fleetsonic.payload.organisation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class OrganisationJoinRequest {

    @NonNull
    @SerializedName(OrganisationKeys.INVITE_CODE) @JsonProperty(OrganisationKeys.INVITE_CODE)
    private String inviteCode;

}
