package hu.barnabaslorincz.fleetsonic.payload.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import hu.barnabaslorincz.fleetsonic.payload.shared.BaseResource;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

@SuperBuilder
@Getter
public class UserSimpleResource extends BaseResource {

    @NonNull
    @SerializedName(UserKeys.FIRST_NAME) @JsonProperty(UserKeys.FIRST_NAME)
    private final String firstName;

    @NonNull
    @SerializedName(UserKeys.LAST_NAME) @JsonProperty(UserKeys.LAST_NAME)
    private final String lastName;

    @NonNull
    @SerializedName(UserKeys.USER_NAME) @JsonProperty(UserKeys.USER_NAME)
    private final String userName;

    @Nullable
    @SerializedName(UserKeys.IMAGE_ID) @JsonProperty(UserKeys.IMAGE_ID)
    private final Long imageId;

}
