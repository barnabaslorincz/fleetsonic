package hu.barnabaslorincz.fleetsonic.payload.shared;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;
import java.util.Date;

@SuperBuilder
public class BaseResource {

    @NonNull
    @SerializedName(SharedKeys.ID) @JsonProperty(SharedKeys.ID)
    private final long id;

    @NonNull
    @SerializedName(SharedKeys.CREATION_TIME) @JsonProperty(SharedKeys.CREATION_TIME)
    private final Date creationTime;

    @NonNull
    @SerializedName(SharedKeys.UPDATE_TIME) @JsonProperty(SharedKeys.UPDATE_TIME)
    private final Date updateTime;

    @Nullable
    @SerializedName(SharedKeys.CREATOR_USER) @JsonProperty(SharedKeys.CREATOR_USER)
    private final String creatorUser;

    @Nullable
    @SerializedName(SharedKeys.CREATOR_USER_ID) @JsonProperty(SharedKeys.CREATOR_USER_ID)
    private final Long creatorUserId;

}
