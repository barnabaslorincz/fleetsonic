package hu.barnabaslorincz.fleetsonic.payload.organisation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationKeys;
import hu.barnabaslorincz.fleetsonic.payload.shared.BaseResource;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class OrganisationResource extends BaseResource {

    @NonNull
    @SerializedName(OrganisationKeys.NAME) @JsonProperty(OrganisationKeys.NAME)
    private final String name;

    @NonNull
    @SerializedName(OrganisationKeys.INVITE_CODE) @JsonProperty(OrganisationKeys.INVITE_CODE)
    private final String inviteCode;

}
