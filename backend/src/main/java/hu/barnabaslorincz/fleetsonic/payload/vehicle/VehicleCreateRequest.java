package hu.barnabaslorincz.fleetsonic.payload.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.fueltype.FuelType;
import hu.barnabaslorincz.fleetsonic.model.organisation.OrganisationKeys;
import hu.barnabaslorincz.fleetsonic.model.vehicle.VehicleKeys;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.time.LocalDate;

@Getter
@Setter
public class VehicleCreateRequest {

    @NonNull
    @SerializedName(VehicleKeys.MAKE) @JsonProperty(VehicleKeys.MAKE)
    private String make;

    @NonNull
    @SerializedName(VehicleKeys.MODEL) @JsonProperty(VehicleKeys.MODEL)
    private String model;

    @NonNull
    @SerializedName(VehicleKeys.FUEL_TYPE) @JsonProperty(VehicleKeys.FUEL_TYPE)
    private FuelType fuelType;

    @NonNull
    @SerializedName(VehicleKeys.LICENSE_PLATE) @JsonProperty(VehicleKeys.LICENSE_PLATE)
    private String licensePlate;

    @Nullable
    @SerializedName(VehicleKeys.FUEL_CAPACITY) @JsonProperty(VehicleKeys.FUEL_CAPACITY)
    private Integer fuelCapacity;

    @NonNull
    @SerializedName(VehicleKeys.KM_COUNTER) @JsonProperty(VehicleKeys.KM_COUNTER)
    private Integer kmCounter;

    @Nullable
    @SerializedName(VehicleKeys.INSPECTION_VALIDITY) @JsonProperty(VehicleKeys.INSPECTION_VALIDITY)
    private LocalDate inspectionValidity;

    @Nullable
    @SerializedName(VehicleKeys.OWNER_ID) @JsonProperty(VehicleKeys.OWNER_ID)
    private Long ownerId;

    @Nullable
    @SerializedName(VehicleKeys.ORGANISATION_ID) @JsonProperty(VehicleKeys.ORGANISATION_ID)
    private Long organisationId;

}
