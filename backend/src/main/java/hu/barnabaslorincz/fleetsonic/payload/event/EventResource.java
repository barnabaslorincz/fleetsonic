package hu.barnabaslorincz.fleetsonic.payload.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.event.EventKeys;
import hu.barnabaslorincz.fleetsonic.model.eventtype.EventType;
import hu.barnabaslorincz.fleetsonic.payload.shared.BaseResource;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleSimpleResource;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

import java.time.LocalDate;

@SuperBuilder
@Getter
public class EventResource extends BaseResource {

    @NonNull
    @SerializedName(EventKeys.EVENT_TYPE) @JsonProperty(EventKeys.EVENT_TYPE)
    private final EventType eventType;

    @NonNull
    @SerializedName(EventKeys.TIME) @JsonProperty(EventKeys.TIME)
    private final LocalDate time;

    @NonNull
    @SerializedName(EventKeys.KM_COUNTER) @JsonProperty(EventKeys.KM_COUNTER)
    private final Integer kmCounter;

    @Nullable
    @SerializedName(EventKeys.PRICE) @JsonProperty(EventKeys.PRICE)
    private final Integer price;

    @Nullable
    @SerializedName(EventKeys.LATITUDE) @JsonProperty(EventKeys.LATITUDE)
    private final Double latitude;

    @Nullable
    @SerializedName(EventKeys.LONGITUDE) @JsonProperty(EventKeys.LONGITUDE)
    private final Double longitude;

    @Nullable
    @SerializedName(EventKeys.AMOUNT) @JsonProperty(EventKeys.AMOUNT)
    private final Double amount;

    @Nullable
    @SerializedName(EventKeys.DESCRIPTION) @JsonProperty(EventKeys.DESCRIPTION)
    private final String description;

    @Nullable
    @SerializedName(EventKeys.VEHICLE) @JsonProperty(EventKeys.VEHICLE)
    private final VehicleSimpleResource vehicle;

}
