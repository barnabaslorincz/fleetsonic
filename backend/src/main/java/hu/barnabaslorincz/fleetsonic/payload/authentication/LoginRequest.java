package hu.barnabaslorincz.fleetsonic.payload.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.user.UserKeys;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginRequest {

    @NonNull
    @SerializedName(UserKeys.USER_NAME) @JsonProperty(UserKeys.USER_NAME)
    private String username;

    @NonNull
    @SerializedName(UserKeys.PASSWORD) @JsonProperty(UserKeys.PASSWORD)
    private String password;
}

