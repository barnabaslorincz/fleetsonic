package hu.barnabaslorincz.fleetsonic.payload.shared;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.shared.SharedKeys;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

@Getter
@Setter
public class IdentityRequest {

    @Nullable
    @SerializedName(SharedKeys.ID) @JsonProperty(SharedKeys.ID)
    private Long id;

}
