package hu.barnabaslorincz.fleetsonic.payload.journey;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import hu.barnabaslorincz.fleetsonic.model.journey.JourneyKeys;
import hu.barnabaslorincz.fleetsonic.payload.shared.BaseResource;
import hu.barnabaslorincz.fleetsonic.payload.vehicle.VehicleSimpleResource;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

import java.time.LocalDate;

@SuperBuilder
@Getter
public class JourneyResource extends BaseResource {

    @NonNull
    @SerializedName(JourneyKeys.START_TIME) @JsonProperty(JourneyKeys.START_TIME)
    private final LocalDate startTime;

    @Nullable
    @SerializedName(JourneyKeys.FINISH_TIME) @JsonProperty(JourneyKeys.FINISH_TIME)
    private final LocalDate finishTime;

    @NonNull
    @SerializedName(JourneyKeys.START_KM) @JsonProperty(JourneyKeys.START_KM)
    private final Integer startKm;

    @Nullable
    @SerializedName(JourneyKeys.FINISH_KM) @JsonProperty(JourneyKeys.FINISH_KM)
    private final Integer finishKm;

    @Nullable
    @SerializedName(JourneyKeys.START_LATITUDE) @JsonProperty(JourneyKeys.START_LATITUDE)
    private final Double startLatitude;

    @Nullable
    @SerializedName(JourneyKeys.START_LONGITUDE) @JsonProperty(JourneyKeys.START_LONGITUDE)
    private final Double startLongitude;

    @Nullable
    @SerializedName(JourneyKeys.FINISH_LATITUDE) @JsonProperty(JourneyKeys.FINISH_LATITUDE)
    private final Double finishLatitude;

    @Nullable
    @SerializedName(JourneyKeys.FINISH_LONGITUDE) @JsonProperty(JourneyKeys.FINISH_LONGITUDE)
    private final Double finishLongitude;

    @Nullable
    @SerializedName(JourneyKeys.DESCRIPTION) @JsonProperty(JourneyKeys.DESCRIPTION)
    private final String description;

    @Nullable
    @SerializedName(JourneyKeys.VEHICLE) @JsonProperty(JourneyKeys.VEHICLE)
    private final VehicleSimpleResource vehicle;

}
